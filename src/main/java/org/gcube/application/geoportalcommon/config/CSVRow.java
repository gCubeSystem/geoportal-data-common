/**
 *
 */
package org.gcube.application.geoportalcommon.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * The Class CSVRow.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * Jan 29, 2019
 */
public class CSVRow implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 6254861811998867626L;

	private List<String> listValues;

	/**
	 * Instantiates a new CSV row.
	 */
	public CSVRow(){

	}


	/**
	 * Gets the list values.
	 *
	 * @return the listValues
	 */
	public List<String> getListValues() {

		return listValues;
	}



	/**
	 * @param listValues the listValues to set
	 */
	public void setListValues(List<String> listValues) {

		this.listValues = listValues;
	}
	
	
	/**
	 * Adds the value.
	 *
	 * @param value the value
	 */
	public void addValue(String value) {
		if(listValues==null)
			listValues = new ArrayList<String>();
		
		listValues.add(value);
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("CSVRow [listValues=");
		builder.append(listValues);
		builder.append("]");
		return builder.toString();
	}

}
