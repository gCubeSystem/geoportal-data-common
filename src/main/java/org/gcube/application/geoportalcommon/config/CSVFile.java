/**
 *
 */
package org.gcube.application.geoportalcommon.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class CSVFile.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * Jan 29, 2019
 */
public class CSVFile implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 6408321963787244600L;
	private CSVRow headerRow;
	private List<CSVRow> valueRows;
	private String fileName;

	/**
	 * Instantiates a new CSV file.
	 */
	public CSVFile(){

	}


	/**
	 * Instantiates a new csv file.
	 *
	 * @param fileName the file name
	 * @param headerRow the header row
	 * @param valueRows the value rows
	 */
	public CSVFile(String fileName, CSVRow headerRow, List<CSVRow> valueRows) {
		this.fileName = fileName;
		this.headerRow = headerRow;
		this.valueRows = valueRows;
	}


	/**
	 * Gets the header row.
	 *
	 * @return the headerRow
	 */
	public CSVRow getHeaderRow() {

		return headerRow;
	}
	
	/**
	 * Adds the value row.
	 *
	 * @param row the row
	 */
	public void addValueRow(CSVRow row) {
		if(valueRows==null)
			valueRows = new ArrayList<CSVRow>();
		
		valueRows.add(row);
	}
	
	
	/**
	 * Sets the value rows.
	 *
	 * @param valueRows the new value rows
	 */
	public void setValueRows(List<CSVRow> valueRows) {
		this.valueRows = valueRows;
	}


	/**
	 * Gets the value rows.
	 *
	 * @return the valueRows
	 */
	public List<CSVRow> getValueRows() {

		return valueRows;
	}


	/**
	 * Sets the header row.
	 *
	 * @param headerRow the headerRow to set
	 */
	public void setHeaderRow(CSVRow headerRow) {

		this.headerRow = headerRow;
	}

	


	/**
	 * Gets the file name.
	 *
	 * @return the fileName
	 */
	public String getFileName() {

		return fileName;
	}



	/**
	 * Sets the file name.
	 *
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {

		this.fileName = fileName;
	}


	/**
	 * To string.
	 *
	 * @return the string
	 */
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("CSVFile [headerRow=");
		builder.append(headerRow);
		builder.append(", valueRows=");
		builder.append(valueRows);
		builder.append(", fileName=");
		builder.append(fileName);
		builder.append("]");
		return builder.toString();
	}



}
