package org.gcube.application.geoportalcommon;

import static org.gcube.resources.discovery.icclient.ICFactory.client;

import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.gcube.application.geoportalcommon.shared.GNADataViewerConfigProfile;
import org.gcube.application.geoportalcommon.shared.exception.ApplicationProfileNotFoundException;
import org.gcube.common.resources.gcore.utils.XPathHelper;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.Query;
import org.gcube.resources.discovery.client.queries.impl.QueryBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 * The Class GNADataViewerConfigProfileReader.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 21, 2021
 */
public class GNADataViewerConfigProfileReader {

	private static final String RESOURCE_PROFILE_BODY = "/Resource/Profile/Body";
	/**
	 *
	 */
	public static final String SECONDARY_TYPE = "ApplicationProfile";
	public static final String GENERIC_RESOURCE_NAME = "Geoportal-DataViewer-Configs";

	private static Logger LOG = LoggerFactory.getLogger(GNADataViewerConfigProfileReader.class);
	private String secondaryType;
	private String scope;
	private String appID;

	/**
	 * Instantiates a new application profile reader.
	 *
	 * @param appID the app id
	 */
	public GNADataViewerConfigProfileReader(String appID) {

		this.appID = appID;
		this.secondaryType = SECONDARY_TYPE;
		this.scope = ScopeProvider.instance.get();
	}

	/**
	 * Read profile from infrastrucure.
	 *
	 * @return the map
	 * @throws Exception the exception
	 */
	protected GNADataViewerConfigProfile readProfileFromInfrastructure() throws Exception {

		String queryString = getGcubeGenericQueryString(secondaryType, appID);
		LOG.info("Scope " + scope + ", trying to perform query: " + queryString);

		try {

			if (scope == null)
				throw new Exception("Scope is null, set scope into ScopeProvider");

			GNADataViewerConfigProfile profile = new GNADataViewerConfigProfile();

			LOG.info("Trying to fetch ApplicationProfile in the scope: " + scope + ", SecondaryType: "
					+ secondaryType + ", AppId: " + appID);
			Query q = new QueryBox(queryString);
			DiscoveryClient<String> client = client();
			List<String> appProfile = client.submit(q);
//			String item_fields = "";
			
			if (appProfile == null || appProfile.size() == 0)
				throw new ApplicationProfileNotFoundException("ApplicationProfile with SecondaryType: " + secondaryType
						+ ", AppId: " + appID + " is not registered in the scope: " + scope);
			else {
				String elem = appProfile.get(0);
				DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document doc = docBuilder.parse(new InputSource(new StringReader(elem)));

				XPathHelper helper = new XPathHelper(doc.getDocumentElement());

				List<String> currValue = null;
				String xPathExp = RESOURCE_PROFILE_BODY + "/RestrictedPortletURL/text()";
				currValue = helper.evaluate(xPathExp);
				if (currValue != null && currValue.size() > 0) {
					profile.setRestrictedPortletURL(currValue.get(0));
				} else
					throw new Exception("I'm not able to read the path: " + xPathExp);

				xPathExp = RESOURCE_PROFILE_BODY + "/OpenPortletURL/text()";
				currValue = helper.evaluate(xPathExp);
				if (currValue != null && currValue.size() > 0) {
					profile.setOpenPortletURL(currValue.get(0));
				} else
					throw new Exception("I'm not able to read the path: " + xPathExp);

				LOG.info("returning: " + profile);
				return profile;
			}

		} catch (Exception e) {
			LOG.error("Error while trying to read the  " + SECONDARY_TYPE + " with SecondaryType "
					+ GENERIC_RESOURCE_NAME + " from scope " + scope, e);
			return null;
		} finally {

		}

	}

	/**
	 * Gets the gcube generic query string.
	 *
	 * @param secondaryType the secondary type
	 * @param appId         the app id
	 * @return the gcube generic query string
	 */
	public static String getGcubeGenericQueryString(String secondaryType, String appId) {

		return "for $profile in collection('/db/Profiles/GenericResource')//Resource "
				+ "where $profile/Profile/SecondaryType/string() eq '" + secondaryType
				+ "' and  $profile/Profile/Body/AppId/string() " + " eq '" + appId + "'" + "return $profile";
	}

	/**
	 * Gets the secondary type.
	 *
	 * @return the secondary type
	 */
	public String getSecondaryType() {
		return secondaryType;
	}

	/**
	 * Gets the scope.
	 *
	 * @return the scope
	 */
	public String getScope() {
		return scope;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GNADataViewerConfigProfileReader [LOG=");
		builder.append(LOG);
		builder.append(", secondaryType=");
		builder.append(secondaryType);
		builder.append(", scope=");
		builder.append(scope);
		builder.append(", appID=");
		builder.append(appID);
		builder.append("]");
		return builder.toString();
	}

}
