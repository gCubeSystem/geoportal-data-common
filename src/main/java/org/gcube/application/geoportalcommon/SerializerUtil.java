package org.gcube.application.geoportalcommon;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.GeoServerPlatformInfoDV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * The Class SerializerUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 26, 2022
 */
public class SerializerUtil {

	private static Logger LOG = LoggerFactory.getLogger(SerializerUtil.class);

	private static ObjectMapper mapper;

	private static ObjectMapper mapperXML;

	static {
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		mapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		mapper.registerModule(new JavaTimeModule());

		SimpleModule s = new SimpleModule();
		mapper.registerModule(s);
	}

	/**
	 * Read JSON.
	 *
	 * @param <T>    the generic type
	 * @param string the string
	 * @param clazz  the clazz
	 * @return the t
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException             Signals that an I/O exception has occurred.
	 */
	public static <T> T readJSON(String string, Class<T> clazz) throws JsonProcessingException, IOException {
		return mapper.readerFor(clazz).readValue(string);
	}

	/**
	 * Unmarshal XML.
	 *
	 * @param <T>      the generic type
	 * @param string   the string
	 * @param theClass the the class
	 * @return the class
	 * @throws Exception the exception
	 */
	public static <T> T unmarshalXML(String string, Class<T> theClass) throws Exception {
		LOG.debug("unmarshalXML: " + string.trim() + " as class: " + theClass);
		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(theClass);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			if (string == null || string.length() == 0)
				throw new Exception("Input string is null or empty");

			InputStream stream = new ByteArrayInputStream(string.getBytes());
			T umarshalledObject = (T) jaxbUnmarshaller.unmarshal(stream);
			LOG.debug("returning: " + umarshalledObject);

			return umarshalledObject;

		} catch (Exception e) {
			LOG.error("An error occurred on unmarshalling " + string + "as " + theClass, e);
			return null;
		}
	}
	
	
	public static class GeoserverPlaformInfoJsonDeserializer extends JsonDeserializer<GeoServerPlatformInfoDV[]> {

	    @Override
	    public GeoServerPlatformInfoDV[] deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
	    	LOG.info("deserialize: " +jp.getValueAsString());
	    	return jp.readValueAs(GeoServerPlatformInfoDV[].class);
	    	//return jp.readValueAs(GeoServerPlatformInfoDV[].class);
	        //List<GeoServerPlatformInfoDV> listPlatform = jp.readValueAs(new TypeReference<List<GeoServerPlatformInfoDV>>() {});
	    }

	}

}
