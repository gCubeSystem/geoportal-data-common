package org.gcube.application.geoportalcommon;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.gcube.application.geoportalcommon.config.CSVFile;
import org.gcube.application.geoportalcommon.config.CSVReader;
import org.gcube.application.geoportalcommon.config.CSVRow;
import org.gcube.application.geoportalcommon.config.FileUtil;
import org.gcube.application.geoportalcommon.shared.exception.GNAConfigException;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ItemFieldDV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GNAConfigsConverter.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 21, 2021
 */
public class GNAConfigsConverter {

	private static final Logger LOG = LoggerFactory.getLogger(GNAConfigsConverter.class);

	
	public GNAConfigsConverter() {
	}
	/**
	 * Read list items config.
	 *
	 * @param contentValue the content value
	 * @return the list
	 * @throws GNAConfigException the GNA user rights config exception
	 */
	public List<ItemFieldDV> readListItemsConfig(String contentValue) throws GNAConfigException {
		LOG.debug("readListItemsConfig called");

		File configurationFile = null;
		List<ItemFieldDV> listItemFields = new ArrayList<ItemFieldDV>();
		try {

			configurationFile = FileUtil.inputStreamToTempFile(contentValue, "GNA_ListItems_Configs"+new Random().nextInt());

			CSVReader reader = new CSVReader(configurationFile);
			CSVFile csvFile = reader.getCsvFile();
			
			if(LOG.isTraceEnabled()) {
				LOG.trace("CSV Header Row: "+csvFile.getHeaderRow());
			}
			
			List<CSVRow> rows = csvFile.getValueRows();
			LOG.debug("CSV Value Row are: "+rows);

			// Starting from index 1 (means the second row in the CSV)
			for (int i = 0; i < rows.size(); i++) {
				LOG.trace(i + " row");
				ItemFieldDV itemField = new ItemFieldDV();
				CSVRow row = rows.get(i);

				// mapping to ItemFiel
				List<String> rowValues = row.getListValues();
				LOG.debug("rowValues: " + rowValues);

				itemField.setDisplayName(rowValues.get(0));

				// Reading JSON Fields
				String[] jsonFields = rowValues.get(1).split(";");
				List<String> theJsonFields = new ArrayList<String>(jsonFields.length);
				for (String jsonField : jsonFields) {
					theJsonFields.add(jsonField.trim());
				}

				itemField.setJsonFields(theJsonFields);

				// Display as result
				if (checkYesNoValue(rowValues.get(2))) {
					itemField.setDisplayAsResult(true);
				}

				// Sortable
				if (checkYesNoValue(rowValues.get(3))) {
					itemField.setSortable(true);
				}

				// Searchable
				if (checkYesNoValue(rowValues.get(4))) {
					itemField.setSearchable(true);
				}

				listItemFields.add(itemField);
			}
			LOG.info("Returning item fields config: " + listItemFields);
			return listItemFields;

		} catch (Exception e) {
			LOG.error("An error occurred on reading the GNA config from: " + contentValue, e);
			throw new GNAConfigException("Error on reading the GNA config from: " + contentValue);
		} finally {

			if (configurationFile != null) {
				try {
					configurationFile.delete();
				} catch (Exception e) {
					// silent
				}
			}
		}

	}

	/**
	 * Check yes no value.
	 *
	 * @param value the value
	 * @return true, if successful
	 */
	public static boolean checkYesNoValue(String value) {

		if (value == null || value.isEmpty())
			return false;

		String lowerValue = value.toLowerCase();

		if (lowerValue.equals("yes") || lowerValue.equals("true")) {
			return true;
		}

		return false;
	}

}
