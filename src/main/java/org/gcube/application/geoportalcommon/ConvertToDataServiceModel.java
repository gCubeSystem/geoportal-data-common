package org.gcube.application.geoportalcommon;

import org.gcube.application.geoportal.common.model.document.access.Access;
import org.gcube.application.geoportal.common.model.document.access.AccessPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.JsonOrgJsonProvider;

/**
 * The Class ConvertToDataServiceModel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 3, 2023
 */
public class ConvertToDataServiceModel {

	private static Logger LOG = LoggerFactory.getLogger(ConvertToDataServiceModel.class);

	/**
	 * Gets the access from document section.
	 *
	 * @param theJSONDocument the the JSON document
	 * @param sectionJSONPath the section JSON path
	 * @return the access from document section
	 */
	public static Access getAccessFromDocumentSection(String theJSONDocument, String sectionJSONPath) {
		String accessPolicyPath = String.format("%s.%s", sectionJSONPath, ConvertToDataValueObjectModel.POLICY);

		AccessPolicy accessPolicy = null;
		com.jayway.jsonpath.Configuration config = com.jayway.jsonpath.Configuration.builder()
				.jsonProvider(new JsonOrgJsonProvider()).build();

		// Reading policy fields
		try {
			JsonPath theSectionPolycJsonPath = JsonPath.compile(accessPolicyPath);
			String policy = theSectionPolycJsonPath.read(theJSONDocument, config).toString();
			LOG.debug("Read " + ConvertToDataValueObjectModel.POLICY + ": " + policy + ", from section: "
					+ accessPolicyPath);
			if (policy != null) {
				accessPolicy = AccessPolicy.valueOf(policy.toUpperCase());
			}
		} catch (Exception e) {
			LOG.info("No " + ConvertToDataValueObjectModel.POLICY + " found in the path: " + accessPolicyPath);
		}

		// Reading policy fields
		String licenseIDPath = String.format("%s.%s", sectionJSONPath, ConvertToDataValueObjectModel.LICENSE_ID);
		String licenseID = null;
		try {
			JsonPath theSectionLicenseJsonPath = JsonPath.compile(licenseIDPath);
			licenseID = theSectionLicenseJsonPath.read(theJSONDocument, config).toString();
			LOG.debug("Read " + ConvertToDataValueObjectModel.LICENSE_ID + ": " + licenseID + ", from section: "
					+ licenseIDPath);
		} catch (Exception e) {
			LOG.info("No " + ConvertToDataValueObjectModel.LICENSE_ID + " found in the path: " + licenseIDPath);
		}

		Access access = new Access();
		if (accessPolicy != null)
			access.setPolicy(accessPolicy);
		if (licenseID != null)
			access.setLicense(licenseID);

		LOG.info("Access is: " + access.getPolicy() + " / " + access.getLicense());
		return access;
	}

}
