package org.gcube.application.geoportalcommon;

import java.io.Serializable;

import org.gcube.application.geoportal.common.model.legacy.AccessPolicy;
import org.gcube.application.geoportal.common.model.legacy.BBOX;
import org.gcube.application.geoportalcommon.shared.gis.BoundsMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class ConvertToGUIModel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Nov 2, 2020
 */
public class ConvertToDataViewModel {

	private static final String NO_TIME = "T00:00";

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ConvertToDataViewModel.class);

	public static final String DATE_FORMAT = "yyyy-MM-dd";

	public static final String HOURS_MINUTES_SEPARATOR = ":";

	public static final String TIME_FORMAT = "HH" + HOURS_MINUTES_SEPARATOR + "mm";

	/**
	 * To user.
	 *
	 * @param username the username
	 * @return the string
	 */
	public static String toUser(String username) {

		if (username == null)
			return null;

		return username;

	}

	/**
	 * To policy.
	 *
	 * @param policy the policy
	 * @return the string
	 */
	public static String toPolicy(AccessPolicy policy) {

		if (policy == null)
			return null;

		return policy.name();

	}

	/**
	 * To bound map.
	 *
	 * @param bbox the bbox
	 * @return the bounds map
	 */
	public static BoundsMap toBoundMap(BBOX bbox) {

		if (bbox == null)
			return null;

		return new BoundsMap(bbox.getMinLong(), bbox.getMinLat(), bbox.getMaxLong(), bbox.getMaxLat(), null);
	}

	/**
	 * To bound map.
	 *
	 * @param wmsVersion the wms version
	 * @param bbox       the bbox
	 * @param separator  the separator
	 * @return the bounds map
	 */
	public static BoundsMap toBoundMap(String wmsVersion, String bbox, String separator) {

		if (bbox == null)
			return null;

		if (wmsVersion == null)
			return null;

		if (separator == null)
			separator = ",";

		try {

			String[] bboxArr = bbox.split(separator);

			if (wmsVersion.startsWith("1.3")) {
				// is 1.3.x
				return new BoundsMap(toDouble(bboxArr[1]), toDouble(bboxArr[0]), toDouble(bboxArr[3]),
						toDouble(bboxArr[2]), wmsVersion);
			} else {
				// should be 1.1.X
				return new BoundsMap(toDouble(bboxArr[0]), toDouble(bboxArr[1]), toDouble(bboxArr[2]),
						toDouble(bboxArr[3]), wmsVersion);
			}
		} catch (Exception e) {
			LOG.warn("Error on creating Bounds for wmsVersion " + wmsVersion + " and bbox " + bbox + " : ", e);
			return null;
		}
	}

	/**
	 * To double.
	 *
	 * @param value the value
	 * @return the double
	 */
	public static Double toDouble(String value) {
		try {
			return Double.parseDouble(value);
		} catch (Exception e) {
			LOG.warn("Error on parsing " + value + " as double: ", e);
			return null;
		}
	}

	/**
	 * To JSON.
	 *
	 * @param theObj the the obj
	 * @return the string
	 */
	public static String toJSON(Object theObj) {
		LOG.debug("toJSON called");
		try {

			if (theObj instanceof Serializable) {
				return org.gcube.application.geoportal.client.utils.Serialization.write(theObj);
			}
			throw new Exception("The input object is not serializable");

		} catch (Exception e) {
			LOG.warn("Error on deserializing: ", e);
			return null;
		}
	}

}
