package org.gcube.application.geoportalcommon.shared.geoportal.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;

public class ProjectView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1681876085329564419L;
	private ProjectDV theProjectDV;
	// The DocumentDV (contained in the ProjectDV) is listed in SectionView
	private List<SectionView> listSections = new ArrayList<SectionView>();

	public ProjectView() {

	}

	public void addSectionView(SectionView sectionView) {
		listSections.add(sectionView);
	}

	public ProjectDV getTheProjectDV() {
		return theProjectDV;
	}

	public List<SectionView> getListSections() {
		return listSections;
	}

	public void setTheProjectDV(ProjectDV theProjectDV) {
		this.theProjectDV = theProjectDV;
	}

	public void setListSections(List<SectionView> listSections) {
		this.listSections = listSections;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProjectView [theProjectDV=");
		builder.append(theProjectDV);
		builder.append(", listSections=");
		builder.append(listSections);
		builder.append("]");
		return builder.toString();
	}

}
