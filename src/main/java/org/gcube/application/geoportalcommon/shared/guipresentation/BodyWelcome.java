package org.gcube.application.geoportalcommon.shared.guipresentation;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "body_welcome")
@XmlAccessorType(XmlAccessType.FIELD)
public class BodyWelcome implements Serializable {

	private static final long serialVersionUID = 801731912505360534L;
	
	@XmlElement
	private String title;
	@XmlElement
	private String description;

	public BodyWelcome() {

	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BodyWelcome [title=");
		builder.append(title);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}
	
	
}
