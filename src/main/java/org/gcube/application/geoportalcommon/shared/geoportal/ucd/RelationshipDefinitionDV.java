package org.gcube.application.geoportalcommon.shared.geoportal.ucd;

import java.io.Serializable;

public class RelationshipDefinitionDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8542669180440066726L;
	private String id;
	private String label;
	private String reverseRelationId;

	public RelationshipDefinitionDV() {

	}

	public RelationshipDefinitionDV(String id, String label, String reverseRelationId) {
		super();
		this.id = id;
		this.label = label;
		this.reverseRelationId = reverseRelationId;
	}

	public String getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public String getReverseRelationId() {
		return reverseRelationId;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setReverseRelationId(String reverseRelationId) {
		this.reverseRelationId = reverseRelationId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RelationshipDefinitionDV [id=");
		builder.append(id);
		builder.append(", label=");
		builder.append(label);
		builder.append(", reverseRelationId=");
		builder.append(reverseRelationId);
		builder.append("]");
		return builder.toString();
	}

}
