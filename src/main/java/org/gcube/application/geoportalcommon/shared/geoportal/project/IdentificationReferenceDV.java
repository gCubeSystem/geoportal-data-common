package org.gcube.application.geoportalcommon.shared.geoportal.project;

import java.io.Serializable;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;

public class IdentificationReferenceDV extends DocumentDV implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -4038859146891291745L;
	private String type;

	public IdentificationReferenceDV() {

	}

	public void setType(String type) {
		this.type = type;

	}

	public String getType() {
		return type;
	}

	public IdentificationReferencesTYPE asIdentificationReferencesTYPE() {
		try {
			return IdentificationReferencesTYPE.valueOf(type);
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IdentificationReferenceDV [type=");
		builder.append(type);
		builder.append("]");
		return builder.toString();
	}

}
