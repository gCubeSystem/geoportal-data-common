package org.gcube.application.geoportalcommon.shared;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.config.RoleRights;
import org.gcube.application.geoportalcommon.shared.guipresentation.DataEntryGUIPresentationConfig;

/**
 * The Class GNADataEntryConfigProfile.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 21, 2021
 */
public class GNADataEntryConfigProfile implements Serializable {

	private static final long serialVersionUID = 5152380669677928785L;
	private List<RoleRights> permissionsForRole;
	private DataEntryGUIPresentationConfig dataEntryGUIPresentation;

	/**
	 * Instantiates a new GNA data entry config profile.
	 */
	public GNADataEntryConfigProfile() {

	}

	public GNADataEntryConfigProfile(List<RoleRights> permissionsForRole,
			DataEntryGUIPresentationConfig dataEntryGUIPresentation) {
		this.permissionsForRole = permissionsForRole;
		this.dataEntryGUIPresentation = dataEntryGUIPresentation;
	}

	/**
	 * Gets the data entry GUI presentation.
	 *
	 * @return the data entry GUI presentation
	 */
	public DataEntryGUIPresentationConfig getDataEntryGUIPresentation() {
		return dataEntryGUIPresentation;
	}

	/**
	 * Sets the data entry GUI presentation.
	 *
	 * @param dataEntryGUIPresentation the new data entry GUI presentation
	 */
	public void setDataEntryGUIPresentation(DataEntryGUIPresentationConfig dataEntryGUIPresentation) {
		this.dataEntryGUIPresentation = dataEntryGUIPresentation;
	}

	/**
	 * Gets the permissions for role.
	 *
	 * @return the permissions for role
	 */
	public List<RoleRights> getPermissionsForRole() {
		return permissionsForRole;
	}

	/**
	 * Sets the permissions for role.
	 *
	 * @param permissionsForRole the new permissions for role
	 */
	public void setPermissionsForRole(List<RoleRights> permissionsForRole) {
		this.permissionsForRole = permissionsForRole;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GNADataEntryConfigProfile [permissionsForRole=");
		builder.append(permissionsForRole);
		builder.append(", dataEntryGUIPresentation=");
		builder.append(dataEntryGUIPresentation);
		builder.append("]");
		return builder.toString();
	}

	
	
}
