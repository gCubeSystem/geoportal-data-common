package org.gcube.application.geoportalcommon.shared.geoportal.project;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PhaseDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1894531938705738017L;

	private Integer count;
	private Phase_Id _id;

	public PhaseDV() {

	}

	@Data
	@AllArgsConstructor
	class Phase_Id {
		String phase;
		String status;

		public Phase_Id() {

		}
	}

}
