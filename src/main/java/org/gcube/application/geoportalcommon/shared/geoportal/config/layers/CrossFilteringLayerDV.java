package org.gcube.application.geoportalcommon.shared.geoportal.config.layers;

import java.util.List;

public class CrossFilteringLayerDV extends ConfiguredLayerDV {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1130075528037312939L;
	String table_show_field;
	String table_key_field;
	String table_parent_key_field;
	String table_geometry_name;
	List<CrossFilteringLayerDV> related_to;

	public CrossFilteringLayerDV() {

	}

	public String getTable_show_field() {
		return table_show_field;
	}

	public String getTable_key_field() {
		return table_key_field;
	}

	public String getTable_parent_key_field() {
		return table_parent_key_field;
	}

	public String getTable_geometry_name() {
		return table_geometry_name;
	}

	public List<CrossFilteringLayerDV> getRelated_to() {
		return related_to;
	}

	public void setTable_show_field(String table_show_field) {
		this.table_show_field = table_show_field;
	}

	public void setTable_key_field(String table_key_field) {
		this.table_key_field = table_key_field;
	}

	public void setTable_parent_key_field(String table_parent_key_field) {
		this.table_parent_key_field = table_parent_key_field;
	}

	public void setTable_geometry_name(String table_geometry_name) {
		this.table_geometry_name = table_geometry_name;
	}

	public void setRelated_to(List<CrossFilteringLayerDV> related_to) {
		this.related_to = related_to;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CrossFilteringLayerDV [table_show_field=");
		builder.append(table_show_field);
		builder.append(", table_key_field=");
		builder.append(table_key_field);
		builder.append(", table_parent_key_field=");
		builder.append(table_parent_key_field);
		builder.append(", table_geometry_name=");
		builder.append(table_geometry_name);
		builder.append(", related_to=");
		builder.append(related_to);
		builder.append("]");
		return builder.toString();
	}

}
