package org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FilesetDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3108729581669778828L;
	private String gcubeProfileFieldName; // It is the gcubeProfileFieldName in the UCD filePaths
	private String filesetFieldName; // It is the fieldName in the UCD filePaths
	private List<PayloadDV> listPayloads = new ArrayList<PayloadDV>();

	public FilesetDV() {

	}

	public String getFilesetFieldName() {
		return filesetFieldName;
	}

	public void setFilesetFieldName(String filesetFieldName) {
		this.filesetFieldName = filesetFieldName;
	}

	public String getGcubeProfileFieldName() {
		return gcubeProfileFieldName;
	}

	public void setGcubeProfileFieldName(String name) {
		this.gcubeProfileFieldName = name;
	}

	public List<PayloadDV> getListPayload() {
		return listPayloads;
	}

	public void addPayloadDV(PayloadDV payloadDV) {
		listPayloads.add(payloadDV);
	}

	public void addListPayloadsDV(List<PayloadDV> listPayloadsDV) {
		listPayloads.addAll(listPayloadsDV);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FilesetDV [gcubeProfileFieldName=");
		builder.append(gcubeProfileFieldName);
		builder.append(", filesetFieldName=");
		builder.append(filesetFieldName);
		builder.append(", listPayloads=");
		builder.append(listPayloads);
		builder.append("]");
		return builder.toString();
	}

	
	

}
