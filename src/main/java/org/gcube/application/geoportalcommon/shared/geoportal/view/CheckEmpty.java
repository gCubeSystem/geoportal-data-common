package org.gcube.application.geoportalcommon.shared.geoportal.view;

public interface CheckEmpty {
	
	public Boolean isEmpty();

}
