package org.gcube.application.geoportalcommon.shared.geoportal.project;

import java.io.Serializable;


/**
 * The Class AccessDV.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Aug 10, 2022
 */
public class AccessDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8017931037814730958L;

	private String policy;

	private String license;

	/**
	 * Instantiates a new access DV.
	 */
	public AccessDV() {

	}

	/**
	 * Gets the policy.
	 *
	 * @return the policy
	 */
	public String getPolicy() {
		return policy;
	}

	/**
	 * Gets the license.
	 *
	 * @return the license
	 */
	public String getLicense() {
		return license;
	}

	/**
	 * Sets the policy.
	 *
	 * @param policy the new policy
	 */
	public void setPolicy(String policy) {
		this.policy = policy;
	}

	/**
	 * Sets the license.
	 *
	 * @param license the new license
	 */
	public void setLicense(String license) {
		this.license = license;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccessDV [policy=");
		builder.append(policy);
		builder.append(", license=");
		builder.append(license);
		builder.append("]");
		return builder.toString();
	}

}
