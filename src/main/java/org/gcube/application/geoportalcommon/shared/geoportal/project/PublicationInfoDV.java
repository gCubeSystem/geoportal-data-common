package org.gcube.application.geoportalcommon.shared.geoportal.project;

import java.io.Serializable;

public class PublicationInfoDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6381578837148417563L;
	private AccountingInfoDV creationInfo;
	private AccountingInfoDV lastEditInfo;
	private AccessDV access;

	public PublicationInfoDV() {

	}

	public AccountingInfoDV getCreationInfo() {
		return creationInfo;
	}

	public AccountingInfoDV getLastEditInfo() {
		return lastEditInfo;
	}

	public AccessDV getAccess() {
		return access;
	}

	public void setCreationInfo(AccountingInfoDV creationInfo) {
		this.creationInfo = creationInfo;
	}

	public void setLastEditInfo(AccountingInfoDV lastEditInfo) {
		this.lastEditInfo = lastEditInfo;
	}

	public void setAccess(AccessDV access) {
		this.access = access;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PublicationInfoDV [creationInfo=");
		builder.append(creationInfo);
		builder.append(", lastEditInfo=");
		builder.append(lastEditInfo);
		builder.append(", access=");
		builder.append(access);
		builder.append("]");
		return builder.toString();
	}

}
