package org.gcube.application.geoportalcommon.shared.geoportal.project;

import java.io.Serializable;
import java.util.Set;

/**
 * The Class AccountingInfoDV.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Aug 10, 2022
 */
public class AccountingInfoDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5676731760855130687L;
	private String username;
	private Set<String> roles;
	private String context;
	private String localDate;

	/**
	 * Instantiates a new accounting info DV.
	 */
	public AccountingInfoDV() {

	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Gets the roles.
	 *
	 * @return the roles
	 */
	public Set<String> getRoles() {
		return roles;
	}

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	public String getContext() {
		return context;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Sets the roles.
	 *
	 * @param roles the new roles
	 */
	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}

	/**
	 * Sets the context.
	 *
	 * @param context the new context
	 */
	public void setContext(String context) {
		this.context = context;
	}

	public String getLocalDate() {
		return localDate;
	}

	public void setLocalDate(String localDate) {
		this.localDate = localDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccountingInfoDV [username=");
		builder.append(username);
		builder.append(", roles=");
		builder.append(roles);
		builder.append(", context=");
		builder.append(context);
		builder.append(", localDate=");
		builder.append(localDate);
		builder.append("]");
		return builder.toString();
	}

}
