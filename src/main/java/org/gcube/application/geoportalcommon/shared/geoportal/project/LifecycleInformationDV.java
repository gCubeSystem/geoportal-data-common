package org.gcube.application.geoportalcommon.shared.geoportal.project;

import java.util.List;

/**
 * The Class LifecycleInformationDV.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 21, 2022
 */
public class LifecycleInformationDV extends BasicLifecycleInformationDV {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3042285563158430778L;
	private String lastInvokedStep;
	private List<String> errorMessages;
	private List<String> warningMessages;
	private String asJSONString;
	private String lastEvent;

	/**
	 * Instantiates a new lifecycle information DV.
	 */
	public LifecycleInformationDV() {

	}

	/**
	 * Instantiates a new lifecycle information DV.
	 *
	 * @param lastInvokedStep the last invoked step
	 * @param errorMessages the error messages
	 * @param warningMessages the warning messages
	 * @param asJSONString the as JSON string
	 * @param lastEvent the last event
	 */
	public LifecycleInformationDV(String lastInvokedStep, List<String> errorMessages, List<String> warningMessages,
			String asJSONString, String lastEvent) {
		super();
		this.lastInvokedStep = lastInvokedStep;
		this.errorMessages = errorMessages;
		this.warningMessages = warningMessages;
		this.asJSONString = asJSONString;
		this.lastEvent = lastEvent;
	}

	/**
	 * Gets the phase.
	 *
	 * @return the phase
	 */
	public String getPhase() {
		return phase;
	}

	/**
	 * Gets the last invoked step.
	 *
	 * @return the last invoked step
	 */
	public String getLastInvokedStep() {
		return lastInvokedStep;
	}

	/**
	 * Gets the last operation status.
	 *
	 * @return the last operation status
	 */
	public Status getLastOperationStatus() {
		return lastOperationStatus;
	}

	/**
	 * Gets the error messages.
	 *
	 * @return the error messages
	 */
	public List<String> getErrorMessages() {
		return errorMessages;
	}

	/**
	 * Gets the warning messages.
	 *
	 * @return the warning messages
	 */
	public List<String> getWarningMessages() {
		return warningMessages;
	}

	/**
	 * Sets the phase.
	 *
	 * @param phase the new phase
	 */
	public void setPhase(String phase) {
		this.phase = phase;
	}

	/**
	 * Sets the last invoked step.
	 *
	 * @param lastInvokedStep the new last invoked step
	 */
	public void setLastInvokedStep(String lastInvokedStep) {
		this.lastInvokedStep = lastInvokedStep;
	}

	/**
	 * Sets the last operation status.
	 *
	 * @param lastOperationStatus the new last operation status
	 */
	public void setLastOperationStatus(Status lastOperationStatus) {
		this.lastOperationStatus = lastOperationStatus;
	}

	/**
	 * Sets the error messages.
	 *
	 * @param errorMessages the new error messages
	 */
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * Sets the warning messages.
	 *
	 * @param warningMessages the new warning messages
	 */
	public void setWarningMessages(List<String> warningMessages) {
		this.warningMessages = warningMessages;
	}

	/**
	 * Sets the as JSON string.
	 *
	 * @param json the new as JSON string
	 */
	public void setAsJSONString(String json) {
		this.asJSONString = json;

	}

	/**
	 * Sets the last event.
	 *
	 * @param lastEvent the new last event
	 */
	public void setLastEvent(String lastEvent) {
		this.lastEvent = lastEvent;

	}

	/**
	 * Gets the last event.
	 *
	 * @return the last event
	 */
	public String getLastEvent() {
		return lastEvent;
	}

	/**
	 * Gets the as JSON string.
	 *
	 * @return the as JSON string
	 */
	public String getAsJSONString() {
		return asJSONString;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LifecycleInformationDV [lastInvokedStep=");
		builder.append(lastInvokedStep);
		builder.append(", errorMessages=");
		builder.append(errorMessages);
		builder.append(", warningMessages=");
		builder.append(warningMessages);
		builder.append(", asJSONString=");
		builder.append(asJSONString);
		builder.append(", lastEvent=");
		builder.append(lastEvent);
		builder.append("]");
		return builder.toString();
	}

}
