package org.gcube.application.geoportalcommon.shared.config;

/**
 * The Enum GcubeUserRole.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 25, 2021
 */
public enum GcubeUserRole {

	DATA_MEMBER("Data-Member", false, false),
	DATA_EDITOR("Data-Editor", true, false),
	DATA_MANAGER("Data-Manager", true, true);

	private String name;
	private boolean writeOwn;
	private boolean writeAny;

	/**
	 * Instantiates a new gcube user role.
	 *
	 * @param name the name
	 */
	private GcubeUserRole(String name, boolean writeOwn, boolean writeAny) {
		this.name = name;
		this.writeOwn = writeOwn;
		this.writeAny = writeAny;
	}

	public String getName() {
		return name;
	}

	public boolean isWriteOwn() {
		return writeOwn;
	}

	public boolean isWriteAny() {
		return writeAny;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWriteOwn(boolean writeOwn) {
		this.writeOwn = writeOwn;
	}

	public void setWriteAny(boolean writeAny) {
		this.writeAny = writeAny;
	}

}
