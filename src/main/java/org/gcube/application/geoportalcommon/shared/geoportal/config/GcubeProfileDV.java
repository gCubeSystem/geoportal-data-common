package org.gcube.application.geoportalcommon.shared.geoportal.config;

import java.io.Serializable;
import java.util.List;

/**
 * The Class GcubeProfileDV.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 7, 2022
 */
public class GcubeProfileDV implements GeoportalConfigurationID, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6177379075015880565L;

	public static final int MIN_MAX_NOT_SPECIFIED = -1;

	private String configID;
	
	private String gcubeSecondaryType;
	private String gcubeName;
	private String sectionName;
	private String sectionTitle;
	private String parentName;

	/**
	 * 0: Not specified means Not Mandatory. The Gcube Profile is not mandatory and
	 * means that all its fields must be optional in the gCube Meta Profile. N: is
	 * the MIN number of Occurrences Default is 1 occurrence
	 */
	private int minOccurs = 1;

	/**
	 * 0: Not specified means max number of Occurrences is arbitrary. N: is the MAX
	 * number of Occurrences
	 */
	private int maxOccurs = 1;

	private List<FilePathDV> filePaths;



	/**
	 * Instantiates a new gcube profile DV.
	 */
	public GcubeProfileDV() {
	}

	@Override
	public String getID() {
		return configID;
	}

	@Override
	public void setID(String configID) {
		this.configID = configID;

	}

	/**
	 * Gets the gcube secondary type.
	 *
	 * @return the gcube secondary type
	 */
	public String getGcubeSecondaryType() {
		return gcubeSecondaryType;
	}

	/**
	 * Gets the gcube name.
	 *
	 * @return the gcube name
	 */
	public String getGcubeName() {
		return gcubeName;
	}

	/**
	 * Gets the section name.
	 *
	 * @return the section name
	 */
	public String getSectionName() {
		return sectionName;
	}

	/**
	 * Gets the section title.
	 *
	 * @return the section title
	 */
	public String getSectionTitle() {
		return sectionTitle;
	}

	/**
	 * Gets the parent name. If is null or empty return the "" /(empty string) as
	 * default.
	 *
	 * @return the parent name
	 */
	public String getParentName() {
		if (parentName == null)
			return "";

		return parentName;
	}

	/**
	 * Gets the min occurs.
	 *
	 * @return the min occurs
	 */
	public int getMinOccurs() {
		return minOccurs;
	}

	/**
	 * Gets the max occurs.
	 *
	 * @return the max occurs
	 */
	public int getMaxOccurs() {
		return maxOccurs;
	}

	/**
	 * Gets the file paths.
	 *
	 * @return the file paths
	 */
	public List<FilePathDV> getFilePaths() {
		return filePaths;
	}

	/**
	 * Sets the gcube secondary type.
	 *
	 * @param gcubeSecondaryType the new gcube secondary type
	 */
	public void setGcubeSecondaryType(String gcubeSecondaryType) {
		this.gcubeSecondaryType = gcubeSecondaryType;
	}

	/**
	 * Sets the gcube name.
	 *
	 * @param gcubeName the new gcube name
	 */
	public void setGcubeName(String gcubeName) {
		this.gcubeName = gcubeName;
	}

	/**
	 * Sets the section name.
	 *
	 * @param sectionName the new section name
	 */
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	/**
	 * Sets the section title.
	 *
	 * @param sectionTitle the new section title
	 */
	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	/**
	 * Sets the parent name.
	 *
	 * @param parentName the new parent name
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	/**
	 * Sets the min occurs.
	 *
	 * @param minOccurs the new min occurs
	 */
	public void setMinOccurs(int minOccurs) {
		this.minOccurs = minOccurs;
	}

	/**
	 * Sets the max occurs.
	 *
	 * @param maxOccurs the new max occurs
	 */
	public void setMaxOccurs(int maxOccurs) {
		this.maxOccurs = maxOccurs;
	}

	/**
	 * Sets the file paths.
	 *
	 * @param filePaths the new file paths
	 */
	public void setFilePaths(List<FilePathDV> filePaths) {
		this.filePaths = filePaths;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GcubeProfileDV [gcubeSecondaryType=");
		builder.append(gcubeSecondaryType);
		builder.append(", gcubeName=");
		builder.append(gcubeName);
		builder.append(", sectionName=");
		builder.append(sectionName);
		builder.append(", sectionTitle=");
		builder.append(sectionTitle);
		builder.append(", parentName=");
		builder.append(parentName);
		builder.append(", minOccurs=");
		builder.append(minOccurs);
		builder.append(", maxOccurs=");
		builder.append(maxOccurs);
		builder.append(", filePaths=");
		builder.append(filePaths);
		builder.append("]");
		return builder.toString();
	}

}
