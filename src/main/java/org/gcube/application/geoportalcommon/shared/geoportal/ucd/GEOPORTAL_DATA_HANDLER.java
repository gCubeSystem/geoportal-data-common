package org.gcube.application.geoportalcommon.shared.geoportal.ucd;

public enum GEOPORTAL_DATA_HANDLER {

	geoportal_data_list("org.gcube.portlets.user.geoportal-data-list", "DATA_LIST_GUI"),
	geoportal_basic_data_list("org.gcube.portlets.user.geoportal-basic-data-list", "BASIC_DATA_LIST_GUI"),
	geoportal_data_entry("org.gcube.portlets.user.geoportal-data-entry-app", "DATA_ENTRY_GUI"),
	geoportal_workflow_action_list("org.gcube.portlets.user.geoportal-workflow-action-list","WORKFLOW_ACTION_LIST_GUI"),
	geoportal_timeline_json_template("org.gcube.portlets.user.geoportal-timeline-json-template","TIMELINE_JSON_TEMPLATE"),
	geoportal_grouped_cross_filtering("org.gcube.portlets.user.grouped_cross_filtering_layers","GROUPED_CROSS_FILTERING_LAYERS"),
	geoportal_grouped_overlay_layers("org.gcube.portlets.user.grouped_overlay_layers","GROUPED_OVERLAY_LAYERS"),
	gna_concessioni_lc("GNA-CONCESSIONI-LC", "LifecycleManagement");

	String id;
	String type;

	GEOPORTAL_DATA_HANDLER(String id, String type) {
		this.id = id;
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}
}
