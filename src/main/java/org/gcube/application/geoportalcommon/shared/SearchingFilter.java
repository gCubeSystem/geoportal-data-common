package org.gcube.application.geoportalcommon.shared;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.config.ItemFieldDV;

/**
 * The Class SearchingFilter.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 30, 2021
 */
public class SearchingFilter implements Serializable {

	/**
	 * The Enum ORDER.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Nov 30, 2021
	 */
	public static enum ORDER {
		ASC("ASCENDING"), DESC("DESCENDING");

		String label;

		/**
		 * Instantiates a new order.
		 *
		 * @param label the label
		 */
		ORDER(String label) {
			this.label = label;
		}

		/**
		 * Gets the label.
		 *
		 * @return the label
		 */
		public String getLabel() {
			return label;
		}
	}

	/**
	 * The Enum LOGICAL_OP.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Dec 15, 2021
	 */
	public static enum LOGICAL_OP {
		AND("$and"), OR("$or");

		String operator;

		/**
		 * Instantiates a new order.
		 *
		 * @param operator the operator
		 */
		LOGICAL_OP(String operator) {
			this.operator = operator;
		}

		/**
		 * Gets the operator.
		 *
		 * @return the operator
		 */
		public String getOperator() {
			return operator;
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4004094263090373626L;

	private List<ItemFieldDV> orderByFields;

	private ORDER order = ORDER.ASC;

	private List<WhereClause> conditions;

	private LinkedHashMap<String, Object> projection;

	private String profileID;

	private String projectID;

	/**
	 * Instantiates a new sort filter.
	 */
	public SearchingFilter() {

	}

	private void addProjectionBaseInfo() {

		if (projection != null) {
			projection.put("_profileID", 1);
			projection.put("_profileVersion", 1);
			projection.put("_version", 1);
			projection.put("_lifecycleInformation", 1);
			projection.put("_info", 1);
			projection.put("_relationships", 1);

		}
	}

	/**
	 * Instantiates a new sort filter.
	 *
	 * @param orderByFields the order by fields
	 * @param order         the order
	 */
	public SearchingFilter(List<ItemFieldDV> orderByFields, ORDER order) {
		this.orderByFields = orderByFields;
		this.order = order;
		if (this.order == null)
			order = ORDER.ASC;
	}

	/**
	 * Instantiates a new sort filter.
	 *
	 * @param orderByFields the order by fields
	 * @param order         the order
	 * @param searchInto    the search into
	 */
	public SearchingFilter(List<ItemFieldDV> orderByFields, ORDER order, List<WhereClause> conditions) {
		this.orderByFields = orderByFields;
		this.order = order;
		this.conditions = conditions;
	}

	public List<WhereClause> getConditions() {
		return conditions;
	}

	public void setConditions(List<WhereClause> conditions) {
		this.conditions = conditions;
	}

	public void setProjection(LinkedHashMap<String, Object> projection) {
		this.projection = projection;
		addProjectionBaseInfo();
	}

	public LinkedHashMap<String, Object> getProjection() {
		return projection;
	}

	/**
	 * Gets the order by fields.
	 *
	 * @return the order by fields
	 */
	public List<ItemFieldDV> getOrderByFields() {
		return orderByFields;
	}

	/**
	 * Gets the order.
	 *
	 * @return the order
	 */
	public ORDER getOrder() {
		return order;
	}

	/**
	 * Sets the order by fields.
	 *
	 * @param orderByFields the new order by fields
	 */
	public void setOrderByFields(List<ItemFieldDV> orderByFields) {
		this.orderByFields = orderByFields;
	}

	public void setGetForIDs(String profileID, String projectID) {
		this.profileID = profileID;
		this.projectID = projectID;
	}
	
	public String getProfileID() {
		return profileID;
	}
	
	public String getProjectID() {
		return projectID;
	}

	/**
	 * Sets the order.
	 *
	 * @param order the new order
	 */
	public void setOrder(ORDER order) {
		this.order = order;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchingFilter [orderByFields=");
		builder.append(orderByFields);
		builder.append(", order=");
		builder.append(order);
		builder.append(", conditions=");
		builder.append(conditions);
		builder.append(", projection=");
		builder.append(projection);
		builder.append(", profileID=");
		builder.append(profileID);
		builder.append(", projectID=");
		builder.append(projectID);
		builder.append("]");
		return builder.toString();
	}
}
