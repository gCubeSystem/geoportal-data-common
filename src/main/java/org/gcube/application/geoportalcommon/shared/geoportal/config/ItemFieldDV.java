package org.gcube.application.geoportalcommon.shared.geoportal.config;

import java.io.Serializable;
import java.util.List;

/**
 * The Class ItemFieldDV.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 16, 2022
 */
public class ItemFieldDV implements GeoportalConfigurationID, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1489912649531434470L;
	private String displayName;
	private List<String> jsonFields;
	private String operator;
	private boolean displayAsResult;
	private boolean sortable;
	private boolean searchable;

	private String configID;

	/**
	 * Instantiates a new item field.
	 */
	public ItemFieldDV() {

	}

	/**
	 * Instantiates a new item field DV.
	 *
	 * @param displayName     the display name
	 * @param jsonFields      the json fields
	 * @param operator        the operator
	 * @param displayAsResult the display as result
	 * @param sortable        the sortable
	 * @param searchable      the searchable
	 */
	public ItemFieldDV(String displayName, List<String> jsonFields, String operator, boolean displayAsResult,
			boolean sortable, boolean searchable) {
		super();
		this.displayName = displayName;
		this.jsonFields = jsonFields;
		this.operator = operator;
		this.displayAsResult = displayAsResult;
		this.sortable = sortable;
		this.searchable = searchable;
	}

	@Override
	public String getID() {
		return configID;
	}

	@Override
	public void setID(String configID) {
		this.configID = configID;

	}

	/**
	 * Gets the display name.
	 *
	 * @return the display name
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Gets the json fields.
	 *
	 * @return the json fields
	 */
	public List<String> getJsonFields() {
		return jsonFields;
	}

	/**
	 * Gets the operator.
	 *
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * Checks if is display as result.
	 *
	 * @return true, if is display as result
	 */
	public boolean isDisplayAsResult() {
		return displayAsResult;
	}

	/**
	 * Checks if is sortable.
	 *
	 * @return true, if is sortable
	 */
	public boolean isSortable() {
		return sortable;
	}

	/**
	 * Checks if is searchable.
	 *
	 * @return true, if is searchable
	 */
	public boolean isSearchable() {
		return searchable;
	}

	/**
	 * Sets the display name.
	 *
	 * @param displayName the new display name
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Sets the json fields.
	 *
	 * @param jsonFields the new json fields
	 */
	public void setJsonFields(List<String> jsonFields) {
		this.jsonFields = jsonFields;
	}

	/**
	 * Sets the operator.
	 *
	 * @param operator the new operator
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	/**
	 * Sets the display as result.
	 *
	 * @param displayAsResult the new display as result
	 */
	public void setDisplayAsResult(boolean displayAsResult) {
		this.displayAsResult = displayAsResult;
	}

	/**
	 * Sets the sortable.
	 *
	 * @param sortable the new sortable
	 */
	public void setSortable(boolean sortable) {
		this.sortable = sortable;
	}

	/**
	 * Sets the searchable.
	 *
	 * @param searchable the new searchable
	 */
	public void setSearchable(boolean searchable) {
		this.searchable = searchable;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ItemFieldDV [displayName=");
		builder.append(displayName);
		builder.append(", jsonFields=");
		builder.append(jsonFields);
		builder.append(", operator=");
		builder.append(operator);
		builder.append(", displayAsResult=");
		builder.append(displayAsResult);
		builder.append(", sortable=");
		builder.append(sortable);
		builder.append(", searchable=");
		builder.append(searchable);
		builder.append(", configID=");
		builder.append(configID);
		builder.append("]");
		return builder.toString();
	}

}
