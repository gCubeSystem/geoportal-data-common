package org.gcube.application.geoportalcommon.shared.geoportal.config;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ActionDefinitionDV implements GeoportalConfigurationID, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6071900142588642601L;
	private String id;
	private String title;
	private String[] callSteps;
	private String description;
	private String[] displayOnPhase;

	private String configID;

	public Set<String> roles = new HashSet<String>(); //No role/s defined means enable the action by default

	public ActionDefinitionDV() {

	}

	@Override
	public String getID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setID(String configID) {
		// TODO Auto-generated method stub

	}

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String[] getCallSteps() {
		return callSteps;
	}

	public String getDescription() {
		return description;
	}

	public String[] getDisplayOnPhase() {
		return displayOnPhase;
	}

	public String getConfigID() {
		return configID;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCallSteps(String[] callSteps) {
		this.callSteps = callSteps;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDisplayOnPhase(String[] displayOnPhase) {
		this.displayOnPhase = displayOnPhase;
	}

	public void setConfigID(String configID) {
		this.configID = configID;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ActionDefinitionDV [id=");
		builder.append(id);
		builder.append(", title=");
		builder.append(title);
		builder.append(", callSteps=");
		builder.append(Arrays.toString(callSteps));
		builder.append(", description=");
		builder.append(description);
		builder.append(", displayOnPhase=");
		builder.append(Arrays.toString(displayOnPhase));
		builder.append(", configID=");
		builder.append(configID);
		builder.append(", roles=");
		builder.append(roles);
		builder.append("]");
		return builder.toString();
	}

}
