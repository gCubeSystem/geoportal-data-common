package org.gcube.application.geoportalcommon.shared.geoportal.materialization;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GeoServerPlatformInfoDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2630467451758269625L;

	@JsonProperty(value = "_type")
	private String type;
	private String workspace;
	private String storeName;

	@JsonProperty(value = "_host")
	private String host;

	private Map<String, String> ogcLinks;

	public GeoServerPlatformInfoDV() {
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getWorkspace() {
		return workspace;
	}

	public void setWorkspace(String workspace) {
		this.workspace = workspace;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Map<String, String> getOgcLinks() {
		return ogcLinks;
	}

	public void setOgcLinks(Map<String, String> ogcLinks) {
		this.ogcLinks = ogcLinks;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GeoServerPlatformInfoDV [type=");
		builder.append(type);
		builder.append(", workspace=");
		builder.append(workspace);
		builder.append(", storeName=");
		builder.append(storeName);
		builder.append(", host=");
		builder.append(host);
		builder.append(", ogcLinks=");
		builder.append(ogcLinks);
		builder.append("]");
		return builder.toString();
	}

}
