package org.gcube.application.geoportalcommon.shared.guipresentation;

import java.io.Serializable;

/**
 * The Class DataEntryGUIPresentationConfig.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 26, 2022
 */
public class DataEntryGUIPresentationConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6007504380531152501L;
	private Header header;
	private BodyWelcome bodyWelcome;

	/**
	 * Instantiates a new data entry GUI presentation config.
	 */
	public DataEntryGUIPresentationConfig() {

	}

	public Header getHeader() {
		return header;
	}

	public BodyWelcome getBodyWelcome() {
		return bodyWelcome;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public void setBodyWelcome(BodyWelcome bodyWelcome) {
		this.bodyWelcome = bodyWelcome;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataEntryGUIPresentationConfig [header=");
		builder.append(header);
		builder.append(", bodyWelcome=");
		builder.append(bodyWelcome);
		builder.append("]");
		return builder.toString();
	}

}
