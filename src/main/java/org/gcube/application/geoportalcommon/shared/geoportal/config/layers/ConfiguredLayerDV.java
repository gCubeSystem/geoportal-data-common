package org.gcube.application.geoportalcommon.shared.geoportal.config.layers;

import java.io.Serializable;

/**
 * The Class ConfiguredLayerDV.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 12, 2023
 */
public class ConfiguredLayerDV implements LayerIDV, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6910607957385140987L;
	
	private String title;
	private String description;
	private String name;
	private String wms_url;
	private String wfs_url;
	boolean display = false;

	public ConfiguredLayerDV() {

	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public String getWMS_URL() {
		return wms_url;
	}

	public String getWFS_URL() {
		return wfs_url;
	}

	public boolean isDisplay() {
		return display;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWms_url(String wms_url) {
		this.wms_url = wms_url;
	}

	public void setWfs_url(String wfs_url) {
		this.wfs_url = wfs_url;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfiguredLayerDV [title=");
		builder.append(title);
		builder.append(", description=");
		builder.append(description);
		builder.append(", name=");
		builder.append(name);
		builder.append(", wms_url=");
		builder.append(wms_url);
		builder.append(", wfs_url=");
		builder.append(wfs_url);
		builder.append(", display=");
		builder.append(display);
		builder.append("]");
		return builder.toString();
	}

	
}
