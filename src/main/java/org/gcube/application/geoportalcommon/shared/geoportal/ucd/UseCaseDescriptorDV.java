package org.gcube.application.geoportalcommon.shared.geoportal.ucd;

import java.io.Serializable;
import java.util.List;

public class UseCaseDescriptorDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2864888245002804887L;
	private String id; // this is the ProfileID
	private String version;
	private String name;
	private String description;

	private List<HandlerDeclarationDV> handlers;

	public UseCaseDescriptorDV() {
	}

	public UseCaseDescriptorDV(String id, String version, String name, String description) {
		super();
		this.id = id;
		this.version = version;
		this.name = name;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public String getProfileID() {
		return id;
	}

	public String getVersion() {
		return version;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public List<HandlerDeclarationDV> getHandlers() {
		return handlers;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setHandlers(List<HandlerDeclarationDV> handlers) {
		this.handlers = handlers;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UseCaseDescriptorDV [id=");
		builder.append(id);
		builder.append(", version=");
		builder.append(version);
		builder.append(", name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", handlers=");
		builder.append(handlers);
		builder.append("]");
		return builder.toString();
	}

}
