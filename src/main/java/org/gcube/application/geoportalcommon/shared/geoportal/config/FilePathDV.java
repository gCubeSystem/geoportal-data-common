package org.gcube.application.geoportalcommon.shared.geoportal.config;

import java.io.Serializable;

public class FilePathDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9186437393759442989L;

	private String gcubeProfileFieldName;
	private String fieldName;
	private String fieldDefinition;

	public FilePathDV() {
	}

	public String getGcubeProfileFieldName() {
		return gcubeProfileFieldName;
	}

	public void setGcubeProfileFieldName(String gcubeProfileFieldName) {
		this.gcubeProfileFieldName = gcubeProfileFieldName;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getFieldDefinition() {
		return fieldDefinition;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setFieldDefinition(String fieldDefinition) {
		this.fieldDefinition = fieldDefinition;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FilePathDV [gcubeProfileFieldName=");
		builder.append(gcubeProfileFieldName);
		builder.append(", fieldName=");
		builder.append(fieldName);
		builder.append(", fieldDefinition=");
		builder.append(fieldDefinition);
		builder.append("]");
		return builder.toString();
	}

}
