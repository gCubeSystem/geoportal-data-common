package org.gcube.application.geoportalcommon.shared.geoportal.materialization;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IndexLayerDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5352754312327975329L;
	
	@JsonProperty(value = "_type")
	private String type;
	private GCubeSDILayer layer;
	private String indexName;
	private int records;
	private String flag;

	public IndexLayerDV() {

	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public GCubeSDILayer getLayer() {
		return layer;
	}

	public void setLayer(GCubeSDILayer layer) {
		this.layer = layer;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IndexLayerDV [type=");
		builder.append(type);
		builder.append(", layer=");
		builder.append(layer);
		builder.append(", indexName=");
		builder.append(indexName);
		builder.append(", records=");
		builder.append(records);
		builder.append(", flag=");
		builder.append(flag);
		builder.append("]");
		return builder.toString();
	}

	
}
