package org.gcube.application.geoportalcommon.shared;

import java.io.Serializable;
import java.util.Map;

import org.gcube.application.geoportalcommon.shared.SearchingFilter.LOGICAL_OP;

/**
 * The Class WhereClause.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 15, 2021
 */
public class WhereClause implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8577885822669878296L;

	private LOGICAL_OP operator = LOGICAL_OP.OR;

	private Map<String, Object> searchInto;

	/**
	 * Instantiates a new where clause.
	 */
	public WhereClause() {

	}

	/**
	 * Instantiates a new where clause.
	 *
	 * @param operator   the operator
	 * @param searchInto the search into
	 */
	public WhereClause(LOGICAL_OP operator, Map<String, Object> searchInto) {
		super();
		this.operator = operator;
		this.searchInto = searchInto;
	}

	/**
	 * Gets the operator.
	 *
	 * @return the operator
	 */
	public LOGICAL_OP getOperator() {
		return operator;
	}

	/**
	 * Gets the search into.
	 *
	 * @return the search into
	 */
	public Map<String, Object> getSearchInto() {
		return searchInto;
	}

	/**
	 * Sets the operator.
	 *
	 * @param operator the new operator
	 */
	public void setOperator(LOGICAL_OP operator) {
		this.operator = operator;
	}

	/**
	 * Sets the search into.
	 *
	 * @param searchInto the search into
	 */
	public void setSearchInto(Map<String, Object> searchInto) {
		this.searchInto = searchInto;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WhereClause [operator=");
		builder.append(operator);
		builder.append(", searchInto=");
		builder.append(searchInto);
		builder.append("]");
		return builder.toString();
	}

}
