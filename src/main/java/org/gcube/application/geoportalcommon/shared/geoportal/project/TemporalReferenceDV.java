package org.gcube.application.geoportalcommon.shared.geoportal.project;

import java.util.Date;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;

/**
 * The Class TemporalReferenceDV.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 10, 2022
 * @param <T>
 */
public class TemporalReferenceDV extends DocumentDV {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7990905553022863653L;
	private String profileID;
	private String projectID;

	private String jsonTimelineObject;

	private Date start;
	private Date end;

	/**
	 * Instantiates a new temporal reference DV.
	 */
	public TemporalReferenceDV() {

	}

	public String getJsonTimelineObject() {
		return jsonTimelineObject;
	}

	public void setJsonTimelineObject(String jsonTimelineObject) {
		this.jsonTimelineObject = jsonTimelineObject;
	}

	public String getProfileID() {
		return profileID;
	}

	public String getProjectID() {
		return projectID;
	}

	public Date getStart() {
		return start;
	}

	public Date getEnd() {
		return end;
	}

	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}

	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TemporalReferenceDV [profileID=");
		builder.append(profileID);
		builder.append(", projectID=");
		builder.append(projectID);
		builder.append(", jsonTimelineObject=");
		builder.append(jsonTimelineObject);
		builder.append(", start=");
		builder.append(start);
		builder.append(", end=");
		builder.append(end);
		builder.append("]");
		return builder.toString();
	}

}
