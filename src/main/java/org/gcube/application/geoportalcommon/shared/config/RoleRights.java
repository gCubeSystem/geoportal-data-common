package org.gcube.application.geoportalcommon.shared.config;

import java.io.Serializable;
import java.util.Map;

/**
 * The Class RoleRights.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 26, 2021
 */
public class RoleRights implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -304157165851633221L;

	/**
	 * The Enum OPERATION_TYPE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Nov 26, 2021
	 */
	public static enum OPERATION_TYPE {
		READ, WRITE, READ_WRITE, UNKNOWN
	}

	private Map<OPERATION_ON_ITEM, OPERATION_TYPE> listPermessions;
	private GcubeUserRole userRole;

	/**
	 * Instantiates a new user rights.
	 */
	public RoleRights() {
		super();
	}

	/**
	 * Instantiates a new role rights.
	 *
	 * @param myUsername      the my username
	 * @param listPermessions the list permessions
	 * @param userRole        the user role
	 */
	public RoleRights(Map<OPERATION_ON_ITEM, OPERATION_TYPE> listPermessions, GcubeUserRole userRole) {
		this.listPermessions = listPermessions;
		this.userRole = userRole;
	}

	/**
	 * Gets the list permessions.
	 *
	 * @return the list permessions
	 */
	public Map<OPERATION_ON_ITEM, OPERATION_TYPE> getListPermessions() {
		return listPermessions;
	}

	/**
	 * Gets the user role.
	 *
	 * @return the user role
	 */
	public GcubeUserRole getUserRole() {
		return userRole;
	}

	/**
	 * Sets the list permessions.
	 *
	 * @param listPermessions the list permessions
	 */
	public void setListPermessions(Map<OPERATION_ON_ITEM, OPERATION_TYPE> listPermessions) {
		this.listPermessions = listPermessions;
	}

	/**
	 * Sets the user role.
	 *
	 * @param userRole the new user role
	 */
	public void setUserRole(GcubeUserRole userRole) {
		this.userRole = userRole;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RoleRights [listPermessions=");
		builder.append(listPermessions);
		builder.append(", userRole=");
		builder.append(userRole);
		builder.append("]");
		return builder.toString();
	}

}
