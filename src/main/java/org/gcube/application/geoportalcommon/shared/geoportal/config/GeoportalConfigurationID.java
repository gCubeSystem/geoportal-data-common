package org.gcube.application.geoportalcommon.shared.geoportal.config;

public interface GeoportalConfigurationID {

	String getID();

	void setID(String configID);

}
