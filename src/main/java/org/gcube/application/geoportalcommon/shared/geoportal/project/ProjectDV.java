package org.gcube.application.geoportalcommon.shared.geoportal.project;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.geojson.GeoJSON;

public class ProjectDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6296612945369540613L;
	private String id;
	private String version;
	// private PublicationInfo info;
	private String profileID; // the profile == UCD
	private String profileVersion; // the profile == UCD
	private String profileName;
	private List<RelationshipDV> relationships;
	private DocumentDV theDocument;

	// the key is the IdentificationReferenceDV.getType()
	private Map<String, IdentificationReferenceDV> mapIdentReferenceDV;
	private LifecycleInformationDV lifecycleInformationDV;

	// Shortcut to "SPATIAL REFERENCE" containted into IdentificationReferenceDV
	private GeoJSON spatialReference;
	private TemporalReferenceDV temporalReference;

	public ProjectDV() {

	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getId() {
		return id;
	}

	public String getVersion() {
		return version;
	}

	public String getProfileID() {
		return profileID;
	}

	public String getProfileVersion() {
		return profileVersion;
	}

	public List<RelationshipDV> getRelationships() {
		return relationships;
	}

	public DocumentDV getTheDocument() {
		return theDocument;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}

	public void setProfileVersion(String profileVersion) {
		this.profileVersion = profileVersion;
	}

	public void setRelationships(List<RelationshipDV> relationships) {
		this.relationships = relationships;
	}

	public void setTheDocument(DocumentDV theDocument) {
		this.theDocument = theDocument;
	}

	public LifecycleInformationDV getLifecycleInformationDV() {
		return lifecycleInformationDV;
	}

	public Map<String, IdentificationReferenceDV> getMapIdentReferenceDV() {
		return mapIdentReferenceDV;
	}

	public void setMapIdentReferenceDV(Map<String, IdentificationReferenceDV> mapIdentReferenceDV) {
		this.mapIdentReferenceDV = mapIdentReferenceDV;
	}

	public void setLifecycleInformationDV(LifecycleInformationDV lifecycleInformationDV) {
		this.lifecycleInformationDV = lifecycleInformationDV;
	}

	public GeoJSON getSpatialReference() {
		return spatialReference;
	}

	public void setSpatialReference(GeoJSON spatialReference) {
		this.spatialReference = spatialReference;
	}

	public TemporalReferenceDV getTemporalReference() {
		return temporalReference;
	}

	public void setTemporalReference(TemporalReferenceDV temporalReference) {
		this.temporalReference = temporalReference;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProjectDV [id=");
		builder.append(id);
		builder.append(", version=");
		builder.append(version);
		builder.append(", profileID=");
		builder.append(profileID);
		builder.append(", profileVersion=");
		builder.append(profileVersion);
		builder.append(", profileName=");
		builder.append(profileName);
		builder.append(", relationships=");
		builder.append(relationships);
		builder.append(", theDocument=");
		builder.append(theDocument);
		builder.append(", mapIdentReferenceDV=");
		builder.append(mapIdentReferenceDV);
		builder.append(", lifecycleInformationDV=");
		builder.append(lifecycleInformationDV);
		builder.append(", spatialReference=");
		builder.append(spatialReference);
		builder.append(", temporalReference=");
		builder.append(temporalReference);
		builder.append("]");
		return builder.toString();
	}

}
