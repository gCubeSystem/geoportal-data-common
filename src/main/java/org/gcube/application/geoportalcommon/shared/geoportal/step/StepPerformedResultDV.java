package org.gcube.application.geoportalcommon.shared.geoportal.step;

import java.io.Serializable;

import org.gcube.application.geoportalcommon.shared.geoportal.project.LifecycleInformationDV;


/**
 * The Class StepPerformedResultDV.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Apr 11, 2024
 */
public class StepPerformedResultDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7065890375433142728L;
	
	private String projectId;
	private String profileID;
	private LifecycleInformationDV lifecycleInfoDV;

	/**
	 * Instantiates a new step performed result DV.
	 */
	public StepPerformedResultDV() {
	}

	
	/**
	 * Instantiates a new step performed result DV.
	 *
	 * @param profileID the profile ID
	 * @param projectId the project id
	 * @param lifecycleInfoDV the lifecycle info DV
	 */
	public StepPerformedResultDV(String profileID, String projectId, LifecycleInformationDV lifecycleInfoDV) {
		super();
		this.projectId = projectId;
		this.profileID = profileID;
		this.lifecycleInfoDV = lifecycleInfoDV;
	}

	/**
	 * Gets the project id.
	 *
	 * @return the project id
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * Gets the profile ID.
	 *
	 * @return the profile ID
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * Gets the lifecycle info DV.
	 *
	 * @return the lifecycle info DV
	 */
	public LifecycleInformationDV getLifecycleInfoDV() {
		return lifecycleInfoDV;
	}

	/**
	 * Sets the project id.
	 *
	 * @param projectId the new project id
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Sets the profile ID.
	 *
	 * @param profileID the new profile ID
	 */
	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}

	/**
	 * Sets the lifecycle info DV.
	 *
	 * @param lifecycleInfoDV the new lifecycle info DV
	 */
	public void setLifecycleInfoDV(LifecycleInformationDV lifecycleInfoDV) {
		this.lifecycleInfoDV = lifecycleInfoDV;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StepPerformedResultDV [projectId=");
		builder.append(projectId);
		builder.append(", profileID=");
		builder.append(profileID);
		builder.append(", lifecycleInfoDV=");
		builder.append(lifecycleInfoDV);
		builder.append("]");
		return builder.toString();
	}

}
