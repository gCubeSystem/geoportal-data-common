package org.gcube.application.geoportalcommon.shared.geoportal;

/**
 * The Enum WORKFLOW_PHASE.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jan 19, 2023
 */
public enum WORKFLOW_PHASE {

	PUBLISHED("Published"), //Should be always the last one
	DRAFT("Draft");

	private String label;

	/**
	 * Instantiates a new workflow phase.
	 *
	 * @param label the label
	 */
	WORKFLOW_PHASE(String label) {
		this.label = label;
	}

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

}
