/**
 *
 */
package org.gcube.application.geoportalcommon.shared;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;

/**
 * The Class ResultSetPaginatedData.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Aug 6, 2021
 */
public class ResultSetPaginatedData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6800997954077785719L;
	private List<? extends DocumentDV> data;
	private int offset = 0;
	private int limit;
	private boolean isServerSearchFinished = false;
	private long totalItems;

	/**
	 * Instantiates a new searched folder.
	 */
	public ResultSetPaginatedData() {
	}

	/**
	 * Instantiates a new result set paginated data.
	 *
	 * @param offset                 the offset
	 * @param limit                  the limit
	 * @param isServerSearchFinished the is server search finished
	 */
	public ResultSetPaginatedData(int offset, int limit, boolean isServerSearchFinished) {

		this.offset = offset;
		this.limit = limit;
		this.isServerSearchFinished = isServerSearchFinished;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public List<? extends DocumentDV> getData() {
		return data;
	}

	/**
	 * Gets the client start index.
	 *
	 * @return the client start index
	 */
	public int getClientStartIndex() {
		return offset;
	}

	/**
	 * Gets the limit.
	 *
	 * @return the limit
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * Checks if is server search finished.
	 *
	 * @return true, if is server search finished
	 */
	public boolean isServerSearchFinished() {
		return isServerSearchFinished;
	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(List<ResultDocumentDV> data) {
		this.data = data;
	}

	/**
	 * Sets the client start index.
	 *
	 * @param clientStartIndex the new client start index
	 */
	public void setClientStartIndex(int clientStartIndex) {
		this.offset = clientStartIndex;
	}

	/**
	 * Sets the limit.
	 *
	 * @param limit the new limit
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * Sets the server search finished.
	 *
	 * @param isServerSearchFinished the new server search finished
	 */
	public void setServerSearchFinished(boolean isServerSearchFinished) {
		this.isServerSearchFinished = isServerSearchFinished;
	}

	/**
	 * Gets the total items.
	 *
	 * @return the total items
	 */
	public long getTotalItems() {
		return totalItems;
	}

	/**
	 * Sets the total items.
	 *
	 * @param totalItems the new total items
	 */
	public void setTotalItems(long totalItems) {
		this.totalItems = totalItems;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Gets the offset.
	 *
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * Sets the offset.
	 *
	 * @param offset the new offset
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResultSetPaginatedData [data=");
		builder.append(data);
		builder.append(", offset=");
		builder.append(offset);
		builder.append(", limit=");
		builder.append(limit);
		builder.append(", isServerSearchFinished=");
		builder.append(isServerSearchFinished);
		builder.append(", totalItems=");
		builder.append(totalItems);
		builder.append("]");
		return builder.toString();
	}

}
