package org.gcube.application.geoportalcommon.shared.exception;

@SuppressWarnings("serial")
public class GNAConfigException extends Exception {
	 public GNAConfigException(String message) {
	    super(message);
	  }
}