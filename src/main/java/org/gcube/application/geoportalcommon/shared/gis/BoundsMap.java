package org.gcube.application.geoportalcommon.shared.gis;

import java.io.Serializable;

/**
 * The Class BoundsMap.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 27, 2020
 */
public class BoundsMap implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 69005924452960006L;

	/** The lower left X. */
	private double lowerLeftX = 0.0;
	
	/** The lower left Y. */
	private double lowerLeftY = 0.0;
	
	/** The upper right X. */
	private double upperRightX = 0.0;
	
	/** The upper right Y. */
	private double upperRightY = 0.0;
	
	/** The crs. */
	private String crs = "";
	

	
	/**
	 * Instantiates a new bounds map.
	 */
	public BoundsMap() {
	}
		
	/**
	 * Instantiates a new bounds map.
	 *
	 * @param lowerLeftX the lower left X
	 * @param lowerLeftY the lower left Y
	 * @param upperRightX the upper right X
	 * @param upperRightY the upper right Y
	 * @param crs the crs
	 */
	public BoundsMap(double lowerLeftX, double lowerLeftY, double upperRightX,
			double upperRightY, String crs) {
		super();
		this.lowerLeftX = lowerLeftX;
		this.lowerLeftY = lowerLeftY;
		this.upperRightX = upperRightX;
		this.upperRightY = upperRightY;
		this.crs = crs;
	}

	/**
	 * Gets the lower left X.
	 *
	 * @return the lower left X
	 */
	public double getLowerLeftX() {
		return lowerLeftX;
	}

	/**
	 * Sets the lower left X.
	 *
	 * @param lowerLeftX the new lower left X
	 */
	public void setLowerLeftX(double lowerLeftX) {
		this.lowerLeftX = lowerLeftX;
	}

	/**
	 * Gets the lower left Y.
	 *
	 * @return the lower left Y
	 */
	public double getLowerLeftY() {
		return lowerLeftY;
	}

	/**
	 * Sets the lower left Y.
	 *
	 * @param lowerLeftY the new lower left Y
	 */
	public void setLowerLeftY(double lowerLeftY) {
		this.lowerLeftY = lowerLeftY;
	}

	/**
	 * Gets the upper right X.
	 *
	 * @return the upper right X
	 */
	public double getUpperRightX() {
		return upperRightX;
	}

	/**
	 * Sets the upper right X.
	 *
	 * @param upperRightX the new upper right X
	 */
	public void setUpperRightX(double upperRightX) {
		this.upperRightX = upperRightX;
	}

	/**
	 * Gets the upper right Y.
	 *
	 * @return the upper right Y
	 */
	public double getUpperRightY() {
		return upperRightY;
	}

	/**
	 * Sets the upper right Y.
	 *
	 * @param upperRightY the new upper right Y
	 */
	public void setUpperRightY(double upperRightY) {
		this.upperRightY = upperRightY;
	}

	/**
	 * Gets the crs.
	 *
	 * @return the crs
	 */
	public String getCrs() {
		return crs;
	}

	/**
	 * Sets the crs.
	 *
	 * @param crs the new crs
	 */
	public void setCrs(String crs) {
		this.crs = crs;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BoundsMap [lowerLeftX=");
		builder.append(lowerLeftX);
		builder.append(", lowerLeftY=");
		builder.append(lowerLeftY);
		builder.append(", upperRightX=");
		builder.append(upperRightX);
		builder.append(", upperRightY=");
		builder.append(upperRightY);
		builder.append(", crs=");
		builder.append(crs);
		builder.append("]");
		return builder.toString();
	}
	
	
}
