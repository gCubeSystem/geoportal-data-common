package org.gcube.application.geoportalcommon.shared.geoportal.config.layers;

public interface LayerIDV {
	
	String getName();
	String getTitle();
	String getWMS_URL();
	String getWFS_URL();

}
