package org.gcube.application.geoportalcommon.shared.geoportal;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.geojson.GeoJSON;
import org.gcube.application.geoportalcommon.shared.geoportal.project.BasicLifecycleInformationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.PublicationInfoDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.RelationshipDV;

public class ResultDocumentDV extends DocumentDV implements Serializable {

	private String id;
	private String profileID;
	private BasicLifecycleInformationDV lifecycleInfo;
	private PublicationInfoDV publicationInfoDV;
	private List<RelationshipDV> listRelationshipDV;

	private GeoJSON spatialReference;
	
	private WORKFLOW_PHASE worflowPhase; //never used. Just for serialization in GWT

	/**
	 * 
	 */
	private static final long serialVersionUID = -7209592503036632772L;

	public ResultDocumentDV() {

	}

	public String getId() {
		return id;
	}

	public String getProfileID() {
		return profileID;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}

	public void addItemToMap(String property, Object value) {

		if (documentAsMap == null)
			documentAsMap = new LinkedHashMap<String, Object>();

		documentAsMap.put(property, value);
	}

	public BasicLifecycleInformationDV getLifecycleInfo() {
		return lifecycleInfo;
	}

	public void setLifecycleInfo(BasicLifecycleInformationDV lifecycleInfo) {
		this.lifecycleInfo = lifecycleInfo;
	}

	public void setListRelationship(List<RelationshipDV> listRelationshipDV) {
		this.listRelationshipDV = listRelationshipDV;
	}

	public void setPublicationInfoDV(PublicationInfoDV publicationInfoDV) {
		this.publicationInfoDV = publicationInfoDV;
	}

	public void setListRelationshipDV(List<RelationshipDV> listRelationshipDV) {
		this.listRelationshipDV = listRelationshipDV;
	}

	public List<RelationshipDV> getListRelationshipDV() {
		return listRelationshipDV;
	}

	public void setPublicationInfo(PublicationInfoDV publicationInfoDV) {
		this.publicationInfoDV = publicationInfoDV;

	}

	public PublicationInfoDV getPublicationInfoDV() {
		return publicationInfoDV;
	}

	public GeoJSON getSpatialReference() {
		return spatialReference;
	}

	public void setSpatialReference(GeoJSON spatialReference) {
		this.spatialReference = spatialReference;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResultDocumentDV [id=");
		builder.append(id);
		builder.append(", profileID=");
		builder.append(profileID);
		builder.append(", lifecycleInfo=");
		builder.append(lifecycleInfo);
		builder.append(", publicationInfoDV=");
		builder.append(publicationInfoDV);
		builder.append(", listRelationshipDV=");
		builder.append(listRelationshipDV);
		builder.append(", spatialReference=");
		builder.append(spatialReference);
		builder.append("]");
		return builder.toString();
	}

}
