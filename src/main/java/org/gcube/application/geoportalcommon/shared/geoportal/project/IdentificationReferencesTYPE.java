package org.gcube.application.geoportalcommon.shared.geoportal.project;


/**
 * The Enum IdentificationReferencesTYPE.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Oct 26, 2022
 */
public enum IdentificationReferencesTYPE {

	SPATIAL_REFERENCE("SPATIAL REFERENCE");

		String type;

	/**
	 * Instantiates a new identification references TYPE.
	 *
	 * @param type the type
	 */
	IdentificationReferencesTYPE(String type) {
		this.type = type;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

}
