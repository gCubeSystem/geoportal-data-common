package org.gcube.application.geoportalcommon.shared.geoportal.ucd;

public enum GEOPORTAL_CONFIGURATION_TYPE {
	item_fields("itemFields"), 
	gcube_profiles("gcubeProfiles"), 
	actions_definition("actionsDefinition"), 
	grouped_overlay_layers("grouped_overlay_layers"),
	grouped_cross_filtering_layers("grouped_cross_filtering_layers");

	String id;

	/**
	 * Instantiates a new gna config managed.
	 *
	 * @param id the id
	 */
	GEOPORTAL_CONFIGURATION_TYPE(String id) {
		this.id = id;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return id;
	}

}