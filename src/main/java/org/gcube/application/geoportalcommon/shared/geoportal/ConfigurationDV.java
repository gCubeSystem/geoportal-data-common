package org.gcube.application.geoportalcommon.shared.geoportal;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.config.GeoportalConfigurationID;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_CONFIGURATION_TYPE;

public class ConfigurationDV<T extends List<? extends GeoportalConfigurationID>> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4836713840177636940L;

	private T configuration;
	private GEOPORTAL_CONFIGURATION_TYPE configurationType;

	public ConfigurationDV() {
	}

	public ConfigurationDV(T configuration) {
		this.configuration = configuration;
	}

	public GEOPORTAL_CONFIGURATION_TYPE getConfigurationType() {
		return configurationType;
	}

	public void setConfigurationType(GEOPORTAL_CONFIGURATION_TYPE configurationType) {
		this.configurationType = configurationType;
	}

	public T getConfiguration() {
		return configuration;
	}

	public void setConfiguration(T config) {
		this.configuration = config;

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfigurationDV [configuration=");
		builder.append(configuration);
		builder.append(", configurationType=");
		builder.append(configurationType);
		builder.append("]");
		return builder.toString();
	}

	
}
