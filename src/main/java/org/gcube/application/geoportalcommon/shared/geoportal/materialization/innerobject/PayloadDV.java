package org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject;

import java.io.Serializable;

public class PayloadDV implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1638565659484691126L;

	private String mimetype;

	private String storageID;

	private String link;

	private String name;

	public PayloadDV() {

	}

	public String getMimetype() {
		return mimetype;
	}

	public String getStorageID() {
		return storageID;
	}

	public String getLink() {
		return link;
	}

	public String getName() {
		return name;
	}

	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}

	public void setStorageID(String storageID) {
		this.storageID = storageID;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Payload [mimetype=");
		builder.append(mimetype);
		builder.append(", storageID=");
		builder.append(storageID);
		builder.append(", link=");
		builder.append(link);
		builder.append(", name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}

}
