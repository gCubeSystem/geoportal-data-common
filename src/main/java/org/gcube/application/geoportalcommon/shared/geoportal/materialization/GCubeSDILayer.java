package org.gcube.application.geoportalcommon.shared.geoportal.materialization;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.BBOXDV;

import com.fasterxml.jackson.annotation.JsonProperty;


public class GCubeSDILayer implements Serializable {

	// TODO manage heterogeneus collection

	/**
	 * 
	 */
	private static final long serialVersionUID = 5317964084778336268L;
	@JsonProperty(value = "_type")
	private String type;
	@JsonProperty(value = "_platformInfo")
	private List<GeoServerPlatformInfoDV> platformInfos;
	@JsonProperty(value = "_bbox")
	private BBOXDV bbox;
	@JsonProperty(value = "_ogcLinks")
	private HashMap<String, String> ogcLinks;

	public GCubeSDILayer() {

	}

	public String getType() {
		return type;
	}

	public List<GeoServerPlatformInfoDV> getPlatformInfos() {
		return platformInfos;
	}

	public BBOXDV getBbox() {
		return bbox;
	}

	public HashMap<String, String> getOgcLinks() {
		return ogcLinks;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setPlatformInfos(List<GeoServerPlatformInfoDV> platformInfos) {
		this.platformInfos = platformInfos;
	}

	public void setBbox(BBOXDV bbox) {
		this.bbox = bbox;
	}

	public void setOgcLinks(HashMap<String, String> ogcLinks) {
		this.ogcLinks = ogcLinks;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GCubeSDILayer [type=");
		builder.append(type);
		builder.append(", platformInfos=");
		builder.append(platformInfos);
		builder.append(", bbox=");
		builder.append(bbox);
		builder.append(", ogcLinks=");
		builder.append(ogcLinks);
		builder.append("]");
		return builder.toString();
	}

}
