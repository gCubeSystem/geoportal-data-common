package org.gcube.application.geoportalcommon.shared.geoportal.config;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.config.layers.LayerIDV;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class GroupedLayersDV.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 12, 2023
 */
public class GroupedLayersDV<T extends LayerIDV> implements Serializable, GeoportalConfigurationID {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2021774489849084231L;
	private String name;
	private String description;

	@JsonProperty(value = "layers")
	private List<T> listCustomLayers;

	@Override
	public String getID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setID(String configID) {
		// TODO Auto-generated method stub

	}

	public GroupedLayersDV() {

	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public List<T> getListCustomLayers() {
		return listCustomLayers;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setListCustomLayers(List<T> listCustomLayers) {
		this.listCustomLayers = listCustomLayers;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GroupedLayersDV [name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", listCustomLayers=");
		builder.append(listCustomLayers);
		builder.append("]");
		return builder.toString();
	}

}
