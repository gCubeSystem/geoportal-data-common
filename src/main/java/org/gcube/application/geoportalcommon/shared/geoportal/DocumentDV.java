package org.gcube.application.geoportalcommon.shared.geoportal;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class DocumentDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4978517506036855883L;
	protected LinkedHashMap<String, Object> documentAsMap;
	private String documentAsJSON;
	private ConfigurationDV<?> configuration;

	private String projectID;

	public DocumentDV() {
	}

	public LinkedHashMap<String, Object> getDocumentAsMap() {
		return documentAsMap;
	}

	public Entry<String, Object> getFirstEntryOfMap() {
		if (documentAsMap != null && documentAsMap.size() >= 1) {
			return documentAsMap.entrySet().iterator().next();
		}

		return null;
	}

	public String getProjectID() {
		return projectID;
	}

	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}

	public String getDocumentAsJSON() {
		return documentAsJSON;
	}

	public void setDocumentAsMap(LinkedHashMap<String, Object> documentAsMap) {
		this.documentAsMap = documentAsMap;
	}

	public void setDocumentAsJSON(String documentAsJSON) {
		this.documentAsJSON = documentAsJSON;
	}

	public ConfigurationDV<?> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(ConfigurationDV<?> configuration) {
		this.configuration = configuration;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DocumentDV [documentAsMap=");
		builder.append(documentAsMap);
		builder.append(", documentAsJSON=");
		builder.append(documentAsJSON);
		builder.append(", configuration=");
		builder.append(configuration);
		builder.append(", projectID=");
		builder.append(projectID);
		builder.append("]");
		return builder.toString();
	}

}
