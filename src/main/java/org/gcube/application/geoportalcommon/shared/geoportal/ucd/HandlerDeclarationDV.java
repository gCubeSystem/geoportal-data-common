package org.gcube.application.geoportalcommon.shared.geoportal.ucd;

import java.io.Serializable;

import org.gcube.application.geoportalcommon.shared.geoportal.ConfigurationDV;

/**
 * The Class HandlerDeclarationDV.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 17, 2022
 */
public class HandlerDeclarationDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3664650151266716354L;

	private String id;
	private String type;
	private String itemType; // This is the use case (e.g. Concessioni)
	private ConfigurationDV<?> configuration;
	private GEOPORTAL_DATA_HANDLER dataHandlerType;

	/**
	 * Instantiates a new handler declaration DV.
	 */
	public HandlerDeclarationDV() {
	}

	/**
	 * Instantiates a new handler declaration DV.
	 *
	 * @param id              the id
	 * @param type            the type
	 * @param itemType        the item type
	 * @param configuration   the configuration
	 * @param dataHandlerType the data handler type
	 */
	public HandlerDeclarationDV(String id, String type, String itemType,
			ConfigurationDV<?> configuration,
			GEOPORTAL_DATA_HANDLER dataHandlerType) {
		super();
		this.id = id;
		this.type = type;
		this.itemType = itemType;
		this.configuration = configuration;
		this.dataHandlerType = dataHandlerType;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Gets the item type.
	 *
	 * @return the item type
	 */
	public String getItemType() {
		return itemType;
	}

	/**
	 * Gets the configuration.
	 *
	 * @return the configuration
	 */
	public ConfigurationDV<?> getConfiguration() {
		return configuration;
	}

	/**
	 * Gets the data handler type.
	 *
	 * @return the data handler type
	 */
	public GEOPORTAL_DATA_HANDLER getDataHandlerType() {
		return dataHandlerType;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Sets the item type.
	 *
	 * @param itemType the new item type
	 */
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	/**
	 * Sets the configuration.
	 *
	 * @param configuration the new configuration
	 */
	public void setConfiguration(ConfigurationDV<?> configuration) {
		this.configuration = configuration;
	}

	/**
	 * Sets the data handler type.
	 *
	 * @param dataHandlerType the new data handler type
	 */
	public void setDataHandlerType(GEOPORTAL_DATA_HANDLER dataHandlerType) {
		this.dataHandlerType = dataHandlerType;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HandlerDeclarationDV [id=");
		builder.append(id);
		builder.append(", type=");
		builder.append(type);
		builder.append(", itemType=");
		builder.append(itemType);
		builder.append(", configuration=");
		builder.append(configuration);
		builder.append(", dataHandlerType=");
		builder.append(dataHandlerType);
		builder.append("]");
		return builder.toString();
	}



}
