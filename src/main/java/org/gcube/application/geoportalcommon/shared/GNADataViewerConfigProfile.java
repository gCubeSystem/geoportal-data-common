package org.gcube.application.geoportalcommon.shared;

import java.io.Serializable;

/**
 * The Class GNADataViewerConfigProfile.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 21, 2021
 */
public class GNADataViewerConfigProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2968334957258327191L;
	private String restrictedPortletURL;
	private String openPortletURL;

	/**
	 * Instantiates a new geo na data viewer profile.
	 */
	public GNADataViewerConfigProfile() {

	}

	/**
	 * Gets the restricted portlet URL.
	 *
	 * @return the restricted portlet URL
	 */
	public String getRestrictedPortletURL() {
		return restrictedPortletURL;
	}

	/**
	 * Sets the restricted portlet URL.
	 *
	 * @param restrictedPortletURL the new restricted portlet URL
	 */
	public void setRestrictedPortletURL(String restrictedPortletURL) {
		this.restrictedPortletURL = restrictedPortletURL;
	}

	/**
	 * Gets the open portlet URL.
	 *
	 * @return the open portlet URL
	 */
	public String getOpenPortletURL() {
		return openPortletURL;
	}

	/**
	 * Sets the open portlet URL.
	 *
	 * @param openPortletURL the new open portlet URL
	 */
	public void setOpenPortletURL(String openPortletURL) {
		this.openPortletURL = openPortletURL;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GNADataViewerConfigProfile [restrictedPortletURL=");
		builder.append(restrictedPortletURL);
		builder.append(", openPortletURL=");
		builder.append(openPortletURL);
		builder.append("]");
		return builder.toString();
	}
	
}
