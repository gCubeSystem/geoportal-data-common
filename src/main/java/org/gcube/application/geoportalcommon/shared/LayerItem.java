package org.gcube.application.geoportalcommon.shared;

import java.io.Serializable;


/**
 * The Class LayerItem.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 1, 2020
 */
public class LayerItem implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 3261972168416698955L;

	/** The name. */
	private String name;

	/** The url. */
	private String wmsLink;
	
	private String geonaType;
	
	public LayerItem() {
		
	}

	public LayerItem(String name, String wmsLink, String geonaType) {
		super();
		this.name = name;
		this.wmsLink = wmsLink;
		this.geonaType = geonaType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWmsLink() {
		return wmsLink;
	}

	public void setWmsLink(String wmsLink) {
		this.wmsLink = wmsLink;
	}

	public String getGeonaType() {
		return geonaType;
	}

	public void setGeonaType(String geonaType) {
		this.geonaType = geonaType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LayerItem [name=");
		builder.append(name);
		builder.append(", wmsLink=");
		builder.append(wmsLink);
		builder.append(", geonaType=");
		builder.append(geonaType);
		builder.append("]");
		return builder.toString();
	}

	
}