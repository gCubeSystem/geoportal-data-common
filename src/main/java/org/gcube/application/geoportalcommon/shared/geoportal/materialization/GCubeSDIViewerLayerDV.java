package org.gcube.application.geoportalcommon.shared.geoportal.materialization;

import java.io.Serializable;
import java.util.HashMap;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.BBOXDV;

public class GCubeSDIViewerLayerDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6620710416056942397L;
	private String type;
	private BBOXDV bbox;
	private HashMap<String, String> ogcLinks;
	private String layerName;

	public GCubeSDIViewerLayerDV() {

	}

	public String getType() {
		return type;
	}

	public BBOXDV getBbox() {
		return bbox;
	}

	public HashMap<String, String> getOgcLinks() {
		return ogcLinks;
	}

	public String getWMSLink() {
		if (ogcLinks != null) {
			return ogcLinks.get("wms");
		}

		return null;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setBbox(BBOXDV bbox) {
		this.bbox = bbox;
	}

	public void setOgcLinks(HashMap<String, String> ogcLinks) {
		this.ogcLinks = ogcLinks;
	}

	public void setLayerName(String layerName) {
		this.layerName = layerName;
	}

	public String getLayerName() {
		return layerName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GCubeSDIViewerLayerDV [type=");
		builder.append(type);
		builder.append(", bbox=");
		builder.append(bbox);
		builder.append(", ogcLinks=");
		builder.append(ogcLinks);
		builder.append(", layerName=");
		builder.append(layerName);
		builder.append("]");
		return builder.toString();
	}

}
