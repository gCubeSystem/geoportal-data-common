package org.gcube.application.geoportalcommon.shared.geoportal.project;

import java.io.Serializable;

public class RelationshipDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8295671124305773593L;
	private String relationshipName;
	private String targetID;
	private String asJSON;
	private String targetUCD;

	public RelationshipDV() {

	}

	public RelationshipDV(String relationshipName, String targetID, String targetUCD, String asJSON) {
		super();
		this.relationshipName = relationshipName;
		this.targetID = targetID;
		this.targetUCD = targetUCD;
		this.asJSON = asJSON;	
	}

	public String getTargetUCD() {
		return targetUCD;
	}

	public void setTargetUCD(String targetUCD) {
		this.targetUCD = targetUCD;
	}

	public String getTargetID() {
		return targetID;
	}

	public void setTargetID(String targetID) {
		this.targetID = targetID;
	}

	public String getRelationshipName() {
		return relationshipName;
	}

	public void setRelationshipName(String relationshipName) {
		this.relationshipName = relationshipName;
	}

	public String getAsJSON() {
		return asJSON;
	}

	public void setAsJSON(String asJSON) {
		this.asJSON = asJSON;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RelationshipDV [relationshipName=");
		builder.append(relationshipName);
		builder.append(", targetID=");
		builder.append(targetID);
		builder.append(", asJSON=");
		builder.append(asJSON);
		builder.append(", targetUCD=");
		builder.append(targetUCD);
		builder.append("]");
		return builder.toString();
	}

}
