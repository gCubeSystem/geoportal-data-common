package org.gcube.application.geoportalcommon.shared.geoportal.project;

import java.io.Serializable;

/**
 * The Class BasicLifecycleInformationDV.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Aug 10, 2022
 */
public class BasicLifecycleInformationDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2301238401365921132L;

	/**
	 * The Enum Status.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Mar 21, 2022
	 */
	public static enum Status {
		OK("Success"), ERROR("Error"), WARNING("Warning"), NOT_SPECIFIED("Not specified");

		private String label;

		/**
		 * Instantiates a new status.
		 *
		 * @param label the label
		 */
		private Status(String label) {
			this.label = label;
		}

		/**
		 * Gets the label.
		 *
		 * @return the label
		 */
		public String getLabel() {
			return label;
		}
	}

	protected String phase;
	protected Status lastOperationStatus;

	/**
	 * Instantiates a new basic lifecycle information DV.
	 */
	public BasicLifecycleInformationDV() {

	}

	/**
	 * Gets the phase.
	 *
	 * @return the phase
	 */
	public String getPhase() {
		return phase;
	}

	/**
	 * Gets the last operation status.
	 *
	 * @return the last operation status
	 */
	public Status getLastOperationStatus() {
		return lastOperationStatus;
	}

	/**
	 * Sets the phase.
	 *
	 * @param phase the new phase
	 */
	public void setPhase(String phase) {
		this.phase = phase;
	}

	/**
	 * Sets the last operation status.
	 *
	 * @param lastOperationStatus the new last operation status
	 */
	public void setLastOperationStatus(Status lastOperationStatus) {
		this.lastOperationStatus = lastOperationStatus;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BasicLifecycleInformationDV [phase=");
		builder.append(phase);
		builder.append(", lastOperationStatus=");
		builder.append(lastOperationStatus);
		builder.append("]");
		return builder.toString();
	}

}
