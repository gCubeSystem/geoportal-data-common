package org.gcube.application.geoportalcommon.shared;

import java.io.Serializable;

/**
 * The Class GeoportalItemReferences.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Oct 18, 2022
 */
public class GeoportalItemReferences implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7021431511279022193L;

	/**
	 * The Enum SHARE_LINK_TO.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 * Apr 5, 2024
	 */
	public static enum SHARE_LINK_TO {
		DATA_VIEWER, DATA_ENTRY
	}

	// this is the mongoID
	private String projectID;
	private String profileID; // this is the profileID - UCD
	private String itemName;

	private PublicLink restrictedLink;
	private PublicLink openLink;

	private String layerObjectType;

	private SHARE_LINK_TO shareLinkTo;

	/**
	 * Instantiates a new geo na object.
	 */
	public GeoportalItemReferences() {

	}
	
	/**
	 * Instantiates a new geoportal item references. 
	 * Backward compatibility. Use {{@link #GeoportalItemReferences(String, String, SHARE_LINK_TO)}}
	 *
	 * @param projectID the project ID
	 * @param profileID the profile ID
	 */
	@Deprecated
	public GeoportalItemReferences(String projectID, String profileID) {
		super();
		this.projectID = projectID;
		this.profileID = profileID;
	}

	/**
	 * Instantiates a new geo na item ref.
	 *
	 * @param projectID   the project ID
	 * @param profileID   the profile ID
	 * @param shareLinkTo the share link to
	 */
	public GeoportalItemReferences(String projectID, String profileID, SHARE_LINK_TO shareLinkTo) {
		super();
		this.projectID = projectID;
		this.profileID = profileID;
		this.shareLinkTo = shareLinkTo;
	}

	/**
	 * Instantiates a new geo na item ref.
	 *
	 * @param projectID       the project ID
	 * @param profileID       the profile ID
	 * @param layerObjectType the layer object type
	 */
	public GeoportalItemReferences(String projectID, String profileID, String layerObjectType) {
		super();
		this.projectID = projectID;
		this.profileID = profileID;
		this.layerObjectType = layerObjectType;
	}

	/**
	 * Gets the profile ID.
	 *
	 * @return the profile ID
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * Gets the project ID.
	 *
	 * @return the project ID
	 */
	public String getProjectID() {
		return projectID;
	}

	/**
	 * Gets the layer object type.
	 *
	 * @return the layer object type
	 */
	public String getLayerObjectType() {
		return layerObjectType;
	}

	/**
	 * Gets the item name.
	 *
	 * @return the item name
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * Gets the share link to.
	 *
	 * @return the share link to
	 */
	public SHARE_LINK_TO getShareLinkTo() {
		return shareLinkTo;
	}

	/**
	 * Sets the item name.
	 *
	 * @param itemName the new item name
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	/**
	 * Gets the restricted link.
	 *
	 * @return the restricted link
	 */
	public PublicLink getRestrictedLink() {
		return restrictedLink;
	}

	/**
	 * Sets the restricted link.
	 *
	 * @param restrictedLink the new restricted link
	 */
	public void setRestrictedLink(PublicLink restrictedLink) {
		this.restrictedLink = restrictedLink;
	}

	/**
	 * Gets the open link.
	 *
	 * @return the open link
	 */
	public PublicLink getOpenLink() {
		return openLink;
	}

	/**
	 * Sets the open link.
	 *
	 * @param openLink the new open link
	 */
	public void setOpenLink(PublicLink openLink) {
		this.openLink = openLink;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GeoportalItemReferences [projectID=");
		builder.append(projectID);
		builder.append(", profileID=");
		builder.append(profileID);
		builder.append(", itemName=");
		builder.append(itemName);
		builder.append(", restrictedLink=");
		builder.append(restrictedLink);
		builder.append(", openLink=");
		builder.append(openLink);
		builder.append(", layerObjectType=");
		builder.append(layerObjectType);
		builder.append(", shareLinkTo=");
		builder.append(shareLinkTo);
		builder.append("]");
		return builder.toString();
	}
}
