package org.gcube.application.geoportalcommon.shared.config;


/**
 * The Enum OPERATION_ON_ITEM.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Sep 13, 2022
 */
public enum OPERATION_ON_ITEM {

	CREATE_NEW_PROJECT("new", "Create New Project"),
	VIEW_PROJECT_AS_DOCUMENT("vpd", "View Project as Document"),
	VIEW_PROJECT_AS_JSON("vpj", "View Projet as JSON"),
	VIEW_ON_MAP("vpm","View on Map"),
	GET_SHAREABLE_LINK("shl","Get Shareable Link"),
	VIEW_REPORT("vpr","View the Report"),
	EDIT_PROJECT("edt","Edit the Project"),
	CLONE_PROJECT("cln","Clone the Project"),
	PUBLISH_UNPUBLISH_PROJECT("pup","Publish/UnPublish the Project"),
	DELETE_PROJECT("dlt","Delete the Project"),
	CREATE_RELATION("crr","Create Relation between two Projects"),
	DELETE_RELATION("dlr","Delete Relation between two Projects"),
	VIEW_RELATIONSHIPS("vpr", "View the relationship/s created for the Project");
	
	String label;
	
	/**
	 * Instantiates a new operation on item.
	 *
	 * @param label the label
	 */
	OPERATION_ON_ITEM(String id, String label){
		this.label = label;
	}
	
	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

}
