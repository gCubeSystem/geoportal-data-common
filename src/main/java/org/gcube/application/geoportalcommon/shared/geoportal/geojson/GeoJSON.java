package org.gcube.application.geoportalcommon.shared.geoportal.geojson;

import java.io.Serializable;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.BBOXDV;

public class GeoJSON implements Serializable {

	public static final String TYPE = "type";
	public static final String BBOX = "bbox";
	public static final String CRS = "crs";
	public static final String COORDINATES = "coordinates";
	/**
	 * 
	 */
	private static final long serialVersionUID = -7798331554142534921L;

	private String type;
	private Crs crs;
	private BBOXDV bbox;
	private String geoJSON;

	public GeoJSON() {

	}

	public String getType() {
		return type;
	}

	public Crs getCrs() {
		return crs;
	}

	public BBOXDV getBbox() {
		return bbox;
	}

	public String getGeoJSON() {
		return geoJSON;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setCrs(Crs crs) {
		this.crs = crs;
	}

	public void setBbox(BBOXDV bbox) {
		this.bbox = bbox;
	}

	public void setGeoJSON(String geoJSON) {
		this.geoJSON = geoJSON;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GeoJSON [type=");
		builder.append(type);
		builder.append(", crs=");
		builder.append(crs);
		builder.append(", bbox=");
		builder.append(bbox);
		builder.append(", geoJSON=");
		builder.append(geoJSON);
		builder.append("]");
		return builder.toString();
	}

}
