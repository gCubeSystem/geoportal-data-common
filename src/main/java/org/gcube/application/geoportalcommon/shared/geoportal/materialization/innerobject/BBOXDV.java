package org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject;

import java.io.Serializable;
import java.util.HashMap;

public class BBOXDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4438327945612263171L;

	public static final String MAX_X = "_maxX";
	public static final String MAX_Y = "_maxY";
	public static final String MAX_Z = "_maxZ";
	public static final String MIN_X = "_minX";
	public static final String MIN_Y = "_minY";
	public static final String MIN_Z = "_minZ";

	public static final BBOXDV WORLD = new BBOXDV(180d, -180d, 90d, -90d);

	public static final BBOXDV WORLD_3D = new BBOXDV(180d, -180d, 90d, -90d);

	private HashMap<String, Double> bbox = new HashMap<String, Double>();

	public BBOXDV() {

	}

	public BBOXDV(Double maxX, Double minX, Double maxY, Double minY) {
		setMaxX(maxX);
		setMinX(minX);
		setMaxY(maxY);
		setMinY(minY);
	}

	public BBOXDV(Double maxX, Double minX, Double maxY, Double minY, Double maxZ, Double minZ) {
		this(maxX, minX, maxY, minY);
		setMaxZ(maxZ);
		setMinZ(minZ);
	}

	public BBOXDV(HashMap<String, Object> hash) {
		setMaxX(toDouble(hash.get(MAX_X)));
		setMaxY(toDouble(hash.get(MAX_Y)));
		setMinX(toDouble(hash.get(MIN_X)));
		setMinY(toDouble(hash.get(MIN_Y)));

		setMinZ(toDouble(hash.get(MIN_Z)));
		setMaxZ(toDouble(hash.get(MAX_Z)));
	}

	private Double toDouble(Object toDouble) {

		try {
			return new Double((double) toDouble);

		} catch (Exception e) {
			// System.out.println(toDouble + " is not castable to " +
			// Double.class.getSimpleName());
		}

		return null;

	}

	public static final BBOXDV fromGeoJSON(double[] coords) {
		BBOXDV toReturn = new BBOXDV();
		toReturn.setMaxX(coords[0]);
		toReturn.setMinY(coords[1]);

		if (coords.length == 6) {
			// 3D
			toReturn.setMinZ(coords[2]);
			toReturn.setMinX(coords[3]);
			toReturn.setMaxY(coords[4]);
			toReturn.setMaxZ(coords[5]);
		} else {
			toReturn.setMinX(coords[2]);
			toReturn.setMaxY(coords[3]);
		}
		return toReturn;
	}

	public BBOXDV setMaxX(Double d) {
		this.bbox.put(MAX_X, d);
		return this;
	}

	public BBOXDV setMaxY(Double d) {
		this.bbox.put(MAX_Y, d);
		return this;
	}

	public BBOXDV setMaxZ(Double d) {
		this.bbox.put(MAX_Z, d);
		return this;
	}

	public BBOXDV setMinX(Double d) {
		this.bbox.put(MIN_X, d);
		return this;
	}

	public BBOXDV setMinY(Double d) {
		this.bbox.put(MIN_Y, d);
		return this;
	}

	public BBOXDV setMinZ(Double d) {
		this.bbox.put(MIN_Z, d);
		return this;
	}

	public Double getMinY() {
		try {
			return this.bbox.get(MIN_Y);
		} catch (Exception e) {

		}
		return new Double(-90d);
	}

	public Double getMaxY() {
		try {
			return this.bbox.get(MAX_Y);
		} catch (Exception e) {

		}
		return new Double(90d);
	}

	public Double getMinX() {
		try {
			return this.bbox.get(MIN_X);
		} catch (Exception e) {

		}
		return new Double(-180d);
	}

	public Double getMaxX() {
		try {
			return this.bbox.get(MAX_X);
		} catch (Exception e) {

		}
		return new Double(180d);
	}

	public Double getMinZ() {
		return (Double) this.bbox.getOrDefault(MIN_Z, null);
	}

	public Double getMaxZ() {
		return (Double) this.bbox.getOrDefault(MAX_Z, null);
	}

	public Boolean is3d() {
		return getMinZ() != null && getMaxZ() != null;
	}

	public final String asGeoJSONBBox() {
		StringBuilder builder = new StringBuilder("[");
		builder.append(getMaxX() + ","); // W
		builder.append(getMinY() + ","); // S
		if (is3d())
			builder.append(getMinZ() + ","); // Z

		builder.append(getMinX() + ","); // E
		builder.append(getMaxY() + ","); // N
		if (is3d())
			builder.append(getMaxZ() + ","); // Z

		builder.deleteCharAt(builder.length());
		builder.append("]");
		return builder.toString();
	}

	public double[] asGeoJSONArray() {
		if (is3d()) {
			return new double[] { getMaxX(), getMinY(), getMinZ(), getMinX(), getMaxY(), getMaxZ() };
		} else
			return new double[] { getMaxX(), getMinY(), getMinX(), getMaxY() };
	}
}