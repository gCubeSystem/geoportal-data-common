package org.gcube.application.geoportalcommon.shared.geoportal.view;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.GCubeSDIViewerLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.FilesetDV;

public class SubDocumentView implements Serializable, CheckEmpty {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3563782450418275140L;

	private String metadataAsJSON;

	private List<FilesetDV> listFiles;
	private List<FilesetDV> listImages;
	private List<GCubeSDIViewerLayerDV> listLayers;

	public SubDocumentView() {

	}

	@Override
	public Boolean isEmpty() {
		
		if (metadataAsJSON != null && !metadataAsJSON.isEmpty())
			return false;

		if (listFiles != null && !listFiles.isEmpty())
			return false;

		if (listImages != null && !listImages.isEmpty())
			return false;

		if (listLayers != null && !listLayers.isEmpty())
			return false;

		return true;
	}

	public String getMetadataAsJSON() {
		return metadataAsJSON;
	}

	public List<FilesetDV> getListFiles() {
		return listFiles;
	}

	public List<FilesetDV> getListImages() {
		return listImages;
	}

	public List<GCubeSDIViewerLayerDV> getListLayers() {
		return listLayers;
	}

	public void setMetadataAsJSON(String metadataAsJSON) {
		this.metadataAsJSON = metadataAsJSON;
	}

	public void setListFiles(List<FilesetDV> listFiles) {
		this.listFiles = listFiles;
	}

	public void setListImages(List<FilesetDV> listImages) {
		this.listImages = listImages;
	}

	public void setListLayers(List<GCubeSDIViewerLayerDV> listLayers) {
		this.listLayers = listLayers;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SubDocumentView [metadataAsJSON=");
		builder.append(metadataAsJSON);
		builder.append(", listFiles=");
		builder.append(listFiles);
		builder.append(", listImages=");
		builder.append(listImages);
		builder.append(", listLayers=");
		builder.append(listLayers);
		builder.append("]");
		return builder.toString();
	}

}
