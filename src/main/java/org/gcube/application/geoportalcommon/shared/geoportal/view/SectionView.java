package org.gcube.application.geoportalcommon.shared.geoportal.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class SectionView.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Nov 11, 2022
 */
public class SectionView implements Serializable, CheckEmpty {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5360469987059239067L;

	private String sectionTitle;

	private List<SubDocumentView> listSubDocuments;

	/**
	 * Instantiates a new section view.
	 */
	public SectionView() {

	}

	/**
	 * Checks if is empty.
	 *
	 * @return true, if is empty
	 */
	@Override
	public Boolean isEmpty() {
		if (listSubDocuments == null)
			return true;

		for (SubDocumentView subDocumentView : listSubDocuments) {
			boolean isEmpty = subDocumentView.isEmpty();
			if (isEmpty)
				return true;
		}

		return false;
	}

	/**
	 * Checks for spatial layers.
	 *
	 * @return true, if successful
	 */
	public Boolean hasSpatialLayers() {
		if (listSubDocuments == null)
			return false;

		for (SubDocumentView subDocumentView : listSubDocuments) {
			boolean hasLayers = subDocumentView.getListLayers() != null && subDocumentView.getListLayers().size() > 0;
			if (hasLayers)
				return true;
		}

		return false;
	}

	/**
	 * Gets the section title.
	 *
	 * @return the section title
	 */
	public String getSectionTitle() {
		return sectionTitle;
	}

	/**
	 * Adds the sub document.
	 *
	 * @param subDocumentView the sub document view
	 */
	public void addSubDocument(SubDocumentView subDocumentView) {

		if (listSubDocuments == null)
			listSubDocuments = new ArrayList<SubDocumentView>();

		listSubDocuments.add(subDocumentView);
	}

	/**
	 * Gets the list sub documents.
	 *
	 * @return the list sub documents
	 */
	public List<SubDocumentView> getListSubDocuments() {

		if (listSubDocuments == null)
			listSubDocuments = new ArrayList<SubDocumentView>();

		return listSubDocuments;
	}

	/**
	 * Sets the section title.
	 *
	 * @param sectionTitle the new section title
	 */
	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SectionView [sectionTitle=");
		builder.append(sectionTitle);
		builder.append(", listSubDocuments=");
		builder.append(listSubDocuments);
		builder.append("]");
		return builder.toString();
	}

}
