package org.gcube.application.geoportalcommon;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Map;

import org.gcube.application.geoportalcommon.shared.GNADataEntryConfigProfile;
import org.gcube.application.geoportalcommon.shared.GNADataViewerConfigProfile;
import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences.SHARE_LINK_TO;
import org.gcube.application.geoportalcommon.shared.PublicLink;
import org.gcube.portlets.user.uriresolvermanager.UriResolverManager;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder.RESOLVE_AS;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder.TARGET_GEOPORTAL_APP;
import org.gcube.portlets.user.urlshortener.UrlShortener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GeoportalCommon.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 2, 2023
 */
public class GeoportalCommon {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(GeoportalCommon.class);

	/**
	 * Instantiates a new geoportal common.
	 */
	public GeoportalCommon() {
	}

	/**
	 * Gets the public links for.
	 *
	 * @param gcubeScope     the gcube scope
	 * @param item           the item
	 * @param createShortURL creates and returns the short URL also.
	 * @return the public links for
	 * @throws Exception the exception
	 */
	public GeoportalItemReferences getPublicLinksFor(String gcubeScope, GeoportalItemReferences item,
			boolean createShortURL) throws Exception {
		LOG.info("getPublicLinksFor called for: " + item);

		try {

			if (item == null)
				throw new Exception("Bad request, the item is null");

			if (item.getProjectID() == null)
				throw new Exception("Bad request, the ProjectID is null");

			if (item.getProfileID() == null)
				throw new Exception("Bad request, the ProfileID is null");

			UriResolverManager resolver = new UriResolverManager("GEO");

			// PRIVATE LINK
			GeoportalResolverQueryStringBuilder builder = new GeoportalResolverQueryStringBuilder(item.getProfileID(),
					item.getProjectID());
			builder.scope(gcubeScope);
			builder.resolverAs(RESOLVE_AS.PRIVATE);

			SHARE_LINK_TO shareLinkTo = item.getShareLinkTo();
			TARGET_GEOPORTAL_APP targetApp = null;
			if (shareLinkTo != null) {
			
				switch (shareLinkTo) {
				case DATA_ENTRY:
					targetApp = TARGET_GEOPORTAL_APP.GEO_DE;
					break;
				case DATA_VIEWER:
				default:
					targetApp = TARGET_GEOPORTAL_APP.GEO_DV;
					break;
				}
				
				builder.targetApp(targetApp);
			}

			// builder.resolverAs(RESOLVE_AS.PRIVATE);
			Map<String, String> params = builder.buildQueryParameters();
			String link = resolver.getLink(params, false);
			String shortUrl = link;
			try {
				if (createShortURL)
					shortUrl = getShortUrl(link);
			} catch (Exception e) {
				LOG.warn("Error on shortening the URL: ", e);
			}
			item.setRestrictedLink(new PublicLink(link, shortUrl));

			// PUBLIC LINK
			builder.resolverAs(RESOLVE_AS.PUBLIC);
			params = builder.buildQueryParameters();
			link = resolver.getLink(params, false);
			shortUrl = link;
			try {
				if (createShortURL)
					shortUrl = getShortUrl(link);
			} catch (Exception e) {
				LOG.warn("Error on shortening the URL: ", e);
			}
			item.setOpenLink(new PublicLink(link, shortUrl));

			LOG.info("returning: " + item);
			return item;

		} catch (Exception e) {
			LOG.error("Error on getPublicLinksFor for: " + item, e);
			throw new Exception("Share link not available for this item. Try later or contact the support. Error: "
					+ e.getMessage());
		}
	}

	/**
	 * Gets the short url.
	 *
	 * @param longUrl the long url
	 * @return the short url
	 * @throws Exception the exception
	 */
	public String getShortUrl(String longUrl) throws Exception {
		LOG.info("getShortUrl called for " + longUrl);

		if (longUrl == null)
			return longUrl;

		try {

			UrlShortener shortener = new UrlShortener();

			if (shortener != null && shortener.isAvailable()) {
				String toShort = longUrl;
				String[] splitted = toShort.split("\\?");
				LOG.debug("Splitted long URL is: " + Arrays.asList(splitted));
				String link = toShort;
				if (splitted.length > 1) {
					LOG.debug("Query string detected, encoding it...");
					String encodedQuery = splitted[1];
					try {
						encodedQuery = URLEncoder.encode(splitted[1], "UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();

					}
					LOG.debug("encodedQuery is: " + encodedQuery);
					link = String.format("%s?%s", splitted[0], encodedQuery);
				}
				return shortener.shorten(link);
			}
			return longUrl;

		} catch (Exception e) {
			LOG.error("Error on shortening the longURL " + longUrl, e);
			return null;
		}
	}

	/**
	 * Read GNA data viewer config.
	 *
	 * @param appID the app ID. If null uses the default
	 *              {@link GeoportalCommonConstants#GEOPORTAL_DATA_VIEWER_APP}
	 * @return the GNA data viewer config profile
	 * @throws Exception the exception
	 */
	public GNADataViewerConfigProfile readGNADataViewerConfig(String appID) throws Exception {
		LOG.info("getGeoNaDataViewProfile called for AppID: " + appID);

		if (appID == null || appID.isEmpty()) {
			appID = GeoportalCommonConstants.GEOPORTAL_DATA_VIEWER_APP;
			LOG.info("AppID is null, so using DEFAULT: " + appID);
		}

		GNADataViewerConfigProfileReader gdvp = new GNADataViewerConfigProfileReader(appID);
		GNADataViewerConfigProfile profile = gdvp.readProfileFromInfrastructure();
		LOG.info("Returning profile: " + profile);
		return profile;
	}

	/**
	 * Gets the GNA data config read from ApplicationProfile stored into IS.
	 *
	 * @return the GNA data config
	 * @throws Exception the exception
	 */
	public GNADataEntryConfigProfile readGNADataEntryConfig() throws Exception {
		LOG.info("readGNADataEntryConfig called");

		GNADataEntryConfigProfileReader gnaConfigReader = new GNADataEntryConfigProfileReader();
		GNADataEntryConfigProfile config = gnaConfigReader.readProfileFromInfrastrucure();
		return config;
	}

}
