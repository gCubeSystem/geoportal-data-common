package org.gcube.application.geoportalcommon;

/**
 * The Class GeoportalCommonConstants.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 1, 2020
 */
public class GeoportalCommonConstants {
	
	public static final String GET_GEONA_ITEM_TYPE = "git";
	public static final String GET_GEONA_ITEM_ID = "gid";
	
	public static final String GEOPORTAL_DATA_VIEWER_APP = "geoportal-data-viewer-app";
	
}
