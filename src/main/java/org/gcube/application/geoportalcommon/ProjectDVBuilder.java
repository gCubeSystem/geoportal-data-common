package org.gcube.application.geoportalcommon;

import java.util.List;

/**
 * The Class ProjectDVBuilder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 10, 2022
 */
public class ProjectDVBuilder {

	private boolean lifecycleInformation;
	private boolean spatialReference = true; //default
	private boolean temporalReference;
	private boolean relationships;
	private List<String> listDocumentKeys;
	private boolean fullDocumentMap;

	private ProjectDVBuilder() {

	}

	public static ProjectDVBuilder newBuilder() {
		return new ProjectDVBuilder();
	}

	public boolean isIncludeLifecycleInformation() {
		return lifecycleInformation;
	}

	public boolean isIncludeSpatialReference() {
		return spatialReference;
	}

	public boolean isIncludeTemporalReference() {
		return temporalReference;
	}

	public boolean isIncludeRelationships() {
		return relationships;
	}

	public List<String> getListDocumentKeys() {
		return listDocumentKeys;
	}

	public boolean isIncludeFullDocumentMap() {
		return fullDocumentMap;
	}

	public ProjectDVBuilder lifecycleInformation(boolean lifecycleInformation) {
		this.lifecycleInformation = lifecycleInformation;
		return this;
	}

	public ProjectDVBuilder spatialReference(boolean includeSpatialReference) {
		this.spatialReference = includeSpatialReference;
		return this;
	}

	public ProjectDVBuilder temporalReference(boolean includeTemporalReference) {
		this.temporalReference = includeTemporalReference;
		return this;
	}

	public ProjectDVBuilder relationships(boolean includeRelationships) {
		this.relationships = includeRelationships;
		return this;
	}

	public ProjectDVBuilder listDocumentKeys(List<String> listDocumentKeys) {
		this.listDocumentKeys = listDocumentKeys;
		return this;
	}

	public ProjectDVBuilder fullDocumentMap(boolean includeFullDocumentMap) {
		this.fullDocumentMap = includeFullDocumentMap;
		return this;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProjectDVBuilder [lifecycleInformation=");
		builder.append(lifecycleInformation);
		builder.append(", spatialReference=");
		builder.append(spatialReference);
		builder.append(", temporalReference=");
		builder.append(temporalReference);
		builder.append(", relationships=");
		builder.append(relationships);
		builder.append(", listDocumentKeys=");
		builder.append(listDocumentKeys);
		builder.append(", fullDocumentMap=");
		builder.append(fullDocumentMap);
		builder.append("]");
		return builder.toString();
	}

}
