package org.gcube.application.geoportalcommon.util;

/**
 * The Class StringUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Aug 31, 2023
 */
public class StringUtil {

	/**
	 * Ellipsize.
	 *
	 * @param input     the input
	 * @param maxLength the max length
	 * @return the string
	 */
	public static String ellipsize(String input, int maxLength) {
		String ellip = "...";
		if (input == null || input.length() <= maxLength || input.length() < ellip.length()) {
			return input;
		}
		return input.substring(0, maxLength - ellip.length()).concat(ellip);
	}

}
