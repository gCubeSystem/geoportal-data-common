package org.gcube.application.geoportalcommon.geoportal.config;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@XmlRootElement(name = "gcubeProfiles")
@Slf4j
public class GcubeProfile {
	
	private static final String FILE_PATHS = "filePaths";
	
	public static final int MIN_MAX_NOT_SPECIFIED = -1;
	
	@JsonProperty
	private String gcubeSecondaryType;
	
	@JsonProperty
	private String gcubeName;
	
	@JsonProperty
	private String parentName;
	
	@JsonProperty
	private String sectionName;
	
	@JsonProperty
	private String sectionTitle;
	
	/**
	 * 0: Not specified means Not Mandatory. The Gcube Profile is not mandatory and means that all its fields must be optional in the gCube Meta Profile.
	 * N: is the MIN number of Occurrences
	 * Default is 1 occurrence
	 */
	@JsonProperty
	private int minOccurs = 1;
	
	/**
	 * 0: Not specified means max number of Occurrences is arbitrary.
	 * N: is the MAX number of Occurrences
	 */
	@JsonProperty
	private int maxOccurs = 1;
	
	@JsonProperty(FILE_PATHS)
	private List<FilePath> filePaths;
	
	public GcubeProfile() {}
	
	

}
