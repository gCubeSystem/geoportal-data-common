package org.gcube.application.geoportalcommon.geoportal.config;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@XmlRootElement(name = "filePaths")
@Slf4j
public class FilePath {

	@JsonProperty
	String gcubeProfileFieldName;
	@JsonProperty
	String fieldDefinition;
	@JsonProperty
	String fieldName;

	public FilePath() {
	}

}
