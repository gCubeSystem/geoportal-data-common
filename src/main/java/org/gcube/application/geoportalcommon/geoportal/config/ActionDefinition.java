package org.gcube.application.geoportalcommon.geoportal.config;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@XmlRootElement(name = "actionsDefinition")
@Slf4j
public class ActionDefinition {

	@JsonProperty
	private String id;
	@JsonProperty
	private String title;
	@JsonProperty
	private String[] call_STEPS;
	@JsonProperty
	private String description;
	@JsonProperty
	private String[] display_on_phase;
	@JsonProperty
	private boolean searchable;

	private String configID;

	public ActionDefinition() {

	}

}
