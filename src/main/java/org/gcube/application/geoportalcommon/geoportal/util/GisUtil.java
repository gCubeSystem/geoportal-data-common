package org.gcube.application.geoportalcommon.geoportal.util;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.BBOXDV;
import org.json.JSONArray;

public class GisUtil {

	public static BBOXDV fromJSONArray(JSONArray outerArray) {

		try {
			if (outerArray == null)
				return new BBOXDV();

			double[] coords = new double[outerArray.length()];

			for (int i = 0; i < outerArray.length(); i++) {
				coords[i] = outerArray.getDouble(i);
			}

			return BBOXDV.fromGeoJSON(coords);
		} catch (Exception e) {
			return new BBOXDV();
		}
	}

}
