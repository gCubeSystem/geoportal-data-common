package org.gcube.application.geoportalcommon.geoportal;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.useCaseDescriptors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.HandlerDeclaration;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.RelationshipDefinition;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportal.common.rest.UseCaseDescriptorsI;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

/**
 * The Class UseCaseDescriptorCaller.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 14, 2022
 */
public class UseCaseDescriptorCaller {

	private static Logger LOG = LoggerFactory.getLogger(UseCaseDescriptorCaller.class);

	public static final String TIMELINE_CONFIG_TJT_DOCUMENT = "tjt_document";

	/**
	 * Use case descriptors client.
	 *
	 * @return the use case descriptors I
	 */
	public UseCaseDescriptorsI useCaseDescriptorsClient() {
		LOG.info("useCaseDescriptorsClient called");
		return useCaseDescriptors().build();
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<UseCaseDescriptor> getList() throws Exception {
		LOG.info("getList called");
		UseCaseDescriptorsI client = useCaseDescriptorsClient();
		List<UseCaseDescriptor> listUCD = new ArrayList<UseCaseDescriptor>();
		Iterator<UseCaseDescriptor> useCaseDescrs = client.query(new QueryRequest());
		for (Iterator<UseCaseDescriptor> iterator = useCaseDescrs; useCaseDescrs.hasNext();) {
			UseCaseDescriptor prg = (UseCaseDescriptor) iterator.next();
			listUCD.add(prg);

		}

		LOG.info("returning {} {}", listUCD.size(), UseCaseDescriptor.class.getName());
		return listUCD;
	}

	/**
	 * Gets the list for handler ids.
	 *
	 * @param listHandlersIds the list handlers ids
	 * @return the list for handler ids
	 * @throws Exception the exception
	 */
	public List<UseCaseDescriptor> getListForHandlerIds(List<String> listHandlersIds) throws Exception {
		LOG.info("getListForHandlerIds called");
		return getListForJSONPath("_handlers._id", listHandlersIds);
	}

	/**
	 * Gets the list for JSON path.
	 *
	 * @param jsonPath   the json path
	 * @param listValues the list values
	 * @return the list for JSON path
	 * @throws Exception the exception
	 */
	public List<UseCaseDescriptor> getListForJSONPath(String jsonPath, List<String> listValues) throws Exception {
		LOG.info("getListForJSONPath called for jsonPath: {}, with listValues: {}", jsonPath, listValues);
		UseCaseDescriptorsI client = useCaseDescriptorsClient();
		List<UseCaseDescriptor> listUCD = new ArrayList<UseCaseDescriptor>();

//		BasicDBObjectBuilder builder = BasicDBObjectBuilder.start();
//		for (String key : listHandlersIds) {
//			// using regex and case-insensitive
//			BasicDBObject bs = new BasicDBObject();
//			bs.append(jsonPath, key);
//			builder.append(key, bs);
//
//		}
		// Building list of Document in OR clause
		BasicDBList list = new BasicDBList();
		for (String value : listValues) {

			Document doc = new Document(jsonPath, value);
			list.add(doc);
		}

		QueryRequest queryRequest = new QueryRequest();
		Document queryDoc = new Document();
		queryDoc.put("$and", list);
		LOG.debug("Performing query: {}", queryDoc.toJson());
		queryRequest.setFilter(queryDoc);

//		QueryRequest queryRequest = new QueryRequest();
//		Document queryDoc = new Document();
//		BasicDBObject bs = new BasicDBObject();
//		bs.append("$eq", hasValue);
//		queryDoc.put(jsonPath, bs);
//		LOG.debug("Performing query: {}", queryDoc.toJson());
//		queryRequest.setFilter(queryDoc);

		Iterator<UseCaseDescriptor> useCaseDescrsIt = client.query(queryRequest);

		if (useCaseDescrsIt != null) {
			while (useCaseDescrsIt.hasNext()) {
				UseCaseDescriptor prg = (UseCaseDescriptor) useCaseDescrsIt.next();
				listUCD.add(prg);

			}
		}
		LOG.info("getListForJSONPath returning {} {}", listUCD.size(), UseCaseDescriptor.class.getName());
		return listUCD;
	}

	/**
	 * Gets the UCD for id.
	 *
	 * @param profileID the profile ID
	 * @return the UCD for id
	 * @throws Exception the exception
	 */
	public UseCaseDescriptor getUCDForId(String profileID) throws Exception {
		LOG.info("getUCDForId called for profileID: {}", profileID);
		UseCaseDescriptorsI client = useCaseDescriptorsClient();

		QueryRequest queryRequest = new QueryRequest();
		Document queryDoc = new Document();
		BasicDBObject bs = new BasicDBObject();
		bs.append("$eq", profileID);
		queryDoc.put("_id", bs);
		LOG.debug("Performing query: {}", queryDoc.toJson());
		queryRequest.setFilter(queryDoc);

		Iterator<UseCaseDescriptor> useCaseDescrs = client.query(queryRequest);

		UseCaseDescriptor ucd = null;

		if (useCaseDescrs.hasNext()) {
			ucd = useCaseDescrs.next();
		}

		LOG.info("for profileID: {}, returning: {}", profileID, ucd);
		return ucd;
	}

	/**
	 * Gets the handlers by type.
	 *
	 * @param profileID      the profile ID
	 * @param theHandlerType the the handler type
	 * @return the handlers by type
	 * @throws Exception the exception
	 */
	public List<HandlerDeclaration> getHandlersByType(String profileID, String theHandlerType) throws Exception {
		LOG.info("getHandlersByType called for profileID: {}", profileID);
		UseCaseDescriptorsI client = useCaseDescriptorsClient();
		UseCaseDescriptor ucd = client.getById(profileID);
		return ucd.getHandlersByType(theHandlerType);
	}

	/**
	 * Gets the relationship definitions.
	 *
	 * @param profileID the profile ID
	 * @return the relationship definitions
	 * @throws Exception the exception
	 */
	public List<RelationshipDefinition> getRelationshipDefinitions(String profileID) throws Exception {
		LOG.info("getRelationshipNames called for profileID: {}", profileID);
		UseCaseDescriptorsI client = useCaseDescriptorsClient();
		UseCaseDescriptor ucd = client.getById(profileID);

		return ucd.getRelationshipDefinitions();
	}

	/**
	 * Read temporal dimension template.
	 *
	 * @param profileID the profile ID
	 * @return the document
	 * @throws Exception the exception
	 */
	public Document readTemporalDimensionTemplate(String profileID) throws Exception {

		UseCaseDescriptorsI client = useCaseDescriptorsClient();
		UseCaseDescriptor ucd = client.getById(profileID);

		List<HandlerDeclaration> timelineHandlers = ucd
				.getHandlersByType(GEOPORTAL_DATA_HANDLER.geoportal_timeline_json_template.getType());

		if (timelineHandlers != null && timelineHandlers.size() > 0) {
			HandlerDeclaration handler = timelineHandlers.get(0); // only one expected
			Document config = handler.getConfiguration();
			try {
				LinkedHashMap<String, Object> tjtDoc = (LinkedHashMap<String, Object>) config
						.get(TIMELINE_CONFIG_TJT_DOCUMENT);
				return new Document(tjtDoc);
			} catch (Exception e) {
				LOG.warn("Error on reading the  handler type "
						+ GEOPORTAL_DATA_HANDLER.geoportal_timeline_json_template.getType() + " in the profileID: "
						+ profileID, e);
				return null;
			}
		}

		return null;
	}

}
