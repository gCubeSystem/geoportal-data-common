package org.gcube.application.geoportalcommon.geoportal;

/**
 * The Class GeoportalClientCaller
 * 
 * Wrapping the geoportal Clients
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Mar 11, 2022
 */
public class GeoportalClientCaller {


	/**
	 * Use case descriptor client.
	 *
	 * @return the use case descriptor caller
	 */
	public static UseCaseDescriptorCaller useCaseDescriptors() {
		return new UseCaseDescriptorCaller();
	}

	/**
	 * Projects client
	 *
	 * @return the projects caller
	 */
	public static ProjectsCaller projects() {
		return new ProjectsCaller();
	}

}
