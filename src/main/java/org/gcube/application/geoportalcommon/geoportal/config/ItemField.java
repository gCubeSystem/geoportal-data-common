package org.gcube.application.geoportalcommon.geoportal.config;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@XmlRootElement(name = "itemFields")
@Slf4j
public class ItemField {

	@JsonProperty
	private String label;
	@JsonProperty
	private List<String> paths;
	@JsonProperty
	private String operator;
	@JsonProperty
	private boolean searchable;
	@JsonProperty
	private boolean sortable;
	@JsonProperty
	private boolean asResult;

	public ItemField() {
	}

}
