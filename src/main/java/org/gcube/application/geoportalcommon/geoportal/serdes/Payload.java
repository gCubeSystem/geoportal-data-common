package org.gcube.application.geoportalcommon.geoportal.serdes;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.extern.slf4j.Slf4j;

@XmlRootElement(name = "_payloads")
@Slf4j
public class Payload implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3975356671779455705L;

	@JsonProperty("_mimetype")
	private String mimetype;

	@JsonProperty("_storageID")
	private String storageID;

	@JsonProperty("_link")
	private String link;

	@JsonProperty("_name")
	private String name;

	public Payload() {

	}

	public String getMimetype() {
		return mimetype;
	}

	public String getStorageID() {
		return storageID;
	}

	public String getLink() {
		return link;
	}

	public String getName() {
		return name;
	}

	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}

	public void setStorageID(String storageID) {
		this.storageID = storageID;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Payload [mimetype=");
		builder.append(mimetype);
		builder.append(", storageID=");
		builder.append(storageID);
		builder.append(", link=");
		builder.append(link);
		builder.append(", name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}

}
