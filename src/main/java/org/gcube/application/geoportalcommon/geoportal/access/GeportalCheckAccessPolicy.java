package org.gcube.application.geoportalcommon.geoportal.access;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GeportalCheckAccessPolicy.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Oct 7, 2022
 */
public class GeportalCheckAccessPolicy {

	private static final Logger LOG = LoggerFactory.getLogger(GeportalCheckAccessPolicy.class);

	/**
	 * The Enum ACCESS_POLICY.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Sep 8, 2021
	 */
	public static enum ACCESS_POLICY {
		OPEN, RESTICTED
	}

	/**
	 * Checks if is open access.
	 *
	 * @param policy the policy
	 * @return true, if is open access
	 */
	private static boolean isOpenAccess(String policy) {
		if (policy == null || policy.equalsIgnoreCase(ACCESS_POLICY.OPEN.name())) {
			return true;
		}

		return false;

	}

	/**
	 * Checks if is restricted access.
	 *
	 * @param policy the policy
	 * @return true, if is restricted access
	 */
	private static boolean isRestrictedAccess(String policy) {
		if (policy == null || policy.equalsIgnoreCase(ACCESS_POLICY.RESTICTED.name())) {
			return true;
		}

		return false;

	}

	/**
	 * Checks if is accessible according to access policies.
	 *
	 * @param policy  the policy
	 * @param myLogin the my login
	 * @return true, if is accessible
	 */
	public static boolean isAccessible(String policy, String myLogin) {

		boolean bool = isOpenAccess(policy);

		if (bool) {
			// is open access
			return true;
		}

		// From here managing is NOT OPEN access

		if (myLogin == null || myLogin.isEmpty()) {
			// here is not open and the user is not authenticated
			return false;
		}

		// Here the login is not null, so checking if the access to item is RESTICTED
		bool = isRestrictedAccess(policy);

		if (bool) {
			// is restricted access
			return true;
		}

		// Here the user is authenticated, but the policy is not managed, so returning
		// true
		return true;
	}


	/**
	 * Access policy from session login.
	 *
	 * @param mySessionLogin the my session login
	 * @return the access policy
	 */
	public static ACCESS_POLICY accessPolicyFromSessionLogin(String mySessionLogin) {

		if (mySessionLogin == null || mySessionLogin.isEmpty()) {
			// here is not open and the user is not authenticated
			return ACCESS_POLICY.OPEN;
		}

		return ACCESS_POLICY.RESTICTED;
	}

}
