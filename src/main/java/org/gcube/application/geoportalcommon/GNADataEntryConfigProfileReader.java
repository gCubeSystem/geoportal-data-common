package org.gcube.application.geoportalcommon;

import static org.gcube.resources.discovery.icclient.ICFactory.client;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.gcube.application.geoportalcommon.config.CSVFile;
import org.gcube.application.geoportalcommon.config.CSVReader;
import org.gcube.application.geoportalcommon.config.CSVRow;
import org.gcube.application.geoportalcommon.config.FileUtil;
import org.gcube.application.geoportalcommon.shared.GNADataEntryConfigProfile;
import org.gcube.application.geoportalcommon.shared.config.GcubeUserRole;
import org.gcube.application.geoportalcommon.shared.config.OPERATION_ON_ITEM;
import org.gcube.application.geoportalcommon.shared.config.RoleRights;
import org.gcube.application.geoportalcommon.shared.config.RoleRights.OPERATION_TYPE;
import org.gcube.application.geoportalcommon.shared.exception.ApplicationProfileNotFoundException;
import org.gcube.application.geoportalcommon.shared.exception.GNAConfigException;
import org.gcube.application.geoportalcommon.shared.guipresentation.BodyWelcome;
import org.gcube.application.geoportalcommon.shared.guipresentation.DataEntryGUIPresentationConfig;
import org.gcube.application.geoportalcommon.shared.guipresentation.Header;
import org.gcube.common.resources.gcore.utils.XPathHelper;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.Query;
import org.gcube.resources.discovery.client.queries.impl.QueryBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 * The Class GNADataEntryConfigProfileReader.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 21, 2021
 */
public class GNADataEntryConfigProfileReader {

	private static final String RESOURCE_PROFILE_BODY = "/Resource/Profile/Body";
	public static final String SECONDARY_TYPE = "ApplicationProfile";
	public static final String GENERIC_RESOURCE_NAME = "Geoportal-DataEntry-Configs";

	private static final String PATH_TO_PERMISSIONS_PATH = RESOURCE_PROFILE_BODY + "/permssions_for_role";
//	private static final String PATH_TO_ITEM_FIELDS_CONFIG = RESOURCE_PROFILE_BODY + "/item_fields_config";
	
	private static final String PATH_TO_HEADER_GUI_PRESENTATION = RESOURCE_PROFILE_BODY + "/gui_presentation/data_entry_facility/header";
	private static final String PATH_TO_BODY_WELCOME_GUI_PRESENTATION = RESOURCE_PROFILE_BODY + "/gui_presentation/data_entry_facility/body_welcome";

	private String scope;

	private static final Logger LOG = LoggerFactory.getLogger(GNADataEntryConfigProfileReader.class);

	/**
	 * Instantiates a new application profile reader.
	 */
	public GNADataEntryConfigProfileReader() {
	}

	/**
	 * Read profile from infrastrucure.
	 *
	 * @return the map
	 * @throws Exception the exception
	 */
	public GNADataEntryConfigProfile readProfileFromInfrastrucure() throws Exception {
		LOG.info("called readProfileFromInfrastrucure");
		String queryString = getGcubeGenericQueryString(SECONDARY_TYPE, GENERIC_RESOURCE_NAME);
		this.scope = ScopeProvider.instance.get();
		LOG.info("Scope " + scope + ", trying to perform query: " + queryString);

		if (scope == null)
			throw new Exception("Scope is null, set scope into ScopeProvider");

		GNADataEntryConfigProfile gnDEC = new GNADataEntryConfigProfile();
		String permissions_for_role = "";
		String item_fields = "";
		try {

			LOG.info("Trying to fetch GR named: " + GENERIC_RESOURCE_NAME + ", in the scope: " + scope
					+ ", SecondaryType: " + SECONDARY_TYPE);
			Query q = new QueryBox(queryString);
			DiscoveryClient<String> client = client();
			List<String> appProfile = client.submit(q);

			if (appProfile == null || appProfile.size() == 0)
				throw new ApplicationProfileNotFoundException("GR with SecondaryType: " + SECONDARY_TYPE
						+ ", and name: " + GENERIC_RESOURCE_NAME + " is not registered in the scope: " + scope);
			else {
				String elem = appProfile.get(0);
				DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document doc = docBuilder.parse(new InputSource(new StringReader(elem)));

				XPathHelper helper = new XPathHelper(doc.getDocumentElement());

				List<String> currValue = null;

				String xPathExp = PATH_TO_PERMISSIONS_PATH + "/text()";
				currValue = helper.evaluate(xPathExp);

				if (currValue != null && currValue.size() > 0) {
					permissions_for_role = currValue.get(0);
				} else
					throw new Exception("I'm not able to read the path: " + xPathExp);

				// replacing \n with new_line string
				LOG.debug("read " + PATH_TO_PERMISSIONS_PATH + ": " + permissions_for_role);
				String value_with_new_lines = permissions_for_role.replaceAll("\\\\n", System.lineSeparator());
				LOG.debug(PATH_TO_PERMISSIONS_PATH + " with new lines: " + value_with_new_lines);

				List<RoleRights> listRoleRights = readRoleRightsConfig(value_with_new_lines);
				gnDEC.setPermissionsForRole(listRoleRights);

				//GUI PRESENTATION CONFIGS
				DataEntryGUIPresentationConfig deGUIP = new DataEntryGUIPresentationConfig();
				
				xPathExp = PATH_TO_HEADER_GUI_PRESENTATION;
				currValue = helper.evaluate(xPathExp);
				LOG.debug("read " + PATH_TO_HEADER_GUI_PRESENTATION + ": " + currValue);
				if (currValue != null && currValue.size() > 0) {
					String headerGUI = currValue.get(0);
					Header headerClass = SerializerUtil.unmarshalXML(headerGUI, Header.class);
					LOG.debug("serialized " + Header.class + ": " + headerClass);
					deGUIP.setHeader(headerClass);
				} else
					throw new Exception("I'm not able to read the path: " + xPathExp);
				
				xPathExp = PATH_TO_BODY_WELCOME_GUI_PRESENTATION;
				currValue = helper.evaluate(xPathExp);
				LOG.debug("read " + PATH_TO_BODY_WELCOME_GUI_PRESENTATION + ": " + currValue);
				if (currValue != null && currValue.size() > 0) {
					String bodyWelcome = currValue.get(0);
					BodyWelcome bodyWelcomeClass = SerializerUtil.unmarshalXML(bodyWelcome, BodyWelcome.class);
					LOG.debug("serialized " + BodyWelcome.class + ": " + bodyWelcomeClass);
					deGUIP.setBodyWelcome(bodyWelcomeClass);
				} else
					throw new Exception("I'm not able to read the path: " + xPathExp);
				
				gnDEC.setDataEntryGUIPresentation(deGUIP);
				LOG.info("returning: " + gnDEC);
				return gnDEC;
			}

		} catch (Exception e) {
			LOG.error("Error while trying to read the  " + SECONDARY_TYPE + " with SecondaryType "
					+ GENERIC_RESOURCE_NAME + " from scope " + scope, e);
			return null;
		} finally {

		}

	}
	

	/**
	 * To gcube user role.
	 *
	 * @param name the name
	 * @return the gcube user role
	 */
	protected static GcubeUserRole toGcubeUserRole(String name) {

		for (GcubeUserRole gCubeUserRole : GcubeUserRole.values()) {
			if (gCubeUserRole.getName().equalsIgnoreCase(name))
				return gCubeUserRole;
		}

		return null;
	}

	/**
	 * Read user rights config.
	 *
	 * @param permissions_for_role the permissions for role
	 * @return the list
	 * @throws GNAConfigException the GNA user rights config not found
	 *                                      exception
	 */
	public List<RoleRights> readRoleRightsConfig(String permissions_for_role) throws GNAConfigException {
		LOG.debug("readRoleRightsConfig called");

		File configurationFile = null;
		List<RoleRights> listUserRights = new ArrayList<RoleRights>();
		try {
			configurationFile = FileUtil.inputStreamToTempFile(permissions_for_role, "GNA_RoleRights_Configurations"+new Random().nextInt());

			CSVReader reader = new CSVReader(configurationFile);
			CSVFile csvFile = reader.getCsvFile();

			List<String> headerKeys = csvFile.getHeaderRow().getListValues();
			List<CSVRow> rows = csvFile.getValueRows();

			// MAPPING OPERATION TYPE AS READ, WRITE, etc.
			Map<String, OPERATION_TYPE> mapOperationTypes = new HashMap<String, RoleRights.OPERATION_TYPE>();
			CSVRow operationTypeRow = rows.get(0);
			List<String> rowValues = operationTypeRow.getListValues();
			for (int j = 1; j < rowValues.size(); j++) {
				String operationType = rowValues.get(j);
				RoleRights.OPERATION_TYPE ot = RoleRights.OPERATION_TYPE.UNKNOWN;
				if (operationType.equalsIgnoreCase("R")) {
					ot = RoleRights.OPERATION_TYPE.READ;
				} else if (operationType.equalsIgnoreCase("RW")) {
					ot = RoleRights.OPERATION_TYPE.READ_WRITE;
				} else if (operationType.equalsIgnoreCase("W")) {
					ot = RoleRights.OPERATION_TYPE.WRITE;
				}

				mapOperationTypes.put(headerKeys.get(j), ot);
			}

			LOG.debug("Map of operation types: " + mapOperationTypes);

			// Starting from index 1 (means the second row in the CSV)
			for (int i = 1; i < rows.size(); i++) {
				LOG.trace(i + " row");
				RoleRights useRights = new RoleRights();
				CSVRow row = rows.get(i);

				// to map properties
				rowValues = row.getListValues();

				LOG.debug("rowValues: " + rowValues);
				Map<String, String> mapUserRolePermissions = new HashMap<String, String>();

				GcubeUserRole gCubeUserRole = toGcubeUserRole(rowValues.get(0));

				if (gCubeUserRole == null) {
					LOG.warn("The Role " + rowValues.get(0) + " not found into roleName of: " + GcubeUserRole.values());
					continue;
				}

				useRights.setUserRole(gCubeUserRole);

				for (int j = 1; j < rowValues.size(); j++) {
					mapUserRolePermissions.put(headerKeys.get(j), rowValues.get(j));
				}
				LOG.debug("Role: " + useRights.getUserRole());
				LOG.debug("Permissions read: " + mapUserRolePermissions);

				Map<OPERATION_ON_ITEM, OPERATION_TYPE> listPermessions = new HashMap<OPERATION_ON_ITEM, OPERATION_TYPE>();

				for (OPERATION_ON_ITEM value : OPERATION_ON_ITEM.values()) {
					String yesno = mapUserRolePermissions.get(value.name());

					if (GNAConfigsConverter.checkYesNoValue(yesno)) {
						listPermessions.put(value, mapOperationTypes.get(value.name()));
					}

				}

				useRights.setListPermessions(listPermessions);

//				String writeOwn = mapUserRolePermissions.get(WRITE_OWN_CONFIG);
//				if (writeOwn != null && writeOwn.equalsIgnoreCase("yes")) {
//					useRights.setWriteOwn(true);
//				}
//
//				String writeAny = mapUserRolePermissions.get(WRITE_ANY_CONFIG);
//				if (writeAny != null && writeAny.equalsIgnoreCase("yes")) {
//					useRights.setWriteAny(true);
//				}

				listUserRights.add(useRights);

			}
			LOG.info("Returning user rights config: " + listUserRights);
			return listUserRights;

		} catch (Exception e) {
			LOG.error("An error occurred on reading the GNA DataEntry config from: " + permissions_for_role, e);
			throw new GNAConfigException("Error on reading the GNA DataEntry from: " + permissions_for_role);
		} finally {

			if (configurationFile != null) {
				try {
					configurationFile.delete();
				} catch (Exception e) {
					// silent
				}
			}
		}

	}

	/**
	 * Gets the gcube generic query string.
	 *
	 * @param secondaryType       the secondary type
	 * @param genericResourceName the generic resource name
	 * @return the gcube generic query string
	 */
	public static String getGcubeGenericQueryString(String secondaryType, String genericResourceName) {

		return "for $profile in collection('/db/Profiles/GenericResource')//Resource "
				+ "where $profile/Profile/SecondaryType/string() eq '" + secondaryType
				+ "' and  $profile/Profile/Name/string() " + " eq '" + genericResourceName + "'" + "return $profile";
	}

	/**
	 * Gets the secondary type.
	 *
	 * @return the secondary type
	 */
	public String getSecondaryType() {
		return SECONDARY_TYPE;
	}

	/**
	 * Gets the scope.
	 *
	 * @return the scope
	 */
	public String getScope() {
		return scope;
	}

}
