package org.gcube.application;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.HandlerDeclaration;
import org.gcube.application.geoportalcommon.ConvertToDataValueObjectModel;
import org.gcube.application.geoportalcommon.geoportal.config.GcubeProfile;
import org.gcube.application.geoportalcommon.shared.geoportal.ConfigurationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.HandlerDeclarationDV;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoadDocumentConfiguration {

	private static String TOKEN = "";
	private static String CONTEXT = "/gcube/devsec/devVRE";
	private static String USERNAME = "francesco.mangiacrapa";
	private static Logger LOG = LoggerFactory.getLogger(LoadDocumentConfiguration.class);

	// @Before
	public void init() {
		ScopeProvider.instance.set(CONTEXT);
	}

	@Test
	public void loadDocumentConfiguration() throws Exception {

//		MockDocumentConfigurationReader mock = new MockDocumentConfigurationReader();
//		System.out.println(mock.getListDocumentConfig());

		toHandlerDeclarationDV(null);
	}

	private static String readFile(String filePath) {
		String content = "";

		try {
			content = new String(Files.readAllBytes(Paths.get(filePath)));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return content;
	}

	/**
	 * To handler declaration DV.
	 *
	 * @param <T>                the generic type
	 * @param handlerDeclaration the handler declaration
	 * @return the handler declaration DV
	 * @throws Exception
	 */
	public static HandlerDeclarationDV toHandlerDeclarationDV(HandlerDeclaration handlerDeclaration) throws Exception {

//		if (handlerDeclaration == null) {
//			LOG.warn(HandlerDeclaration.class.getSimpleName() + " is null");
//			return null;
//		}

//		Document configuration = handlerDeclaration.getConfiguration();
//
//		if (configuration == null) {
//			LOG.warn("Configuration is null");
//			return null;
//		}

		String jsonConfig = readFile(
				"/home/francescomangiacrapa/git/geoportal-data-common/src/test/resources/geoportal-config5.json");

		LOG.debug("File as JSON: " + jsonConfig);

		Document document = org.gcube.application.geoportal.client.utils.Serialization.read(jsonConfig, Document.class);

		LinkedHashMap<String, String> configuration = (LinkedHashMap<String, String>) document.get("_configuration");

		// configuration.values();

		try {
			List<String> gcubeProfiles = new ArrayList<String>(configuration.values());
//			try {
//				gcubeProfiles = configuration.get("gcubeProfiles", ArrayList.class);
//			} catch (Exception e) {
//				LOG.info("gcubeProfiles not found in the handler");
//				return null;
//			}

			// HANDLER OF "gcubeProfiles" configuration
			if (gcubeProfiles != null) {

				List<GcubeProfileDV> listGcubeProfiles = new ArrayList<GcubeProfileDV>(gcubeProfiles.size());

				int i = 0;
				for (String gCubeProfile : gcubeProfiles) {
					LOG.debug(i++ + ") the gCubeProfile is: " + gCubeProfile);
					GcubeProfile profile = org.gcube.application.geoportal.client.utils.Serialization.read(gCubeProfile,
							GcubeProfile.class);
					listGcubeProfiles.add(ConvertToDataValueObjectModel.toGcubeProfileDV(profile));
				}

				HandlerDeclarationDV hdDV = new HandlerDeclarationDV();
				hdDV.setId(handlerDeclaration.getId());
				hdDV.setType(handlerDeclaration.getType());

				ConfigurationDV<List<GcubeProfileDV>> dDV = new ConfigurationDV<List<GcubeProfileDV>>(
						listGcubeProfiles);
				dDV.setConfiguration(listGcubeProfiles);
				hdDV.setConfiguration(dDV);

				LOG.info("returning {}", hdDV);
				return hdDV;
			}
		} catch (Exception e) {
			LOG.error("Error on getting " + HandlerDeclaration.class.getSimpleName(), e);
			throw e;
		}

		return null;

	}

}
