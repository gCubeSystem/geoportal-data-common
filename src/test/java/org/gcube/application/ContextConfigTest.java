package org.gcube.application;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ContextConfigTest {
	
	private static final String GCUBE_CONFIG_PROPERTIES_FILENAME = "gcube_config.properties";
	// APP Working Directory + /src/test/resources must be the location of
	// gcube_config.properties
	private static String gcube_config_path = String.format("%s/%s",
			System.getProperty("user.dir") + "/src/test/resources", GCUBE_CONFIG_PROPERTIES_FILENAME);
	
	
	public static String CONTEXT;
	public static String TOKEN;

	/**
	 * Read context settings.
	 */
	public static void readContextSettings() {

		try (InputStream input = new FileInputStream(gcube_config_path)) {

			Properties prop = new Properties();

			// load a properties file
			prop.load(input);

			CONTEXT = prop.getProperty("CONTEXT");
			TOKEN = prop.getProperty("TOKEN");
			// get the property value and print it out
			System.out.println("CONTEXT: " + CONTEXT);
			System.out.println("TOKEN: " + TOKEN);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
