package org.gcube.application;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.useCaseDescriptors;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.HandlerDeclaration;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportalcommon.ConvertToDataValueObjectModel;
import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.UseCaseDescriptorCaller;
import org.gcube.application.geoportalcommon.shared.geoportal.ConfigurationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ActionDefinitionDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ItemFieldDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_CONFIGURATION_TYPE;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.HandlerDeclarationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.JsonOrgJsonProvider;

/**
 * The Class UCD_Tests.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 9, 2022
 */
public class UCD_Tests {
	
	private static final String GCUBE_CONFIG_PROPERTIES_FILENAME = "gcube_config.properties";
	// APP Working Directory + /src/test/resources must be the location of
	// gcube_config.properties
	private static String gcube_config_path = String.format("%s/%s",
			System.getProperty("user.dir") + "/src/test/resources", GCUBE_CONFIG_PROPERTIES_FILENAME);
	private static String CONTEXT;
	private static String TOKEN;

	private UseCaseDescriptorCaller client = null;

	//private static String CONTEXT = "/gcube/devsec/devVRE";
	//private static String TOKEN = ""; // devVRE

	private static String PROFILE_ID = "profiledConcessioni";

	
	/**
	 * Read context settings.
	 */
	public static void readContextSettings() {

		try (InputStream input = new FileInputStream(gcube_config_path)) {

			Properties prop = new Properties();

			// load a properties file
			prop.load(input);

			CONTEXT = prop.getProperty("CONTEXT");
			TOKEN = prop.getProperty("TOKEN");
			// get the property value and print it out
			System.out.println("CONTEXT: " + CONTEXT);
			System.out.println("TOKEN: " + TOKEN);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Gets the client.
	 *
	 * @return the client
	 */
	@Before
	public void getClient() {
		readContextSettings();
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		client = GeoportalClientCaller.useCaseDescriptors();
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	//@Test
	public void getList() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		List<UseCaseDescriptor> listOfUCD = client.getList();

		int i = 0;
		for (UseCaseDescriptor useCaseDescriptor : listOfUCD) {
			System.out.println(++i + ") " + useCaseDescriptor);
		}
	}

	/**
	 * Gets the item by ID.
	 *
	 * @return the item by ID
	 * @throws Exception the exception
	 */
	//@Test
	public void getItemByID() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		UseCaseDescriptor ucd = client.getUCDForId(PROFILE_ID);
		System.out.println(ucd);
	}

	/**
	 * Convert to DV object.
	 *
	 * @throws Exception the exception
	 */
	// @Test
	public void convertToDVObject() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		List<UseCaseDescriptor> listOfUCD = client.getList();

		int i = 0;
		for (UseCaseDescriptor useCaseDescriptor : listOfUCD) {
			System.out.println(++i + ") " + useCaseDescriptor);
			ConvertToDataValueObjectModel.toUseCaseDescriptorDV(useCaseDescriptor, null);
		}
	}

	/**
	 * Gets the UCD for data entry handler ids.
	 *
	 * @return the UCD for data entry handler ids
	 * @throws Exception the exception
	 */
	//@Test
	public void getUCDFor_DataEntry_HandlerIds() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		List<String> handlersIds = null;
		List<UseCaseDescriptor> listUseCaseDescriptor = null;
		try {
			UseCaseDescriptorCaller client = GeoportalClientCaller.useCaseDescriptors();

			if (handlersIds == null) {
				handlersIds = Arrays.asList(GEOPORTAL_DATA_HANDLER.geoportal_data_entry.getId());
				System.out.println("handlersIds is null, so using default: " + handlersIds);
			}
			listUseCaseDescriptor = client.getListForHandlerIds(handlersIds);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (listUseCaseDescriptor == null) {
			listUseCaseDescriptor = new ArrayList<UseCaseDescriptor>();
		}

		List<UseCaseDescriptorDV> listUCDDV = new ArrayList<UseCaseDescriptorDV>(listUseCaseDescriptor.size());
		for (UseCaseDescriptor ucd : listUseCaseDescriptor) {
			listUCDDV.add(ConvertToDataValueObjectModel.toUseCaseDescriptorDV(ucd, null));
		}

		for (UseCaseDescriptorDV useCaseDescriptorDV : listUCDDV) {
			System.out.println("Found: " + useCaseDescriptorDV);
		}

	}

	/**
	 * Gets the UCD for data list handler ids.
	 *
	 * @return the UCD for data list handler ids
	 * @throws Exception the exception
	 */
	//@Test
	public void getUCDFor_DataList_HandlerIds() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		List<String> handlersIds = null;
		List<UseCaseDescriptor> listUseCaseDescriptor;

		try {
			UseCaseDescriptorCaller client = GeoportalClientCaller.useCaseDescriptors();

			if (handlersIds == null) {
				handlersIds = Arrays.asList(GEOPORTAL_DATA_HANDLER.geoportal_data_list.getId());
				System.out.println("handlersIds is null, so using default: " + handlersIds);
			}
			listUseCaseDescriptor = client.getListForHandlerIds(handlersIds);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (listUseCaseDescriptor == null) {
			listUseCaseDescriptor = new ArrayList<UseCaseDescriptor>();
		}

		List<UseCaseDescriptorDV> listUCDDV = new ArrayList<UseCaseDescriptorDV>(listUseCaseDescriptor.size());
		for (UseCaseDescriptor ucd : listUseCaseDescriptor) {
			listUCDDV.add(ConvertToDataValueObjectModel.toUseCaseDescriptorDV(ucd, null));
		}

		System.out.println(listUCDDV);

	}
	
	//@Test
	public void getUCDGroupedOverlayLayers() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		List<String> handlersIds = null;
		List<UseCaseDescriptor> listUseCaseDescriptor;

		try {
			UseCaseDescriptorCaller client = GeoportalClientCaller.useCaseDescriptors();

			if (handlersIds == null) {
				handlersIds = Arrays.asList(GEOPORTAL_DATA_HANDLER.geoportal_grouped_overlay_layers.getId());
				System.out.println("handlersIds is null, so using default: " + handlersIds);
			}
			listUseCaseDescriptor = client.getListForHandlerIds(handlersIds);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (listUseCaseDescriptor == null) {
			listUseCaseDescriptor = new ArrayList<UseCaseDescriptor>();
		}

		List<UseCaseDescriptorDV> listUCDDV = new ArrayList<UseCaseDescriptorDV>(listUseCaseDescriptor.size());
		for (UseCaseDescriptor ucd : listUseCaseDescriptor) {
			listUCDDV.add(ConvertToDataValueObjectModel.toUseCaseDescriptorDV(ucd, null));
		}

		System.out.println(listUCDDV);

	}
	
	
	//@Test
	public void getUCDGroupedCrossFilteringLayers() throws Exception {
		System.out.println("Running getUCDGroupedCrossFilteringLayers");
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		List<String> handlersIds = null;
		List<UseCaseDescriptor> listUseCaseDescriptor;

		try {
			UseCaseDescriptorCaller client = GeoportalClientCaller.useCaseDescriptors();

			if (handlersIds == null) {
				handlersIds = Arrays.asList(GEOPORTAL_DATA_HANDLER.geoportal_grouped_cross_filtering.getId());
				System.out.println("handlersIds is null, so using default: " + handlersIds);
			}
			listUseCaseDescriptor = client.getListForHandlerIds(handlersIds);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (listUseCaseDescriptor == null) {
			listUseCaseDescriptor = new ArrayList<UseCaseDescriptor>();
		}

		List<UseCaseDescriptorDV> listUCDDV = new ArrayList<UseCaseDescriptorDV>(listUseCaseDescriptor.size());
		for (UseCaseDescriptor ucd : listUseCaseDescriptor) {
			listUCDDV.add(ConvertToDataValueObjectModel.toUseCaseDescriptorDV(ucd, null));
		}
		
		System.out.println(listUCDDV);
		System.out.println("Terminated getUCDGroupedCrossFilteringLayers");

	}

	/**
	 * Gets the UCD for handler types.
	 *
	 * @return the UCD for handler types
	 * @throws Exception the exception
	 */
	//@Test
	public void getUCDForHandlerTypes() throws Exception {
		System.out.println("Running getUCDForHandlerTypes");
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		
		String handlerType = GEOPORTAL_DATA_HANDLER.geoportal_grouped_cross_filtering.getType();
		
		UseCaseDescriptor ucd = null;
		List<HandlerDeclaration> handlers = null;
		try {
			UseCaseDescriptorCaller client = GeoportalClientCaller.useCaseDescriptors();
			ucd = client.getUCDForId(PROFILE_ID);
			handlers = client.getHandlersByType(PROFILE_ID, handlerType);

		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		int i = 0;
		for (HandlerDeclaration handlerDeclaration : handlers) {
			System.out.println(handlerDeclaration);
			System.out.println(++i+")##Config json: "+handlerDeclaration.getConfiguration().toJson());
			HandlerDeclarationDV handlerDV = ConvertToDataValueObjectModel.toHandlerDeclarationDV(handlerDeclaration, ucd, GEOPORTAL_CONFIGURATION_TYPE.grouped_cross_filtering_layers);
			System.out.println("\nhandlerDV is: "+handlerDV);
		}
		System.out.println("\n");
		System.out.println("Terminated getUCDForHandlerTypes");
	}
	

	
	
	/**
	 * Gets the UCD for handler types.
	 *
	 * @return the UCD for handler types
	 * @throws Exception the exception
	 */
	//@Test
	public void getTimelineJSONTemplateConfig() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		UseCaseDescriptor ucd = null;
		List<HandlerDeclaration> handlers = null;
		try {

			useCaseDescriptors().build().getById(PROFILE_ID).getHandlersByType(GEOPORTAL_DATA_HANDLER.geoportal_timeline_json_template.getType()).forEach(u -> {
				Document config = u.getConfiguration();
				System.out.println("config "+config);

				System.out.println("config.keySet() "+config.keySet());

				
				LinkedHashMap<String, Object> ttd = (LinkedHashMap<String, Object>) config.get("tjt_document");

				String jsonDocOfTJT = new Document(ttd).toJson();
				System.out.println("ttd "+jsonDocOfTJT);
				
				System.out.println(jsonDocOfTJT);
				
			});

		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

//		for (HandlerDeclaration handlerDeclaration : handlers) {
//			System.out.println(handlerDeclaration);
//		}

	}

	/**
	 * Gets the roles for STEP id.
	 *
	 * @return the roles for STEP id
	 * @throws Exception the exception
	 */
	//@Test
	public void getRolesForSTEPId() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		try {
			UseCaseDescriptor ucd = null;
			List<HandlerDeclaration> handlers = null;
			String stepID = "REJECT-DRAFT";

			UseCaseDescriptorCaller client = GeoportalClientCaller.useCaseDescriptors();
			ucd = client.getUCDForId(PROFILE_ID);
			handlers = client.getHandlersByType(PROFILE_ID, GEOPORTAL_DATA_HANDLER.gna_concessioni_lc.getType());
			
			com.jayway.jsonpath.Configuration configuration = com.jayway.jsonpath.Configuration.builder()
					.jsonProvider(new JsonOrgJsonProvider()).build();

			for (HandlerDeclaration handler : handlers) {

				String toJSON = handler.getConfiguration().toJson();
				System.out.println("Handler to JSON: " + toJSON);
				JSONObject jObject = new JSONObject(toJSON);
				JsonPath jsonPath = JsonPath.compile("$.step_access[?(@.STEP == '"+ stepID +"' )].roles");
				System.out.println("jsonPath: " + jsonPath.getPath());
				// $..book[?(@.price <= $['expensive'])]
				JSONArray data = jsonPath.read(jObject, configuration);
				System.out.println("matching data lenght: " + data.length());
				HashSet<String> listdata = new HashSet<String>(data.length());
				for (int i = 0; i < data.length(); i++) {
					listdata.add(data.get(i).toString());
				}
				List<String> roles = listdata.stream().collect(Collectors.toList());
				System.out.println("getRole returning: " + roles);

			}

		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

	}

	/**
	 * Gets the configurations for UC D data handlers.
	 *
	 * @return the configurations for UC D data handlers
	 * @throws Exception the exception
	 */
	//@Test
	public void getConfigurations_ForUCD_Data_Handlers() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		List<String> handlersIds = null;
		List<UseCaseDescriptor> listUseCaseDescriptor = null;
		try {
			UseCaseDescriptorCaller client = GeoportalClientCaller.useCaseDescriptors();

			if (handlersIds == null) {
				// handlersIds =
				// Arrays.asList(GEOPORTAL_DATA_HANDLER.geoportal_data_list.getId(),
				// GEOPORTAL_DATA_HANDLER.geoportal_data_entry.getId());
				handlersIds = Arrays.asList(GEOPORTAL_DATA_HANDLER.geoportal_workflow_action_list.getId());
				System.out.println("handlersIds is null, so using default: " + handlersIds);
			}
			listUseCaseDescriptor = client.getListForHandlerIds(handlersIds);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (listUseCaseDescriptor == null) {
			listUseCaseDescriptor = new ArrayList<UseCaseDescriptor>();
		}

		List<UseCaseDescriptorDV> listUCDDV = new ArrayList<UseCaseDescriptorDV>(listUseCaseDescriptor.size());
		for (UseCaseDescriptor ucd : listUseCaseDescriptor) {
			listUCDDV.add(ConvertToDataValueObjectModel.toUseCaseDescriptorDV(ucd, null));
		}

		for (UseCaseDescriptorDV useCaseDescriptorDV : listUCDDV) {
			System.out.println("\n");
			System.out.println("UCD name:" + useCaseDescriptorDV.getName());
			System.out.println("UCD profileID:" + useCaseDescriptorDV.getProfileID());
			List<HandlerDeclarationDV> handlers = useCaseDescriptorDV.getHandlers();
			for (HandlerDeclarationDV handler : handlers) {
				System.out.println("\n\thandler id:" + handler.getId());
				System.out.println("\thandler ItemType:" + handler.getItemType());
				System.out.println("\thandler Type:" + handler.getType());
				ConfigurationDV<?> config = handler.getConfiguration();
				System.out.println("\tConfig type:" + config.getConfigurationType());
				// System.out.println("\tConfig:" + config.getConfiguration());
				switch (config.getConfigurationType()) {
				case gcube_profiles:
					List<GcubeProfileDV> listProfiles = toListGcubeProfiles(config);
					System.out.println("\t\t" + listProfiles);
					break;
				case item_fields:
					List<ItemFieldDV> listItems = toListItemFields(config);
					System.out.println("\t\t" + listItems);
					break;
				case actions_definition:
					List<ActionDefinitionDV> listActions = toListActionsDefinition(config);
					System.out.println("\t\t" + listActions);
					break;

				default:
					break;
				}

			}
			System.out.println("\n\n");
		}
	}

	/**
	 * Registrer file sets.
	 *
	 * @throws Exception the exception
	 */
	// @Test
	public void registrerFileSets() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		// FileSets.build(TOKEN, PROFILE_ID, CONTEXT)

		// System.out.println(listUCDDV);

	}

	/**
	 * To list gcube profiles.
	 *
	 * @param config the config
	 * @return the list
	 */
	private List<GcubeProfileDV> toListGcubeProfiles(ConfigurationDV<?> config) {

		try {
			return (List<GcubeProfileDV>) config.getConfiguration();
		} catch (Exception e) {
			System.err.println("Error on casting " + ConfigurationDV.class.getName() + " to List of "
					+ GcubeProfileDV.class.getName());
		}

		return null;
	}

	/**
	 * To list actions definition.
	 *
	 * @param config the config
	 * @return the list
	 */
	private List<ActionDefinitionDV> toListActionsDefinition(ConfigurationDV<?> config) {

		try {
			return (List<ActionDefinitionDV>) config.getConfiguration();
		} catch (Exception e) {
			System.err.println("Error on casting " + ConfigurationDV.class.getName() + " to List of "
					+ ActionDefinitionDV.class.getName());
		}

		return null;
	}

	/**
	 * To list item fields.
	 *
	 * @param config the config
	 * @return the list
	 */
	private List<ItemFieldDV> toListItemFields(ConfigurationDV<?> config) {

		try {
			return (List<ItemFieldDV>) config.getConfiguration();
		} catch (Exception e) {
			System.err.println("Error on casting " + ConfigurationDV.class.getName() + " to List of "
					+ ItemFieldDV.class.getName());
		}

		return null;
	}

}
