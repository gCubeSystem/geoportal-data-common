package org.gcube.application;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.gcube.application.geoportalcommon.GeoportalCommon;
import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.ProjectsCaller;
import org.gcube.application.geoportalcommon.geoportal.UseCaseDescriptorCaller;
import org.gcube.application.geoportalcommon.shared.GNADataEntryConfigProfile;
import org.gcube.application.geoportalcommon.shared.GNADataViewerConfigProfile;
import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.config.RoleRights;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;

public class TestGNACommon {
	
	private static final String GCUBE_CONFIG_PROPERTIES_FILENAME = "gcube_config.properties";
	// APP Working Directory + /src/test/resources must be the location of
	// gcube_config.properties
	private static String gcube_config_path = String.format("%s/%s",
			System.getProperty("user.dir") + "/src/test/resources", GCUBE_CONFIG_PROPERTIES_FILENAME);
	private static String CONTEXT;
	private static String TOKEN;

	private static UseCaseDescriptorCaller clientUCD = null;
	private static ProjectsCaller clientPrj = null;

	private static String PROFILE_ID = "profiledConcessioni";
	private static String PROJECT_ID = "644a66e944aad51c80409a3b";

	private static String MY_LOGIN = "francesco.mangiacrapa";

	/**
	 * Read context settings.
	 */
	public static void readContextSettings() {

		try (InputStream input = new FileInputStream(gcube_config_path)) {

			Properties prop = new Properties();

			// load a properties file
			prop.load(input);

			CONTEXT = prop.getProperty("CONTEXT");
			TOKEN = prop.getProperty("TOKEN");
			// get the property value and print it out
			System.out.println("CONTEXT: " + CONTEXT);
			System.out.println("TOKEN: " + TOKEN);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	//@Before
	public void init() {
		readContextSettings();
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		clientPrj = GeoportalClientCaller.projects();
		clientUCD = GeoportalClientCaller.useCaseDescriptors();
	}

	// @Test
	public void getGeoNaDataViewProfile() throws Exception {
		System.out.println("getGeoNaDataViewProfile called");

		ScopeProvider.instance.set(CONTEXT);
		GeoportalCommon gc = new GeoportalCommon();
		GNADataViewerConfigProfile profile = gc.readGNADataViewerConfig(null);
		System.out.println("Returning profile: " + profile);
	}

	//@Test
	public void getLinks() throws Exception {
		System.out.println("getLinks called");

		ScopeProvider.instance.set(CONTEXT);
		GeoportalCommon gc = new GeoportalCommon();
		GeoportalItemReferences item = new GeoportalItemReferences(PROJECT_ID, PROFILE_ID);
		GeoportalItemReferences links = gc.getPublicLinksFor(CONTEXT, item, true);
		System.out.println(links);
	}

	// @Test
//	public void queryConcessioniTest() throws Exception {
//		try {
//
//			ScopeProvider.instance.set(CONTEXT);
//			MongoServiceCommon msc = new MongoServiceCommon();
//			SearchingFilter filter = new SearchingFilter();
//
//			Map<String, Object> searchInto = new HashMap<String, Object>();
//			searchInto.put("nome", "san");
//			searchInto.put("authors", "silvia");
//			// searchInto.put("report.status", "PASSED");
//
//			WhereClause where1 = new WhereClause(LOGICAL_OP.OR, searchInto);
//
//			Map<String, Object> searchInto2 = new HashMap<String, Object>();
//			searchInto2.put("report.status", "PASSED");
//			WhereClause where2 = new WhereClause(LOGICAL_OP.AND, searchInto2);
//
//			ArrayList<WhereClause> list = new ArrayList<WhereClause>();
//			list.add(where1);
//			// list.add(where2);
//			filter.setConditions(list);
//			ResultSetPaginatedData result = msc.queryOnMongo(30, 0, 30, filter, "concessione");
//
//			int i = 0;
//			for (ConcessioneDV concessione : result.getData()) {
//				System.out.println(++i + ") " + concessione);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}

	// @Test
	public void readGNDataEntryConfigsFromIS() throws Exception {

		Thread thread = new Thread() {
			public void run() {
				geoCommon();
			}
		};

		Thread thread2 = new Thread() {
			public void run() {
				geoCommon();
			}
		};

		thread.run();
		thread2.run();
	}

	private void geoCommon() {
		GeoportalCommon r = new GeoportalCommon();
		try {
			ScopeProvider.instance.set(CONTEXT);
			GNADataEntryConfigProfile configurations = r.readGNADataEntryConfig();

			System.out.println("Permissions are:");
			int i = 0;
			for (RoleRights role : configurations.getPermissionsForRole()) {
				System.out.println(++i + " " + role);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//@Test
	public void readGNDataViewerConfigsFromIS() throws Exception {

		GeoportalCommon r = new GeoportalCommon();
		try {
			ScopeProvider.instance.set(CONTEXT);
			GNADataViewerConfigProfile configurations = r.readGNADataViewerConfig(null);
			System.out.println(configurations.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
