package org.gcube.application;

import org.gcube.application.geoportalcommon.GNADataEntryConfigProfileReader;
import org.gcube.application.geoportalcommon.shared.GNADataEntryConfigProfile;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;

public class IS_Read_Configurations_Tests {
	
	private static String CONTEXT = "/gcube/devsec/devVRE";
	private static String TOKEN = ""; // devVRE

	//@Before
	public void getClient() {
		// assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
	}

	//@Test
	public void loadGNA_DataEntry_Configs() throws Exception{
		ScopeProvider.instance.set(CONTEXT);
		GNADataEntryConfigProfileReader dataEntryConfigs = new GNADataEntryConfigProfileReader();
		GNADataEntryConfigProfile configProfile = dataEntryConfigs.readProfileFromInfrastrucure();
		System.out.println(configProfile);
		
		System.out.println("GUI presentantion: "+configProfile.getDataEntryGUIPresentation());
		System.out.println("GUI permissions: "+configProfile.getPermissionsForRole());
		
	}
}
