package org.gcube.application.se;

import java.io.Serializable;
import java.util.List;

import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;

/**
 * The Class ServerParameters.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it May 16, 2017
 */
public class ServiceEndpointBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2459971193655529274L;

	protected org.gcube.common.resources.gcore.ServiceEndpoint.Runtime runtime;
	protected List<AccessPoint> listAP;
	
	public ServiceEndpointBean() {
		// TODO Auto-generated constructor stub
	}

	public ServiceEndpointBean(org.gcube.common.resources.gcore.ServiceEndpoint.Runtime runtime, List<AccessPoint> listAP) {
		super();
		this.runtime = runtime;
		this.listAP = listAP;
	}

	public org.gcube.common.resources.gcore.ServiceEndpoint.Runtime getRuntime() {
		return runtime;
	}

	public List<AccessPoint> getListAP() {
		return listAP;
	}

	public void setRuntime(org.gcube.common.resources.gcore.ServiceEndpoint.Runtime runtime2) {
		this.runtime = runtime2;
	}

	public void setListAP(List<AccessPoint> listAP) {
		this.listAP = listAP;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ServiceEndpoint [runtime=");
		builder.append(runtime);
		builder.append(", listAP=");
		builder.append(listAP);
		builder.append("]");
		return builder.toString();
	}

}
