package org.gcube.application;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.ProjectsCaller;
import org.gcube.application.se.RuntimeResourceReader;
import org.gcube.application.se.ServiceEndpointBean;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.scope.api.ScopeProvider;

public class GNA_New_DataModel_IndexTable_PROD_25926 {

	private static String PROFILE_ID = "profiledConcessioni";

	private static final String platformName = "postgis";
	private static final String category = "Database";

	// These are defined via ContextConfigTest.readContextSettings();
	private static String CONTEXT = "";
	private static String TOKEN = "";

	// #### PROD
	// DB
	private static final String resourceName = "GNA-postgis"; // GNA

	// Esquiline
//	private static final String CONTEXT = "/d4science.research-infrastructures.eu/gCubeApps/Esquiline";
//	private static final String TOKEN = ""; //Esquiline
//	private static String PROFILE_ID = "esquilino";

	private static final Map<String, String> ADD_TABLE_COLUMNS = new LinkedHashMap<>();
//	static {
//		ADD_TABLE_COLUMNS.put("ufficio_mic_competente", "TEXT");
//		ADD_TABLE_COLUMNS.put("funzionario_responsabile", "TEXT");
//		ADD_TABLE_COLUMNS.put("modalita_individuazione", "TEXT");
//		ADD_TABLE_COLUMNS.put("contesto_indagine", "TEXT");
//		ADD_TABLE_COLUMNS.put("denominazione", "TEXT");
//		ADD_TABLE_COLUMNS.put("stato_attuale", "TEXT");
//		ADD_TABLE_COLUMNS.put("accessibilita", "TEXT");
//		ADD_TABLE_COLUMNS.put("cronologia_macrofase", "TEXT");
//		ADD_TABLE_COLUMNS.put("specifiche_cronologia", "TEXT");
//		ADD_TABLE_COLUMNS.put("quota_max", "TEXT");
//		ADD_TABLE_COLUMNS.put("quota_min", "TEXT");
//	}
//
	private static final Map<String, String> DROP_TABLE_COLUMNS = new HashMap<>();
//	static {
//		DROP_TABLE_COLUMNS.put("autore", "");
//		DROP_TABLE_COLUMNS.put("titolare", "");
//		DROP_TABLE_COLUMNS.put("titolare_licenza", "");
//	}

	private static final Map<String, String> RENAME_TABLE_COLUMNS = new HashMap<>();
	// Rename from column to column
	static {
		RENAME_TABLE_COLUMNS.put("ufficio_mic_competente", "ufficio_competente");
	}

	private static ProjectsCaller client = null;

	public static final boolean READ_ONLY_MODE = true;

	public static List<String> listTable = new ArrayList<String>();

	/**
	 * Gets the client.
	 *
	 * @return the client
	 */
	// @Before
	public static void initGeoportalClient() {
		// assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		ContextConfigTest.readContextSettings();
		CONTEXT = ContextConfigTest.CONTEXT;
		TOKEN = ContextConfigTest.TOKEN;

		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		client = GeoportalClientCaller.projects();
	}

	public static void main(String[] args) {

		initGeoportalClient();

		System.out.println("READ_ONLY_MODE ENABLED: " + READ_ONLY_MODE);
		System.out.println("CONTEXT IS: " + CONTEXT);
		System.out.println("PROFILE_ID: " + PROFILE_ID);

		// GNA
//		PROFILE_ID = "profiledConcessioni";
//		listTable.add("profiledconcessioni_internal__gna_centroids");
//		listTable.add("profiledconcessioni_gna_centroids");
//		PROFILE_ID = "concessioni-estere";
//		listTable.add("concessioni_estere_internal__gna_centroids");
//		listTable.add("concessioni_estere_gna_centroids");

		// Esquiline
//		PROFILE_ID = "esquilino";
//		String tableName = "esquilino_esquiline_centroids";

		long startTime = System.currentTimeMillis();

		try {
			RuntimeResourceReader rrr = new RuntimeResourceReader(CONTEXT, resourceName, platformName, category, null);
			ServiceEndpointBean se = rrr.getListSE().get(0);
			AccessPoint ap = se.getListAP().get(0);

			System.out.println(ap.address());
			System.out.println(ap.username());
			String pwdDB = RuntimeResourceReader.dectryptPassword(CONTEXT, ap);
			System.out.println(pwdDB);

			Connection dbConnection = getDatabaseConnection(ap.address(), ap.username(), pwdDB);

			for (String tableName : listTable) {

				System.out.println("\n\n# SOURCE TABLE");
				Boolean tableExists = checkTableExists(dbConnection, tableName);

				if (!tableExists) {
					throw new Exception("Table '" + tableName + "' does not exits in the DB!!! Exit");
				}

				printTableColumn(dbConnection, tableName);

				System.out.println("\n\n### ADDING COLUMNS...\n");

				// ADD NEW COLUMNS
				for (String columnName : ADD_TABLE_COLUMNS.keySet()) {
					try {

						boolean columnExists = checkColumnExists(dbConnection, tableName, columnName);

						if (!columnExists) {
							addColumnTable(dbConnection, tableName, columnName, ADD_TABLE_COLUMNS.get(columnName));
							int sleeping = 500;
							System.out.println("... sleeping " + sleeping + " ...");
							Thread.sleep(sleeping);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return;
					}
				}

				System.out.println("\n\n### END ADD COLUMNS");

				System.out.println("\n\n### DROPPING COLUMNS...\n");

				// REMOVE OLD COLUMNS
				for (String columnName : DROP_TABLE_COLUMNS.keySet()) {
					try {

						boolean columnExists = checkColumnExists(dbConnection, tableName, columnName);

						if (columnExists) {
							dropColumnTable(dbConnection, tableName, columnName, DROP_TABLE_COLUMNS.get(columnName));
							int sleeping = 1000;
							System.out.println("... sleeping " + sleeping + " ...");
							Thread.sleep(sleeping);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return;
					}
				}

				System.out.println("\n\n### END DROP COLUMNS");

				System.out.println("\n\n### RENAMING COLUMNS...\n");
				// RENAME_TABLE_COLUMNS
				for (String columnName : RENAME_TABLE_COLUMNS.keySet()) {
					try {

						boolean columnExists = checkColumnExists(dbConnection, tableName, columnName);

						if (columnExists) {
							renameColumnTable(dbConnection, tableName, columnName,
									RENAME_TABLE_COLUMNS.get(columnName));
							int sleeping = 1000;
							System.out.println("... sleeping " + sleeping + " ...");
							Thread.sleep(sleeping);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return;
					}
				}

				System.out.println("\n\n### END RENAME COLUMNS");

				System.out.println("\n\n# UPDATED TABLE");
				printTableColumn(dbConnection, tableName);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long endTime = System.currentTimeMillis();
		System.out.println("\n\nSCRIPT END at: " + endTime);
		double toSec = (endTime - startTime) / 1000;
		System.out.println("SCRIPT TERMINATED in " + toSec + "sec");

	}

	public static void printTableColumn(Connection con, String tablename) {

		try {
			DatabaseMetaData databaseMetaData = con.getMetaData();
			// Print TABLE_TYPE "TABLE"
			ResultSet columns = databaseMetaData.getColumns(null, null, tablename, null);
			System.out.println("\n==== TABLE " + tablename + " ");
			System.out.println("[COLUMN_NAME - TYPE_NAME ( COLUMN_SIZE )]");
			System.out.println("-----------------------------------------------");
			while (columns.next()) {
				String columnName = columns.getString("COLUMN_NAME");
				String columnType = columns.getString("TYPE_NAME");
				int columnSize = columns.getInt("COLUMN_SIZE");
				System.out.println("\t" + columnName + " - " + columnType + " (" + columnSize + ")");
			}
			System.out.println("-----------------------------------------------");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Gets the database connection.
	 *
	 * @param dbURL the db url
	 * @param user  the user
	 * @param pwd   the pwd
	 * @return the database connection
	 */
	public static Connection getDatabaseConnection(String dbURL, String user, String pwd) {

		System.out.println("dbURL: " + dbURL);
		Connection c = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection(dbURL, user, pwd);
			c.setAutoCommit(false);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		System.out.println("Opened database successfully");

		return c;
	}

	public static void addColumnTable(Connection con, String tableName, String newColumn, String type)
			throws SQLException {

		String alterTableString = String.format("ALTER TABLE %s ADD COLUMN %s %s", tableName, newColumn, type);

		System.out.println("Executing: " + alterTableString);

		try {
			con.setAutoCommit(false);
			if (!READ_ONLY_MODE) {
				con.createStatement().execute(alterTableString);
				con.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			if (con != null) {
				try {
					System.err.print("Transaction is being rolled back");
					con.rollback();
				} catch (SQLException excep) {
					e.printStackTrace();

				}
			}
		}

		System.out.println("Executed: " + alterTableString);
	}

	public static void renameColumnTable(Connection con, String tableName, String oldColumn, String newColumn)
			throws SQLException {

		String alterTableString = String.format("ALTER TABLE %s RENAME COLUMN %s TO %s", tableName, oldColumn,
				newColumn);

		System.out.println("Executing: " + alterTableString);

		try {
			con.setAutoCommit(false);
			if (!READ_ONLY_MODE) {
				con.createStatement().execute(alterTableString);
				con.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			if (con != null) {
				try {
					System.err.print("Transaction is being rolled back");
					con.rollback();
				} catch (SQLException excep) {
					e.printStackTrace();

				}
			}
		}

		System.out.println("Executed: " + alterTableString);
	}

	public static void dropColumnTable(Connection con, String tableName, String oldColumn, String type)
			throws SQLException {

		String alterTableString = String.format("ALTER TABLE %s DROP COLUMN %s", tableName, oldColumn);

		System.out.println("Executing: " + alterTableString);

		try {
			con.setAutoCommit(false);
			if (!READ_ONLY_MODE) {
				con.createStatement().execute(alterTableString);
				con.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			if (con != null) {
				try {
					System.err.print("Transaction is being rolled back");
					con.rollback();
				} catch (SQLException excep) {
					e.printStackTrace();

				}
			}
		}

		System.out.println("Executed: " + alterTableString);
	}

	public static Boolean checkColumnExists(Connection con, String tableName, String columnName) throws SQLException {

		String columnExistsLabel = "COLUMN_EXISTS";

		String sql = String.format(
				"SELECT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='%s' AND column_name='%s') as %s",
				tableName, columnName, columnExistsLabel);

		System.out.println("\n+++ " + columnExistsLabel + " checking '" + tableName + "': " + sql);

		PreparedStatement p;
		ResultSet resultSet;
		try {

			p = con.prepareStatement(sql);
			resultSet = p.executeQuery();
			// Expected 1 row
			resultSet.next();
			Boolean columnExists = resultSet.getBoolean(columnExistsLabel);
			System.out.println("\t RESP --> Column '" + columnName + "' exists: " + columnExists + "\n");
			return columnExists;

		} catch (SQLException e) {
			System.err.println(e);
			return false;
		}
	}

	public static Boolean checkTableExists(Connection con, String tableName) throws SQLException {

		String tableExistsLabel = "TABLE_EXISTS";

		String sql = String.format(
				"SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name='%s' AND table_type LIKE 'BASE TABLE' AND table_schema LIKE 'public') as %s",
				tableName, tableExistsLabel);

		System.out.println("\n+++ " + tableExistsLabel + " checking '" + tableName + "': " + sql);

		PreparedStatement p;
		ResultSet resultSet;
		try {

			p = con.prepareStatement(sql);
			resultSet = p.executeQuery();
			// Expected 1 row
			resultSet.next();
			Boolean tableExists = resultSet.getBoolean(tableExistsLabel);
			System.out.println("\t RESP --> Table '" + tableName + "' exists: " + tableExists + "\n");
			return tableExists;

		} catch (SQLException e) {
			System.err.println(e);
			return false;
		}
	}

	public static void updateTable(Connection con, String tableName, String whereCondition,
			LinkedHashMap<String, String> mapColumnValue) throws SQLException {

		StringBuilder updateSQL = new StringBuilder();
		updateSQL.append("UPDATE " + tableName + " SET");

		int i = 1;
		for (String column : mapColumnValue.keySet()) {
			updateSQL.append(" " + column + "=?");
			if (i < mapColumnValue.size())
				updateSQL.append(", ");
			i++;
		}

		updateSQL.append(" WHERE " + whereCondition);

		String update = updateSQL.toString();

		// System.out.println("Executing: " + update);
		try (PreparedStatement updatePS = con.prepareStatement(update);) {
			int index = 1;
			for (String column : mapColumnValue.keySet()) {
				updatePS.setString(index, mapColumnValue.get(column));
				index++;
			}
			con.setAutoCommit(false);

			System.out.println("Executing: " + updatePS.toString());
			if (!READ_ONLY_MODE) {
				updatePS.executeUpdate();
				con.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			if (con != null) {
				try {
					System.err.print("Transaction is being rolled back");
					con.rollback();
				} catch (SQLException excep) {
					e.printStackTrace();

				}
			}
		}
	}

	// Step1: Main driver method
	public static List<String> readTableIDs(Connection con, String tableName, String columnNameProjectId) {
		PreparedStatement p = null;
		ResultSet resultSet = null;

		// Try block to catch exception/s
		List<String> listProjectIds = new ArrayList<String>();
		try {

			// SQL command data stored in String datatype
			String sql = String.format("SELECT * FROM %s", tableName);
			p = con.prepareStatement(sql);
			resultSet = p.executeQuery();

			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			int row = 1;
			System.out.println("\n####TABLE: " + tableName + " content..\n");
			while (resultSet.next()) {
				System.out.print("" + row + "] ");
				for (int i = 1; i <= columnsNumber; i++) {
					String columnValue = resultSet.getString(i);
					System.out.print("(" + rsmd.getColumnName(i) + ") " + columnValue + " | ");
				}
				row++;
				System.out.println("\n");

				String projectId = resultSet.getString(columnNameProjectId);
				listProjectIds.add(projectId);
			}
			System.out.println("####TABLE: " + tableName + " end content\n");
		}

		// Catch block to handle exception
		catch (SQLException e) {

			// Print exception pop-up on screen
			System.err.println(e);
		}

		System.out.println("returning list IDs: " + listProjectIds);
		return listProjectIds;

	}

}
