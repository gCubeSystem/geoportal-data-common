package org.gcube.application;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.configuration.Configuration;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.lifecycle.LifecycleInformation;
import org.gcube.application.geoportal.common.model.document.relationships.Relationship;
import org.gcube.application.geoportal.common.model.document.relationships.RelationshipNavigationObject;
import org.gcube.application.geoportalcommon.ConvertToDataValueObjectModel;
import org.gcube.application.geoportalcommon.ProjectDVBuilder;
import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.ProjectsCaller;
import org.gcube.application.geoportalcommon.geoportal.UseCaseDescriptorCaller;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.SearchingFilter.LOGICAL_OP;
import org.gcube.application.geoportalcommon.shared.WhereClause;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.LifecycleInformationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.PhaseDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.Before;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Project_Tests {

	private static final String GCUBE_CONFIG_PROPERTIES_FILENAME = "gcube_config.properties";
	// APP Working Directory + /src/test/resources must be the location of
	// gcube_config.properties
	private static String gcube_config_path = String.format("%s/%s",
			System.getProperty("user.dir") + "/src/test/resources", GCUBE_CONFIG_PROPERTIES_FILENAME);
	private static String CONTEXT;
	private static String TOKEN;

	private static UseCaseDescriptorCaller clientUCD = null;
	private static ProjectsCaller clientPrj = null;

	private static String PROFILE_ID = "profiledConcessioni";
	private static String PROJECT_ID = "6384aaac308f5c28c5ee0888";

	private static String MY_LOGIN = "francesco.mangiacrapa";

	private static final String FOLLOWS = "follows";
	private static final String PRECEDES = "precedes";

	protected static class Phases {
		public static final String PENDING_APPROVAL = "Pending Approval";
		public static final String PUBLISHED = "Published";
		public static final String UNPUBLISHED = "UnPublished";

	}

	@Before
	public void init() {
		ContextConfigTest.readContextSettings();
		CONTEXT = ContextConfigTest.CONTEXT;
		TOKEN = ContextConfigTest.TOKEN;
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		clientPrj = GeoportalClientCaller.projects();
		clientUCD = GeoportalClientCaller.useCaseDescriptors();
	}

	// @Test
	public void getList() throws Exception {
		List<Project> listOfProjects = clientPrj.getListForProfileID(PROFILE_ID);

		int i = 0;
		for (Project project : listOfProjects) {
			System.out.println(++i + ") " + project);
		}
	}

	//@Test
	public void getByID() throws Exception {
		Project project = clientPrj.getProjectByID(PROFILE_ID, PROJECT_ID);
		ProjectDVBuilder projectBuilder = ProjectDVBuilder.newBuilder().fullDocumentMap(true);
		ProjectDV projectDV = ConvertToDataValueObjectModel.toProjectDV(project, projectBuilder);
		System.out.println(projectDV);

		LinkedHashMap<String, Object> theMap = projectDV.getTheDocument().getDocumentAsMap();
		for (String key : theMap.keySet()) {
			Object theValue = theMap.get(key);
			System.out.println("The key: " + key + " has value: " + theValue);
		}

		System.out.println("JSON: " + projectDV.getTheDocument().getDocumentAsJSON());

		System.out.println("Spatial reference: " + projectDV.getSpatialReference());
	}

	// @Test
	public void checkDocumentSerDes() throws Exception {
		Project project = clientPrj.getProjectByID(PROFILE_ID, PROJECT_ID);

		String documentValueAsJson = project.getTheDocument().toJson();
		System.out.println("1 " + documentValueAsJson);
		String updatedDocumentAsJson = new Document(Project.THE_DOCUMENT, documentValueAsJson).toJson();
		System.out.println("2 " + updatedDocumentAsJson);
		Document setUpdatedDocument = new Document("$set", updatedDocumentAsJson);
		System.out.println("3 " + setUpdatedDocument);

	}

	// @Test
	public void getListProjectsDV() throws Exception {
		List<Project> listOfProjects = clientPrj.getListForProfileID(PROFILE_ID);
		ProjectDVBuilder projectBuilder = ProjectDVBuilder.newBuilder().fullDocumentMap(true);
		int i = 0;
		for (Project project : listOfProjects) {
			ProjectDV projectDV = ConvertToDataValueObjectModel.toProjectDV(project, projectBuilder);
			System.out.println(++i + ") " + projectDV);
		}

	}

	// @Test
	public void getResultDocumentForID() {
		try {
			ProjectsCaller projects = GeoportalClientCaller.projects();
			Project project = projects.getProjectByID(PROFILE_ID, PROJECT_ID);
			ResultDocumentDV documentDV = ConvertToDataValueObjectModel.toResultDocumentDV(project);
			System.out.println("returning: " + documentDV);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void getListProjectsDVFilteredJSONDocument() throws Exception {
		// List<Project> listOfProjects = client.getListForProfileID(PROFILE_ID);

		SearchingFilter filter = new SearchingFilter();

		// Where Clause
		List<WhereClause> conditions = new ArrayList<WhereClause>();
		Map<String, Object> searchInto = new HashMap<String, Object>();
		//searchInto.put("_id", "6384aaac308f5c28c5ee0888");
		searchInto.put("_theDocument.nome", "della Civita di Tarquinia ");
		
		WhereClause whereClause = new WhereClause(LOGICAL_OP.OR, searchInto);

		conditions.add(whereClause);
		filter.setConditions(conditions);

		/* ORDER BY */
//		List<ItemFieldDV> orderByFields = new ArrayList<ItemFieldDV>();
//		List<String> jsonFields = Arrays.asList("_theDocument.dataInizioProgetto");
//		ItemFieldDV itemField = new ItemFieldDV("dataInizioProgetto", jsonFields, "$or", false, false, false);
//		orderByFields.add(itemField);
//		filter.setOrderByFields(orderByFields);

		LinkedHashMap<String, Object> projection = new LinkedHashMap<String, Object>();
		// default
		// PROJECTION
		projection.put(Project.ID, 1);
//		projection.put("_theDocument.nome", 1);
//		projection.put("_profileID", 1);
//		projection.put("_profileVersion", 1);
//		projection.put("_version", 1);
//		projection.put("_theDocument", 1);
//
//		projection.put("_theDocument.paroleChiaveLibere", 1);
//		projection.put("_theDocument.editore", 1);
//		projection.put("_theDocument.paroleChiaveICCD", 1);
//		projection.put("_theDocument.responsabile", 1);
//
//		projection.put("_theDocument.introduzione", 1);
//		projection.put("_theDocument.authors", 1);
//		projection.put("_theDocument.dataInizioProgetto", 1);

		//filter.setProjection(projection);

		Integer totalDocs = clientPrj.getTotalDocument(PROFILE_ID);
		Iterator<Project> projects = clientPrj.queryOnMongo(PROFILE_ID, 10, 0, null, filter);
		//Iterable<Project> itP = () -> projects;
		//Stream<Project> targetStream = StreamSupport.stream(itP.spliterator(), false);
		//List<String> listProjectIDs = targetStream.map(Project::getId).collect(Collectors.toList());

		List<ResultDocumentDV> results = ConvertToDataValueObjectModel.toListResultDocument(projects);
		int i = 0;
		for (ResultDocumentDV projectDV : results) {
			System.out.println(++i + ") " + projectDV.getId() + " JSON: "
					+ projectDV.getDocumentAsJSON());
		}
	}


	//@Test
	public void getListProjectsDVFiltered() throws Exception {
		// List<Project> listOfProjects = client.getListForProfileID(PROFILE_ID);

		SearchingFilter filter = new SearchingFilter();

		// Where Clause
		List<WhereClause> conditions = new ArrayList<WhereClause>();
		Map<String, Object> searchInto = new HashMap<String, Object>();
		// searchInto.put("_id", "61f0299baf51592c36795f52");
		// searchInto.put("_theDocument.nome", "Test progetto con clustering di
		// fileset");
//		searchInto.put("_theDocument.paroleChiaveLibere", "scavo");
//		searchInto.put("_theDocument.editore", "scavo");
//		searchInto.put("_theDocument.paroleChiaveICCD", "scavo");
//		searchInto.put("_theDocument.responsabile", "scavo");

		searchInto.put("_theDocument.dataInizioProgetto", "2019");
		WhereClause whereClause = new WhereClause(LOGICAL_OP.OR, searchInto);

		conditions.add(whereClause);
		filter.setConditions(conditions);

		/* ORDER BY */
//		List<ItemFieldDV> orderByFields = new ArrayList<ItemFieldDV>();
//		List<String> jsonFields = Arrays.asList("_theDocument.dataInizioProgetto");
//		ItemFieldDV itemField = new ItemFieldDV("dataInizioProgetto", jsonFields, "$or", false, false, false);
//		orderByFields.add(itemField);
//		filter.setOrderByFields(orderByFields);

		LinkedHashMap<String, Object> projection = new LinkedHashMap<String, Object>();
		// default
		// PROJECTION
		projection.put(Project.ID, 1);
//		projection.put("_theDocument.nome", 1);
//		projection.put("_profileID", 1);
//		projection.put("_profileVersion", 1);
//		projection.put("_version", 1);
//		projection.put("_theDocument", 1);
//
//		projection.put("_theDocument.paroleChiaveLibere", 1);
//		projection.put("_theDocument.editore", 1);
//		projection.put("_theDocument.paroleChiaveICCD", 1);
//		projection.put("_theDocument.responsabile", 1);
//
//		projection.put("_theDocument.introduzione", 1);
//		projection.put("_theDocument.authors", 1);
//		projection.put("_theDocument.dataInizioProgetto", 1);

		filter.setProjection(projection);

		Integer totalDocs = clientPrj.getTotalDocument(PROFILE_ID);
		Iterator<Project> projects = clientPrj.queryOnMongo(PROFILE_ID, totalDocs, 0, null, filter);
		Iterable<Project> itP = () -> projects;
		Stream<Project> targetStream = StreamSupport.stream(itP.spliterator(), false);
		List<String> listProjectIDs = targetStream.map(Project::getId).collect(Collectors.toList());

		List<ResultDocumentDV> results = ConvertToDataValueObjectModel.toListResultDocument(projects);
		int i = 0;
		for (ResultDocumentDV projectDV : results) {
			System.out.println(++i + ") " + projectDV.getId() + " dataInizioProgetto: "
					+ projectDV.getDocumentAsMap().get("dataInizioProgetto"));
		}

		// TEST TO PROJECT DV
		/*
		 * ProjectDVBuilder projectBuilder =
		 * ProjectDVBuilder.newBuilder().fullDocumentMap(true);
		 * 
		 * List<ProjectDV> listProjects =
		 * ConvertToDataValueObjectModel.toListProject(projects, projectBuilder); i = 0;
		 * for (ProjectDV projectDV : listProjects) { System.out.println(++i + ") " +
		 * projectDV); }
		 */

		/*
		 * int limit = 2; LinkedHashMap<String, Object> documentAsMap = new
		 * LinkedHashMap<String, Object>(limit);
		 * 
		 * try { // Project theProject = //
		 * GeoportalClientCaller.projects().getProjectByID(profileID, projectID);
		 * 
		 * QueryRequest request = new QueryRequest();
		 * request.setFilter(Document.parse("{\"" + Project.ID + "\" : " +
		 * "{\"$eq\" : \"" + PROJECT_ID + "\"}}"));
		 * request.setProjection(Document.parse("{\"" + Project.ID + "\" : " + "1}"));
		 * // should be one projects(PROFILE_ID).build().f
		 * projects(PROFILE_ID).build().query(request).forEachRemaining(p -> { try {
		 * System.out.println("p is: "+p); Iterator<Entry<String, Object>> entrySetsIt =
		 * p.getTheDocument().entrySet().iterator(); int i = 0; while
		 * (entrySetsIt.hasNext()) { if (i > limit) break;
		 * 
		 * Entry<String, Object> entry = entrySetsIt.next();
		 * documentAsMap.put(entry.getKey(), entry.getValue()); i++; }
		 * 
		 * } catch (Exception e) { throw e; } });
		 * 
		 * System.out.println("getEntrySetsDocumentForProjectID returning map: "
		 * +documentAsMap);
		 * 
		 * 
		 * } catch (Exception e) { String erroMsg =
		 * "Error occurred on loading EntrySets document for profileID " + PROFILE_ID +
		 * " and projectID " + PROJECT_ID; e.printStackTrace(); }
		 */
	}

	// @Test
	public void getLifecycleForProjectId() throws Exception {
		Project project = clientPrj.getProjectByID(PROFILE_ID, PROJECT_ID);
		LifecycleInformation lci = project.getLifecycleInformation();
		LifecycleInformationDV liDV = ConvertToDataValueObjectModel.toLifecycleInformationDV(lci);
		System.out.println(liDV);
//		
	}

	// @Test
	public void getConfiguration() throws Exception {
		Configuration config = clientPrj.getConfiguration(PROFILE_ID);
		System.out.println(config);
	}

	// @Test
	public void getTotalDocument() throws Exception {

		System.out.println(clientPrj.getTotalDocument(PROFILE_ID));
	}

	// @Test
	public void getListPhases() throws Exception {

		List<String> idsPhases = clientPrj.getIDsPhases(PROFILE_ID);
		System.out.println(idsPhases);

		PhaseDV[] phases = clientPrj.getPhasesIntoDocumentStoreCollection(PROFILE_ID);
		for (PhaseDV phaseDV : phases) {
			System.out.println(phaseDV);
		}

	}

	// @Test
	public void getCountByPhase() throws Exception {

		Integer integer = clientPrj.getCountByPhaseFor(PROFILE_ID, "Published", "OK");
		System.out.println(integer);
		integer = clientPrj.getCountByPhaseFor(PROFILE_ID, "Pending Approval", "OK");
		System.out.println(integer);

	}

	// @Test
	public void getRelationshipsChain() throws Exception {
		System.out.println("getRelationshipsChain test");
		Project project = clientPrj.getProjectByID(PROFILE_ID, PROJECT_ID);

		List<Relationship> relations = project.getRelationships();

		for (Relationship relationship : relations) {
			System.out.println("\n\ngetRelationshipsChain for " + relationship);
			Iterator<RelationshipNavigationObject> iterator = clientPrj.getRelationshipChain(PROFILE_ID, PROJECT_ID,
					relationship.getRelationshipName(), true);

			while (iterator.hasNext()) {
				RelationshipNavigationObject nav = (RelationshipNavigationObject) iterator.next();
				visitRelationshipsChain(nav, relationship.getRelationshipName());
			}
		}

	}

	public void visitRelationshipsChain(RelationshipNavigationObject nav, String relationshipName) {
		System.out.println("visitRelationshipsChain of target: " + nav.getTarget().getId() + ", relationshipName: "
				+ relationshipName + ", doc: " + nav.getTarget().getTheDocument());
		System.out.println("visitRelationshipsChain children " + nav.getChildren());

		if (nav == null)
			return;

		printObject(nav.getTarget());

		if (nav.getChildren() != null) {

			for (RelationshipNavigationObject relationship : nav.getChildren()) {
				visitRelationshipsChain(relationship, relationshipName);
			}
		}
	}

	public void printObject(Project project) {

		System.out.println("Project id: " + project.getId() + ", profile: " + project.getProfileID() + ", doc: "
				+ project.getTheDocument().toJson());
	}

	// @Test
	public void deserializeISOJSONObjectAsDate() throws Exception {

		String jsonDate = "{hour=0, minute=0, " + "dayOfYear=259, " + "dayOfWeek=MONDAY, " + "dayOfMonth=16, "
				+ "monthValue=9, " + "year=2019, " + "month=SEPTEMBER, " + "second=0, " + "nano=0, "
				+ "chronology={calendarType=iso8601, id=ISO}" + "}";

		try {

			/*
			 * ObjectMapper objectMapper = new ObjectMapper(); JavaTimeModule javaTimeModule
			 * = new JavaTimeModule(); javaTimeModule.addDeserializer(LocalDate.class, new
			 * MyLocalDateDeserializer()); javaTimeModule.addSerializer(LocalDate.class, new
			 * MyLocalDateSerializer()); objectMapper.registerModule(javaTimeModule);
			 * 
			 * objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
			 * objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,
			 * false); objectMapper.configure(SerializationFeature.
			 * WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false); LocalDate date =
			 * objectMapper.readerFor(LocalDate.class).readValue(jsonDate);
			 * System.out.println(date);
			 */

			// checking if the jsonDate is a LocalDate
			// LocalDateTime date =
			// org.gcube.application.geoportal.client.utils.Serialization.read(jsonDate,
			// LocalDateTime.class);
			LocalDate date = org.gcube.application.geoportal.client.utils.Serialization.read(jsonDate, LocalDate.class);
			System.out.println(date);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static final DateTimeFormatter FULL_FORMATTER = DateTimeFormatter.ofPattern("uuuuMMdd_HH-mm-ss");

	public static class MyLocalDateSerializer extends JsonSerializer<LocalDate> {
		@Override
		public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeString(value.format(FULL_FORMATTER));
		}
	}

	public static class MyLocalDateDeserializer extends JsonDeserializer<LocalDate> {

		@Override
		public LocalDate deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			return LocalDate.parse(p.getValueAsString(), FULL_FORMATTER);
		}
	}

	// @Test
	public void testEvaluateRelationshipsChain() throws Exception {
		System.out.println("testEvaluateRelationshipsChain test");

//		PROJECT_ID = "6399de3ca0a4545420373251";
//		
//		PROJECT_ID = "6399de68a0a4545420373257";
//		
//		
//		PROJECT_ID = "6399de96a0a4545420373258"; //no Relation
//		

		PROJECT_ID = "63c80aebacb8c7657b858741";

		Project project = clientPrj.getProjectByID(PROFILE_ID, PROJECT_ID);

		evaluateAdditionalIndexParameters(project);

	}

	public static Document evaluateAdditionalIndexParameters(Project project) throws Exception {
		Document toReturn = null;
		if (toReturn == null)
			toReturn = new Document();
		Project indexingProject = project;

		// Evaluate to display project IDs
		log.debug("Evaluating Last ID in relationship chain. Current Concessione ID is {}", indexingProject.getId());
		try {
			ArrayList<Project> projects = new ArrayList<>();

			// get Last ID in relation chain
			projects.add(indexingProject);

			if (!indexingProject.getRelationshipsByName(PRECEDES).isEmpty()) {
				List<RelationshipNavigationObject> relChain = getRelationshipChain(indexingProject.getProfileID(),
						indexingProject, PRECEDES, true);
				scanRelation(projects, relChain.get(0), false);
			}
			if (!indexingProject.getRelationshipsByName(FOLLOWS).isEmpty()) {
				List<RelationshipNavigationObject> relChain = getRelationshipChain(indexingProject.getProfileID(),
						indexingProject, FOLLOWS, true);
				scanRelation(projects, relChain.get(0), false);
			}

			log.debug("Produced full chain [size : {}] from {}, evaluating last available for PHASE {} ",
					projects.size(), indexingProject.getId(), indexingProject.getLifecycleInformation().getPhase());

			List<String> toDisplayId = new ArrayList<>();
			List<String> toHideIds = new ArrayList<>();

			log.info("projects are: " + projects.size());

			// Reverse Order means from the last FOLLOW to the first one (temporal reverse
			// order)
			Collections.sort(projects, Collections.reverseOrder(new ProjectRelationComparator()));

			int j = 0;
			for (Project theProject : projects) {
				log.info(++j + ") " + theProject.getId() + " data inizio: "
						+ theProject.getTheDocument().get("dataInizioProgetto"));
			}

			log.trace("Checking from LAST.. ");

			for (int i = 0; i < projects.size(); i++) {

				Project p = projects.get(i);
				String phase = p.getLifecycleInformation().getPhase();
				// IS TO DISPLAY EMPTY? Step into only once
				if (toDisplayId.isEmpty()) {
					// IF PHASE IS PENDING APPROVAL OR PUBLISHED
					if ((phase.equals(Phases.PENDING_APPROVAL) || phase.equals(Phases.PUBLISHED))) {
						toDisplayId.add(p.getId());
					}
				} else {

					switch (phase) {
					case Phases.PENDING_APPROVAL:
					case Phases.PUBLISHED: {
						if ((p.getLifecycleInformation().getPhase().equals(Phases.PENDING_APPROVAL)
								|| p.getLifecycleInformation().getPhase().equals(Phases.PUBLISHED)))
							toHideIds.add(p.getId());
						break;
					}
					}
				}
			}

			toReturn.put("_toHideIds", toHideIds);
			toReturn.put("_toDisplayIds", toDisplayId);

			log.info("Indexing request for Concessione [ID {}] with to HIDE {} and toDisplay {} ",
					indexingProject.getId(), toHideIds, toDisplayId);
			return toReturn;
		} catch (Exception e) {
			log.error("Unable to evaluate to Hide and Display Ids ", e);
			throw new Exception("Unable to evaluate chain ids to hide / display", e);
		}

	}

	public static class ProjectRelationComparator implements Comparator<Project> {
		@Override
		public int compare(Project p1, Project p2) {

			if (p1 == null)
				return 1;
			if (p2 == null)
				return -1;

			Integer compareResult = null;
			compareResult = compareProjectAgainstRelations(p1, p2);
			log.trace("p1 & p2, comparator result: {}", compareResult);
			if (compareResult == null) {
				log.debug("No relations beetween p1 & p2, checking inverted relations");
				compareResult = compareProjectAgainstRelations(p2, p1);
				log.trace("p2 & p1, comparator result: {}", compareResult);

				if (compareResult == null) {
					log.trace("p1 & p2, are not comparable, returning 0");
					compareResult = 0;
				}
			}

			log.debug("p1 & p2, comparator result, returns: {}", compareResult);
			return compareResult;
		}
	}

	public static Integer compareProjectAgainstRelations(Project source, Project target) {
		log.debug("comparing source {} and target {}", source.getId(), target.getId());
		List<Relationship> listRel = source.getRelationships();
		log.debug("relationships of {} are : {}", source.getId(), listRel);
		if (listRel != null) {
			String targetId = target.getId();
			for (Relationship relationship : listRel) {
				String relTargetId = relationship.getTargetID();
				if (targetId.compareTo(relTargetId) == 0) {
					String relationName = relationship.getRelationshipName();
					if (relationName.equals(PRECEDES)) {
						log.debug("source {} [rel {} ] target {}, so source < target ", source.getId(), PRECEDES,
								target.getId());
						// source < target
						return -1;
					} else if (relationName.equals(FOLLOWS)) {
						log.debug("source {} [rel {} ] target {}, so source > target ", source.getId(), FOLLOWS,
								target.getId());
						// source > target
						return 1;
					}
				}
			}
		}
		log.debug("No relations beetween a & b");
		return null;
	}

	private static void scanRelation(ArrayList chain, RelationshipNavigationObject obj, boolean putBefore) {
		if (putBefore)
			chain.add(0, obj.getTarget());
		else
			chain.add(obj.getTarget());
		if (obj.getChildren() != null)
			obj.getChildren().forEach(r -> scanRelation(chain, r, putBefore));
	}

	public static List<RelationshipNavigationObject> getRelationshipChain(String ucid, Project current,
			String relationshipId, Boolean deep) throws Exception {
		// recursive
		// ProfiledMongoManager manager = new ProfiledMongoManager(ucid);
		log.info("UCD {} : Getting Relationships List for {} [rel : {}, recurse {}]", ucid, current.getId(),
				relationshipId, deep);
//	        Project current = manager.getByID(id);
		long startTime = System.currentTimeMillis();
		List<RelationshipNavigationObject> toReturn = getLinked(current, relationshipId, deep);
		log.info("Got {} relationship elements in {}ms", toReturn.size(), (System.currentTimeMillis() - startTime));
		return toReturn;
	}

	private static List<RelationshipNavigationObject> getLinked(Project current, String relationName, Boolean recurse) {
		log.debug("Getting Relationships Lists for {} [rel : {}, recurse {}]", current.getId(), relationName, recurse);
		ArrayList<RelationshipNavigationObject> toReturn = new ArrayList<>();
		List<Relationship> existing = current.getRelationshipsByName(relationName);
		for (Relationship relationship : existing) {
			try {
				log.debug("Navigating from  {} : {} to[rel {} ] {} : {}", relationship.getTargetUCD(),
						relationship.getTargetID(), relationship.getRelationshipName(), current.getProfileID(),
						current.getId());
				RelationshipNavigationObject linkedProject = new RelationshipNavigationObject();
				// linkedProject.setTarget(new
				// ProfiledMongoManager(relationship.getTargetUCD()).getByID(relationship.getTargetID()));
//				Project target = new Project();
//				target.setProfileID(relationship.getTargetUCD());
//				target.setId(relationship.getTargetID());

				Project target = clientPrj.getProjectByID(relationship.getTargetUCD(), relationship.getTargetID());

				linkedProject.setTarget(target);
				if (recurse) {
					List<RelationshipNavigationObject> linked = getLinked(linkedProject.getTarget(), relationName,
							recurse);
					linkedProject.setChildren(linked);
				}
				toReturn.add(linkedProject);
			} catch (Exception e) {
				log.warn("Unable to navigate from  {} : {} to[rel {} ] {} : {}", relationship.getTargetUCD(),
						relationship.getTargetID(), relationship.getRelationshipName(), current.getProfileID(),
						current.getId(), e);
			}
		}
		return toReturn;
	}

}
