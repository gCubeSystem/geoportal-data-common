package org.gcube.application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportalcommon.ConvertToDataValueObjectModel;
import org.gcube.application.geoportalcommon.ProjectDVBuilder;
import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.ProjectsCaller;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.SearchingFilter.LOGICAL_OP;
import org.gcube.application.geoportalcommon.shared.WhereClause;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.se.RuntimeResourceReader;
import org.gcube.application.se.ServiceEndpointBean;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.scope.api.ScopeProvider;

public class ReSyncCentroidsMetadataOnDB {

//	private static final String CONTEXT = "/gcube/devsec/devVRE";
//	private static final String TOKEN = ""; // devVRE
	
//	private static final String CONTEXT = "/pred4s/preprod/preVRE";
//	private static final String TOKEN = ""; //preVRE
	
	private static final String CONTEXT = "/d4science.research-infrastructures.eu/D4OS/GNA";
	private static final String TOKEN = ""; //GNA

	// GEOPORTAL
	private static String PROFILE_ID = "profiledConcessioni";
	//private static final String PROFILE_ID = "concessioni-estere";
	private static final String JSON_KEY_DATA_FINE_PROGETTO = "dataFineProgetto";
	private static final String JSON_KEY_DATA_INIZIO_PROGETTO = "dataInizioProgetto";
	private static final String JSON_KEY_INTRODUZIONE = "introduzione";

	// DB
	private static final String platformName = "postgis";
	private static final String category = "Database";
	//private static final String resourceName = "GNA-POSTGIS-DB"; //devVRE
	//private static final String resourceName = "Geoserver-t postgis"; //preVRE
	private static final String resourceName = "GNA-postgis"; //GNA
	
	
	private static final String TABLE_COLUMN_DATA_FINE_PROGETTO = "data_fine_progetto";
	private static final String TABLE_COLUMN_DATA_INIZIO_PROGETTO = "data_inizio_progetto";
	private static final String TABLE_COLUMN_DESCRIZIONE = "descrizione";

	private static ProjectsCaller client = null;

	public static final boolean READ_ONLY_MODE = true;

	/**
	 * Gets the client.
	 *
	 * @return the client
	 */
	// @Before
	public static void initGeoportalClient() {
		// assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		client = GeoportalClientCaller.projects();
	}

	public static void main(String[] args) {
		
		System.out.println("READ_ONLY_MODE ENABLED: "+READ_ONLY_MODE);
		System.out.println("CONTEXT IS: "+CONTEXT);
		System.out.println("PROFILE_ID: "+PROFILE_ID);

		initGeoportalClient();
		
		//preVRE
//		String tableName = "profiledconcessioni_internal__prevre_centroids";
//		tableName = "profiledconcessioni_prevre_centroids";
//		tableName = "concessioni_estere_prevre_centroids";
//		tableName = "concessioni_estere_internal__prevre_centroids";
		
		
		//GNA
//		PROFILE_ID = "concessioni-estere";
//		String tableName = "concessioni_estere_internal__gna_centroids";
//		tableName = "concessioni_estere_gna_centroids";
		PROFILE_ID = "profiledConcessioni";
		String tableName = "profiledconcessioni_internal__gna_centroids";
		tableName = "profiledconcessioni_gna_centroids";

		
		long startTime = System.currentTimeMillis();
		
		try {
			RuntimeResourceReader rrr = new RuntimeResourceReader(CONTEXT, resourceName, platformName, category, null);
			ServiceEndpointBean se = rrr.getListSE().get(0);
			AccessPoint ap = se.getListAP().get(0);

			System.out.println(ap.address());
			System.out.println(ap.username());
			String pwdDB = RuntimeResourceReader.dectryptPassword(CONTEXT, ap);
			System.out.println(pwdDB);

			Connection dbConnection = getDatabaseConnection(ap.address(), ap.username(), pwdDB);

			// MUST BE DONE JUST ONCE
			try {
				renameColumnTable(dbConnection, tableName, "anno", TABLE_COLUMN_DATA_INIZIO_PROGETTO);
				renameColumnTable(dbConnection, tableName, "date_scavo", TABLE_COLUMN_DATA_FINE_PROGETTO);
				// addColumnTable(dbConnection, tableName, "gna_url", "TEXT"); //Could be a
				// VARCHAR(2048). Is it manageable by service?
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}

			List<String> listProjectIdsIntoDB = readTableIDs(dbConnection, tableName, "projectid");
			ScopeProvider.instance.set(CONTEXT);
			SecurityTokenProvider.instance.set(TOKEN);
			// List<ResultDocumentDV> listPublishedProjectsIntoService =
			// getListProjectsDVFiltered();

			int counter = 1;
			for (String projectId : listProjectIdsIntoDB) {
				Project project = null;
				System.out.println("\n\n################ "+counter+" of "+listProjectIdsIntoDB.size()+ ") Reading the project id: " + projectId);
				try {
					project = client.getProjectByID(PROFILE_ID, projectId);
				} catch (Exception e) {
					System.err.println("Project id not found: " + projectId);
				}
				try {

					if (project == null)
						continue;

					System.out.println("\n###### Trying to update Centroid TABLE for project id: " + projectId);

					ProjectDVBuilder projectBuilder = ProjectDVBuilder.newBuilder().fullDocumentMap(true);
					ProjectDV projectDV = ConvertToDataValueObjectModel.toProjectDV(project, projectBuilder);

					LinkedHashMap<String, Object> projectMap = projectDV.getTheDocument().getDocumentAsMap();

					LinkedHashMap<String, String> hashMap = new LinkedHashMap<String, String>();
					hashMap.put(TABLE_COLUMN_DATA_INIZIO_PROGETTO, projectMap.get(JSON_KEY_DATA_INIZIO_PROGETTO) + "");
					hashMap.put(TABLE_COLUMN_DATA_FINE_PROGETTO, projectMap.get(JSON_KEY_DATA_FINE_PROGETTO) + "");
					hashMap.put(TABLE_COLUMN_DESCRIZIONE, projectMap.get(JSON_KEY_INTRODUZIONE) + "");
					updateTable(dbConnection, tableName, "projectId='" + projectId + "'", hashMap);
					System.out.println("###UPDATED project id: " + projectId);
					
					Thread.sleep(500);
					System.out.println("################ Update completed for: " + projectId);
				} catch (Exception e) {
					System.err.println("Error on updating the table for the project id: " + projectId);
				}
				
				counter++;

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		long endTime = System.currentTimeMillis();
		System.out.println("\n\nSCRIPT END at: "+endTime);
		double toSec = (endTime-startTime)/1000;
		System.out.println("SCRIPT TERMINATED in "+toSec + "sec");

	}

	public static List<ResultDocumentDV> getListProjectsDVFiltered() throws Exception {
		// List<Project> listOfProjects = client.getListForProfileID(PROFILE_ID);

		SearchingFilter filter = new SearchingFilter();

		// Where Clause
		List<WhereClause> conditions = new ArrayList<WhereClause>();
		Map<String, Object> searchInto = new HashMap<String, Object>();
		searchInto.put("_lifecycleInformation._phase", "Published");
		WhereClause whereClause = new WhereClause(LOGICAL_OP.OR, searchInto);

		conditions.add(whereClause);
		filter.setConditions(conditions);

		LinkedHashMap<String, Object> projection = new LinkedHashMap<String, Object>();
		// default
		// PROJECTION
		String theDoc = Project.THE_DOCUMENT + ".";
		projection.put(Project.ID, 1);
		projection.put(theDoc + "nome", 1);
		projection.put(theDoc + JSON_KEY_DATA_INIZIO_PROGETTO, 1);
		projection.put(theDoc + JSON_KEY_DATA_FINE_PROGETTO, 1);

		filter.setProjection(projection);

		Integer totalDocs = client.getTotalDocument(PROFILE_ID);
		Iterator<Project> projects = client.queryOnMongo(PROFILE_ID, totalDocs, 0, null, filter);

		List<ResultDocumentDV> results = ConvertToDataValueObjectModel.toListResultDocument(projects);
		int i = 0;
		for (ResultDocumentDV projectDV : results) {
			System.out.println(++i + ") " + projectDV.getId() + " " + JSON_KEY_DATA_INIZIO_PROGETTO + ": "
					+ projectDV.getDocumentAsMap().get(JSON_KEY_DATA_INIZIO_PROGETTO) + " "
					+ JSON_KEY_DATA_FINE_PROGETTO + ": " + projectDV.getDocumentAsMap().get(JSON_KEY_DATA_FINE_PROGETTO)
					+ " " + JSON_KEY_INTRODUZIONE + ": " + projectDV.getDocumentAsMap().get(JSON_KEY_INTRODUZIONE));
		}

		return results;
	}

	/**
	 * Gets the database connection.
	 *
	 * @param dbURL the db url
	 * @param user  the user
	 * @param pwd   the pwd
	 * @return the database connection
	 */
	public static Connection getDatabaseConnection(String dbURL, String user, String pwd) {

		System.out.println("dbURL: " + dbURL);
		Connection c = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection(dbURL, user, pwd);
			c.setAutoCommit(false);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		System.out.println("Opened database successfully");

		return c;
	}

	public static void renameColumnTable(Connection con, String tableName, String oldColumn, String newColumn)
			throws SQLException {

		String alterTableString = String.format("ALTER TABLE %s RENAME COLUMN %s TO %s", tableName, oldColumn,
				newColumn);

		System.out.println("Executing: " + alterTableString);
		try {
			con.setAutoCommit(false);
			if (!READ_ONLY_MODE) {
				con.createStatement().execute(alterTableString);
				con.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			if (con != null) {
				try {
					System.err.print("Transaction is being rolled back");
					con.rollback();
				} catch (SQLException excep) {
					e.printStackTrace();

				}
			}
		}

		System.out.println("Executed: " + alterTableString);
	}

	public static void addColumnTable(Connection con, String tableName, String newColumn, String type)
			throws SQLException {

		String alterTableString = String.format("ALTER TABLE %s ADD COLUMN %s %s", tableName, newColumn, type);

		System.out.println("Executing: " + alterTableString);

		try {
			con.setAutoCommit(false);
			if (!READ_ONLY_MODE) {
				con.createStatement().execute(alterTableString);
				con.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			if (con != null) {
				try {
					System.err.print("Transaction is being rolled back");
					con.rollback();
				} catch (SQLException excep) {
					e.printStackTrace();

				}
			}
		}

		System.out.println("Executed: " + alterTableString);
	}

	public static void updateTable(Connection con, String tableName, String whereCondition,
			LinkedHashMap<String, String> mapColumnValue) throws SQLException {

//		String alterTableString = String.format("UPDATE TABLE %s SET COLUMN %s TO %s", tableName, oldColumn,
//				newColumn);
//		
		StringBuilder updateSQL = new StringBuilder();
		updateSQL.append("UPDATE " + tableName + " SET");

		int i = 1;
		for (String column : mapColumnValue.keySet()) {
			updateSQL.append(" " + column + "=?");
			if (i < mapColumnValue.size())
				updateSQL.append(", ");
			i++;
		}

		updateSQL.append(" WHERE " + whereCondition);

		String update = updateSQL.toString();

		// System.out.println("Executing: " + update);
		try (PreparedStatement updatePS = con.prepareStatement(update);) {
			int index = 1;
			for (String column : mapColumnValue.keySet()) {
				updatePS.setString(index, mapColumnValue.get(column));
				index++;
			}
			con.setAutoCommit(false);

			System.out.println("Executing: " + updatePS.toString());
			if (!READ_ONLY_MODE) {
				updatePS.executeUpdate();
				con.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			if (con != null) {
				try {
					System.err.print("Transaction is being rolled back");
					con.rollback();
				} catch (SQLException excep) {
					e.printStackTrace();

				}
			}
		}
	}

	// Step1: Main driver method
	public static List<String> readTableIDs(Connection con, String tableName, String columnNameProjectId) {
		PreparedStatement p = null;
		ResultSet resultSet = null;

		// Try block to catch exception/s
		List<String> listProjectIds = new ArrayList<String>();
		try {

			// SQL command data stored in String datatype
			String sql = String.format("SELECT * FROM %s", tableName);
			p = con.prepareStatement(sql);
			resultSet = p.executeQuery();

			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			int row = 1;
			System.out.println("\n####TABLE: " + tableName + " content..\n");
			while (resultSet.next()) {
				System.out.print("" + row + "] ");
				for (int i = 1; i <= columnsNumber; i++) {
					String columnValue = resultSet.getString(i);
					System.out.print("(" + rsmd.getColumnName(i) + ") " + columnValue + " | ");
				}
				row++;
				System.out.println("\n");

				String projectId = resultSet.getString(columnNameProjectId);
				listProjectIds.add(projectId);
			}
			System.out.println("####TABLE: " + tableName + " end content\n");
		}

		// Catch block to handle exception
		catch (SQLException e) {

			// Print exception pop-up on screen
			System.err.println(e);
		}

		System.out.println("returning list IDs: " + listProjectIds);
		return listProjectIds;

	}

}
