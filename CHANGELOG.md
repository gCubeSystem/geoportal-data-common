
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.4.0]

- Search Filter for profileId and projectID [#27120]
- Integrated new Uri-Resolver-Manager [#27160]
- Added new operation "Share project"
- Added optional message when performing lifecycle step [#27192]

## [v2.3.0]

- Read countByPhase from configuration [#25598]
- Reduced/Optimized some LOGs [#25539]

## [v2.2.0]

- Integrated the cross-filtering configuration [#25074]
- Integrated the overlay layers configuration [#25110]

## [v2.1.0] - 2023-05-11

- Integrated the deleteFileset method [#24977]
- Integrated with the Geoportal_Resolver service [#25031]

## [v2.0.3] - 2023-02-09

#### Enhancements

- [#24569] Added the phase DRAFT in the enum (also required for #24571)

## [v2.0.2] - 2022-02-03

#### Enhancements

- [#24432] Reverted serialization from Sting to Object values returning the Document as Map
- [#24475] Propagated the Access Policy in the fileset

## [v2.0.1] - 2022-01-19

#### Bug fixes

- [#24263] Fixing JSON library v20090211
- [#24432] Fixing serialization issue using LinkedHashMap<String, String> instead of LinkedHashMap<String, Object>.
- [#24432] Added a trim for returning value to solve a space as suffix into returned date (fixing the data displayed in the Timeline Widget)

## [v2.0.0] - 2022-11-17

#### Enhancements
 
- [#22883] Integrated the configurations exposed by (the new) geoportal-client (>= 1.1.0-SNAPSHOT)
- Passed to UCD/Projects geoportal client/service
- [#23835] Integrated with WORKFLOW_ACTION_LIST_GUI configuration
- [#23909] Declared lombok required for JDK_11
- [#23913] Integrated with GUI presentation configurations read from IS
- [#23927] Integrated with Relationship definition in UCD
- [#23834] Integrated with the create/view/delete Relationship facility
- [#24136] Integrated the temporal dimension on the front-end side
- [#24458] Published projects cannot be edited/updated

## [v1.4.0] - 2022-06-08

#### Enhancements

- [#23392] Implemented the "Clone Project" facility
- [#23457] Implemented the "Publish/UnPublish Project" facility

## [v1.3.0] - 2021-12-03

#### Enhancements

- [#22506] Added classes to read configurations and common classes from the GNA components: DataEntry and DataViewer

## [v1.2.0] - 2021-09-29

#### Enhancements

- [#20595] Porting common model
- [#21890] Passed to mongoID
- [#22002] Integrated with ValidationReport bean
- [#22040] Revisited the "Abstract Relazione di Scavo"
- Passed to gcube-bom 2.0.1


## [v1.0.0] - 2020-12-01

- First release
